import React, {useState} from "react";
import TextField from "@material-ui/core/TextField";
import {Button, createStyles, Divider, Grid, InputLabel, MenuItem, Select, Theme,} from "@material-ui/core";
import {makeStyles} from "@material-ui/core/styles";
import FormHelperText from "@material-ui/core/FormHelperText";
import Popper from "@material-ui/core/Popper";
import Fade from "@material-ui/core/Fade";
import Typography from "@material-ui/core/Typography";
import Paper from "@material-ui/core/Paper";

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        inputItem: {
            margin: "10px",
        },
        personalData: {
            margin: "10px 40px 0 40px",
        },
        buttonGroup: {
            "margin-top": "100px",
            "margin-bottom": "20px",
        },
        buttonItem: {
            "margin-left": "10px",
        },
        formControl: {
            margin: theme.spacing(1),
            minWidth: 120,
        },
        typography: {
            padding: theme.spacing(2),
            margin: theme.spacing(1),
        },
    })
);

export default function CreateUser() {


    const classes = useStyles();

    //это для раскрывающегося списка
    const [role, setRole] = React.useState<string[]>([]); //тут храниться значение поля
    const [isStudent, setIsStudent] = React.useState<boolean>(false); //это состояние для определения выбрали студента или нет
    const [roleDirty, setRoleDirty] = useState<boolean>(false);
    const [roleError, setRoleError] = useState<string>("");
    const [roleIsError, setRoleIsError] = useState<boolean>(false);

    //здесь обработчик смены значения в раскрывающемся списке
    let valueSelect = [""];
    const handleChange = (event: React.ChangeEvent<{ value: unknown }>) => {
        setRole(event.target.value as string[]);
        valueSelect = event.target.value as string[];
        setIsStudent(valueSelect.includes("student"));

        if (valueSelect.length == 0) {
            setRoleError("Поле обязательно");
            setRoleIsError(true);
        } else {
            setRoleError("");
            setRoleIsError(false);
        }
    };

    /* дальше код валидации */
    //состояния для значений
    const [email, setEmail] = useState<string>("");
    const [firstName, setFirstName] = useState<string>("");
    const [lastName, setLastName] = useState<string>("");
    const [numberGroup, setNumberGroup] = useState<string>("");
    ///для ролей определили выше, отчество - небязательное поле

    //состояния-флаги если значение есть
    const [emailDirty, setEmailDirty] = useState<boolean>(false);
    const [firstNameDirty, setFirstNameDirty] = useState<boolean>(false);
    const [lastNameDirty, setLastNameDirty] = useState<boolean>(false);
    const [numberGroupDirty, setNumberGroupDirty] = useState<boolean>(false);

    //состояния для текста ошибки
    const [emailError, setEmailError] = useState<string>("");
    const [firstNameError, setFirstNameError] = useState<string>("");
    const [lastNameError, setLastNameError] = useState<string>("");
    const [numberGroupError, setNumberGroupError] = useState<string>("");

    //состояния-флаги если ошибка есть
    const [emailIsError, setEmailIsError] = useState<boolean>(false);
    const [firstNameIsError, setFirstNameIsError] = useState<boolean>(false);
    const [lastNameIsError, setLastNameIsError] = useState<boolean>(false);
    const [numberGroupIsError, setNumberGroupIsError] = useState<boolean>(false);

    //состояние для открытия подсказки popper, текст подсказки
    const [openPopper, setOpenPopper] = useState<boolean>(false);
    const [textPopper, setTextPopper] = useState<string>("");
    const [anchorEl, setAnchorEl] = React.useState(null);

    //обработчик события, когда пользователь покинул поле ввода
    const blurHandler = (e: any) => {
        switch (e.target.id) {
            case "EmailField":
                setEmailDirty(true);
                if (e.target.value == "") {
                    setEmailError("Поле обязательно");
                    setEmailIsError(true);
                }
                break;
            case "lastNameField":
                setFirstNameDirty(true);
                if (e.target.value == "") {
                    setLastNameError("Поле обязательно");
                    setLastNameIsError(true);
                }
                break;
            case "firstNameField":
                setLastNameDirty(true);
                if (e.target.value == "") {
                    setFirstNameError("Поле обязательно");
                    setFirstNameIsError(true);
                }
                break;
            case"numberGroupField":
                setNumberGroupDirty(true);
                if (e.target.value == "") {
                    setNumberGroupError("Поле обязательно");
                    setNumberGroupIsError(true);
                }
                break;
            case "select-role":
                setRoleDirty(true);
                if (e.target.value.length == 0) {
                    setRoleError("Поле обязательно");
                    setRoleIsError(true);
                }
                break;
        }
    };

    //обработчики смены значений в полях (для value)
    const emailHandler = (e: any) => {
        setEmail(e.target.value);
        const reg = /^[a-zA-Z0-9]+@[a-zA-Z0-9]+\.[a-zA-Z0-9]+$/;
        if (!reg.test(String(e.target.value).toLowerCase())) {
            setEmailError("Неправильный формат e-mail");
            setEmailIsError(true);
        } else {
            setEmailError("");
            setEmailIsError(false);
        }
    }

    const lastNameHandler = (e: any) => {
        setLastName(e.target.value);
        const reg = /^[а-яА-Я]+$/;
        if (!reg.test(String(e.target.value).toLowerCase())) {
            setLastNameError("Некорректная фамилия!");
            setLastNameIsError(true);
        } else {
            setLastNameError("");
            setLastNameIsError(false);
        }
    }

    const firstNameHandler = (e: any) => {
        setFirstName(e.target.value);
        const reg = /^[а-яА-Я]+$/;
        if (!reg.test(String(e.target.value).toLowerCase())) {
            setFirstNameError("Некорректное имя!");
            setFirstNameIsError(true);
        } else {
            setFirstNameError("");
            setFirstNameIsError(false);
        }
    }

    const numberGroupHandler = (e: any) => {
        setNumberGroup(e.target.value);
        const reg = /^[0-9]+$/;
        if (!reg.test(String(e.target.value).toLowerCase())) {
            setNumberGroupError("Некорректная группа!");
            setNumberGroupIsError(true);
        } else {
            setNumberGroupError("");
            setNumberGroupIsError(false);
        }
    }

    //обработчик нажатия на сохранить - валидация формы
    const saveButtonHandler = (e: any) => {
        if (email == "" || lastName == "" || firstName == "" || role.length == 0) {
            //если не заполнены обязательные поля
            setAnchorEl(e.currentTarget);
            setTextPopper("Не заполнены обязательные поля");
            setOpenPopper(true);
        } else if (role.includes("student") && numberGroup == "") {
            //если выбран студент, но не указана группа
            setAnchorEl(e.currentTarget);
            setTextPopper("Укажите группу студента");
            setOpenPopper(true);
        } else {
            //если все в порядке
            setAnchorEl(e.currentTarget);
            setTextPopper("");
            setOpenPopper(false);

            //вернуться на страницу пользователей
            window.history.back()
        }

    }

    //обработчик нажатия на отмена - валидация формы
    const cancelButtonHandler = (e: any) => {
        //вернуться на страницу пользователей
        window.history.back()
    }


    return (
        <div className={classes.personalData}>
            <Grid container>
                <Grid item xs={12} className={classes.inputItem}>
                    <TextField
                        error={emailIsError}
                        helperText={emailError}
                        fullWidth
                        required
                        id="EmailField"
                        label="E-mail"
                        size="small"
                        variant="outlined"
                        value={email}
                        onBlur={e => blurHandler(e)}
                        onChange={(e) => emailHandler(e)}
                    />
                </Grid>

                <Grid item xs className={classes.inputItem}>
                    <TextField
                        error={lastNameIsError}
                        helperText={lastNameError}
                        required
                        id="lastNameField"
                        label="Фамилия"
                        size="small"
                        variant="outlined"
                        onBlur={e => blurHandler(e)}
                        onChange={(e) => lastNameHandler(e)}
                    />
                </Grid>
                <Grid item xs className={classes.inputItem}>
                    <TextField
                        error={firstNameIsError}
                        helperText={firstNameError}
                        required
                        id="firstNameField"
                        label="Имя"
                        size="small"
                        variant="outlined"
                        onBlur={e => blurHandler(e)}
                        onChange={(e) => firstNameHandler(e)}
                    />
                </Grid>
                <Grid item xs className={classes.inputItem}>
                    <TextField
                        id="patronymicField"
                        label="Отчество"
                        size="small"
                        variant="outlined"
                    />
                </Grid>
                <Grid item xs={12} className={classes.inputItem}>
                    <InputLabel id="select-role">
                        Выберите одну или несколько ролей
                    </InputLabel>
                    <Select
                        multiple
                        fullWidth
                        labelId="select-role"
                        id="select-role"
                        value={role}
                        onBlur={e => blurHandler(e)}
                        onChange={handleChange}
                    >
                        <MenuItem value={"student"}>Студент</MenuItem>
                        <MenuItem value={"teacher"}>Преподаватель</MenuItem>
                        <MenuItem value={"employee"}>Сотрудник университета</MenuItem>
                    </Select>
                    <FormHelperText>{roleError}</FormHelperText>
                </Grid>
                <Grid item xs={12} className={classes.inputItem}>
                    <Divider/>
                </Grid>
                {/*если выбрали студента*/}
                {isStudent && (
                    <Grid item xs className={classes.inputItem}>
                        <TextField
                            error={numberGroupIsError}
                            helperText={numberGroupError}
                            required
                            id="numberGroupField"
                            label="Группа"
                            size="small"
                            variant="outlined"
                            onBlur={e => blurHandler(e)}
                            onChange={(e) => numberGroupHandler(e)}
                        />
                    </Grid>
                )}
                <Grid item xs className={classes.buttonGroup}>
                    <Button
                        variant="contained"
                        color="secondary"
                        className={classes.buttonItem}
                        onClick={(e) => cancelButtonHandler(e)}
                    >
                        Отмена
                    </Button>
                    <Button
                        variant="contained"
                        color="primary"
                        className={classes.buttonItem}
                        onClick={(e) => saveButtonHandler(e)}
                    >
                        Сохранить
                    </Button>
                    <Popper open={openPopper} anchorEl={anchorEl} placement="bottom" transition>
                        {({TransitionProps}) => (
                            <Fade {...TransitionProps} timeout={350}>
                                <Paper>
                                    <Typography className={classes.typography}>{textPopper}</Typography>
                                </Paper>
                            </Fade>
                        )}
                    </Popper>
                </Grid>
            </Grid>
        </div>
    );
}
