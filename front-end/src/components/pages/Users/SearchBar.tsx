import React from 'react'
import {createStyles, makeStyles, Theme} from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import InputBase from '@material-ui/core/InputBase';
import IconButton from '@material-ui/core/IconButton';
import SearchIcon from '@material-ui/icons/Search';

interface SearchBarProps {
    text: string,
    placeholder: string
    value: string

    onChange(event: any): void

    onEnter(event: any): void
}

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        root: {
            padding: '2px 4px',
            display: 'flex',
            alignItems: 'center',
            width: 600,
        },
        input: {
            marginLeft: theme.spacing(1),
            flex: 1,
        },
        iconButton: {
            padding: 10,
        }
    }),
);

export const SearchBar: React.FC<SearchBarProps> = ({text, placeholder, value, onChange, onEnter}) => {
    const classes = useStyles();

    return (
        <Paper component="form" className={classes.root}>
            <InputBase
                className={classes.input}
                placeholder={placeholder}
                inputProps={{
                    'aria-label': 'search google maps',
                    'value': value,
                    'onChange': onChange,
                    'onKeyPress': onEnter
                }}
            />
            <IconButton type="submit" className={classes.iconButton} aria-label="search">
                <SearchIcon/>
            </IconButton>
        </Paper>
    )
}
