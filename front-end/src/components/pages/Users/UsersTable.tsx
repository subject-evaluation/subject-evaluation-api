import React, {useEffect, useState} from 'react';
import {User} from './User'
import {
    Button,
    Checkbox,
    Paper,
    Table,
    TableBody,
    TableCell,
    TableContainer,
    TableHead,
    TableRow
} from '@material-ui/core/';
import {roleRender, statusRender, StatusType} from './Types';

type Status = "BLOCK" | "ACTIVATE" | "DISABLED"

interface usersTableProps {
    users: User[]
}

export const UsersTable: React.FC<usersTableProps> = ({users}) => {


    const [selected, setSelected] = useState<number[]>([]);
    const [status, setStatus] = useState<Status>("DISABLED")

    useEffect(() => {
        setStatus(validateUserStatuses())
    }, [selected])

    const isSelected = (id: number) => selected.indexOf(id) !== -1;

    const isAllSelected =
        users.length > 0 && selected.length === users.length;


    const onSelect = (userId: number, checked: boolean) => {
        if (checked) {
            setSelected((prev) => prev.filter((id) => id !== userId));
        } else {
            setSelected((prev) => [userId, ...prev]);
        }
    };

    const onSelectAll = (event: React.ChangeEvent<HTMLInputElement>) => {
        if (event.target.checked) {
            setSelected(users.map((user) => user.id));
        } else {
            setSelected([]);
        }
    };

    const validateUserStatuses = () => {
        if (selected.length === 0) {
            return "DISABLED";
        }
        var lastStatus = getUserStatusById(selected[0]);
        for (let i = 0; i < selected.length; i++) {
            var userStatus = getUserStatusById(selected[i]);
            if (userStatus !== lastStatus) {
                return "DISABLED";
            }
            lastStatus = userStatus;
        }
        return (lastStatus === "ACTIVE" ? "BLOCK" : "ACTIVATE");
    }

    const getUserStatusById = (id: number) => {
        var userStatus: StatusType = "ACTIVE";
        users.forEach(user => {
            if (user.id === id) {
                userStatus = user.status;
                return;
            }
        })
        return userStatus;
    }

    const statusButtonRender = (role: Status) => {
        switch (role) {
            case "BLOCK":
                return 'Заблокировать';
            case "ACTIVATE":
                return 'Разблокировать';
            default:
                return 'Недоступно';
        }
    }

    const StatusButton: React.FC = () => {
        return (
            <Button variant="contained" color="primary" disabled={status === "DISABLED"}>
                {statusButtonRender(status)}
            </Button>
        )
    }

    return (
        <div>
            <Paper>
                <TableContainer>
                    <Table>
                        <TableHead>
                            <StatusButton/>
                            <TableRow>
                                <TableCell padding="checkbox">
                                    <Checkbox
                                        checked={isAllSelected}
                                        onChange={onSelectAll}
                                    />
                                </TableCell>
                                <TableCell>E-mail</TableCell>
                                <TableCell>ФИО</TableCell>
                                <TableCell>Роль</TableCell>
                                <TableCell>Статус</TableCell>
                                <TableCell>Группа</TableCell>
                            </TableRow>
                        </TableHead>
                        <TableBody>
                            {
                                users
                                    .map((user) => {
                                        const checked = isSelected(user.id);
                                        return (
                                            <TableRow
                                                hover
                                                onClick={() => onSelect(user.id, checked)}
                                                role="checkbox"
                                                aria-checked={checked}
                                                tabIndex={-1}
                                                key={user.id}
                                                selected={checked}
                                            >
                                                <TableCell padding="checkbox">
                                                    <Checkbox checked={checked}/>
                                                </TableCell>
                                                <TableCell>{user.email}</TableCell>
                                                <TableCell>{user.lastName + " " + user.firstName + " " + user.patronymic}</TableCell>
                                                <TableCell>{user.roles.map(role => roleRender(role)).join(", ")}</TableCell>
                                                <TableCell>{statusRender(user.status)}
                                                </TableCell>
                                                <TableCell>{user.group}</TableCell>
                                            </TableRow>
                                        )
                                    })
                            }
                        </TableBody>
                    </Table>
                </TableContainer>
            </Paper>
        </div>
    )
}
