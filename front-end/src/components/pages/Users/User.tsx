import {Role, UserStatus} from "../../../api/RestApi";

export interface User {
    id: number
    email: string;
    firstName: string;
    lastName: string;
    patronymic: string;
    roles: Role[];
    status: UserStatus;
    group?: string;
} 
