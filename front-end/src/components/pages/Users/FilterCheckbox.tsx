import React from 'react'
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Checkbox from '@material-ui/core/Checkbox';

interface FilterCheckboxProps {
    name: string,
    label: string,
    color: any,
    checked: boolean,

    onChange(e: any): void
}

export const FilterCheckbox: React.FC<FilterCheckboxProps> = ({name, label, color, checked, onChange}) => {
    return (
        <FormControlLabel
            control={<Checkbox name={name} color={color} checked={checked} onChange={onChange}/>}
            label={label}
        />
    )
}
