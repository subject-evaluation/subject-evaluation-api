import React, {useEffect, useState} from "react";
import {User} from './User'
import {UsersTable} from './UsersTable'
import {SearchBar} from './SearchBar'
import {FilterCheckbox} from './FilterCheckbox'
import {createStyles, FormLabel, Grid, Theme} from '@material-ui/core'
import FormControl from '@material-ui/core/FormControl';
import FormGroup from '@material-ui/core/FormGroup';
import {makeStyles} from "@material-ui/core/styles";
import {getToken} from "../../Auth/TokenProvider";
import Api from "../../../api/Api";
import {toAuthorizationHeaderValue} from "../../Utils";
import {Role, UserResponseDto, UserStatus} from "../../../api/RestApi";
import Button from "@material-ui/core/Button";
import TablePagination from "@material-ui/core/TablePagination";


const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        buttonClass: {
            "margin-top": "10px",
        },
    })
);

const UsersPage: React.FC = () => {
    const [activeFilter, setActive] = useState<boolean>(true)
    const [blockedFilter, setBlocked] = useState<boolean>(true)
    const [studentFilter, setStudent] = useState<boolean>(true)
    const [professorFilter, setProfessor] = useState<boolean>(true)
    const [employeeFilter, setEmployee] = useState<boolean>(true)
    const [searchText, setSearchText] = useState<string>('')
    const [currentUsers, setUsers] = useState<User[]>([])
    const [total, setTotal] = useState<number>(0);
    const [page, setPage] = useState(0);
    const [rowsPerPage, setRowsPerPage] = useState(5);

    useEffect(() => {
        getUsers();
    }, [activeFilter, blockedFilter, studentFilter, professorFilter, employeeFilter, searchText, page, rowsPerPage])

    const activeFilterHandler = () => setActive(activeFilter => !activeFilter)
    const blockedFilterHandler = () => setBlocked(blockedFilter => !blockedFilter)
    const studentFilterHandler = () => setStudent(prev => !studentFilter)
    const professorFilterHandler = () => setProfessor(prev => !professorFilter)
    const employeeFilterHandler = () => setEmployee(prev => !employeeFilter)

    const searchChangeHandler = (event: React.ChangeEvent<HTMLInputElement>) => {
        setSearchText(event.target.value)
    }

    const getUsers = () => {
        console.log(getStatusFilters())
        console.log(getRoleFilters())
        getToken().then(accessToken =>
            Api.searchUsers(
                {statuses: getStatusFilters(), roles: getRoleFilters(), line: "%" + searchText + "%"},
                {page: page, size: rowsPerPage},
                {headers: {Authorization: toAuthorizationHeaderValue(accessToken)}})
                .then((response) => {
                    setTotal(response.data.totalElements);
                    handleResponse(response.data.content);
                })
        );
    }

    const getStatusFilters = () => {
        var statusFilters = []
        if (activeFilter) statusFilters.push(UserStatus.ACTIVE)
        if (blockedFilter) statusFilters.push(UserStatus.BLOCKED)
        return statusFilters
    }

    const getRoleFilters = () => {
        var roleFilters = []
        if (studentFilter) roleFilters.push(Role.STUDENT)
        if (professorFilter) roleFilters.push(Role.TEACHER)
        if (employeeFilter) roleFilters.push(Role.EMPLOYEE)
        return roleFilters
    }

    const handleResponse = (users: UserResponseDto[]) => {
        var newUsers: User[] = []
        users.map((user) => newUsers.push(createUser(user)))
        setUsers(newUsers)
    }

    const createUser = (user: UserResponseDto): User => {
        console.log(user.roles);
        return {
            id: user.id,
            firstName: user.firstName,
            lastName: user.lastName,
            patronymic: user.patronymic,
            group: user.groupNumber,
            status: user.status,
            email: user.email,
            roles: user.roles
        }
    }

    const keyPressedHandler = (event: React.KeyboardEvent) => {
        if (event.key === 'Enter') {
            getUsers()
        }
    }

    const matches = (u: User, str: string): boolean => {
        return ((u.firstName.toLowerCase().includes(str.toLowerCase())) ||
            (u.lastName.toLowerCase().includes(str.toLowerCase())) ||
            (u.patronymic.toLowerCase().includes(str.toLowerCase())) ||
            (u.email.toLowerCase().includes(str.toLowerCase())))
    }

    const onChangePage = (event: unknown, newPage: number) => {
        setPage(newPage);
    };

    const onChangeRowsPerPage = (event: React.ChangeEvent<HTMLInputElement>) => {
        setRowsPerPage(parseInt(event.target.value, 10));
        setPage(0);
    };

    const FilterBlock: React.FC = () => {
        return (
            <>
                <FormControl component="fieldset">
                    <FormLabel component="legend">Роли</FormLabel>
                    <FormGroup>
                        <FilterCheckbox label="Студент" color="primary" checked={studentFilter}
                                        onChange={studentFilterHandler} name="student"/>
                        <FilterCheckbox label="Преподаватель" color="primary" checked={professorFilter}
                                        onChange={professorFilterHandler} name="professor"/>
                        <FilterCheckbox label="Сотрудник университета" color="primary" checked={employeeFilter}
                                        onChange={employeeFilterHandler} name="employee"/>
                    </FormGroup>
                </FormControl>
                <FormControl component="fieldset">
                    <FormLabel component="legend">Статус</FormLabel>
                    <FormGroup>
                        <FilterCheckbox label="Активен" color="secondary" checked={activeFilter}
                                        onChange={activeFilterHandler} name="active"/>
                        <FilterCheckbox label="Заблокирован" color="secondary" checked={blockedFilter}
                                        onChange={blockedFilterHandler} name="blocked"/>
                    </FormGroup>
                </FormControl>
            </>)
    }

    return (
        <Grid container>
            <Grid item xs={9}>
                <UsersTable users={currentUsers}/>
                <Button variant="contained" color="primary" href="/create-user" className={useStyles().buttonClass}>
                    Создать пользователя
                </Button>
                <TablePagination
                    rowsPerPageOptions={[5, 10, 25]}
                    component="div"
                    count={total}
                    rowsPerPage={rowsPerPage}
                    page={page}
                    onChangePage={onChangePage}
                    onChangeRowsPerPage={onChangeRowsPerPage}
                />
            </Grid>
            <Grid item xs={3}>
                <SearchBar text={searchText} placeholder="Поиск пользователя по ФИО или e-mail" value={searchText}
                           onChange={searchChangeHandler} onEnter={keyPressedHandler}/>
                <FilterBlock/>
            </Grid>
        </Grid>
    );
};

export default UsersPage
