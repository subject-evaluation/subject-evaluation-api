import {Role, UserStatus} from "../../../api/RestApi";

export type StatusType = "ACTIVE" | "BLOCKED"
export type RoleType = "EMPLOYEE" | "PROFESSOR" | "STUDENT"

export const statusRender = (status: UserStatus) => {
    switch (status) {
        case "ACTIVE":
            return 'Активен';
        default:
            return 'Заблокирован';
    }
}

export const roleRender = (role: Role) => {
    switch (role) {
        case Role.EMPLOYEE:
            return 'Сотрудник университета';
        case Role.TEACHER:
            return 'Преподаватель';
        default:
            return 'Студент';
    }
}
