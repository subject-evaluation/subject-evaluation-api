import React from "react";
import {SubjectForm} from "../subject_form/SubjectForm";

const UpdateSubject: React.FC = () => {
    return <SubjectForm headerName="Изменение курса"></SubjectForm>;
};

export default UpdateSubject;
