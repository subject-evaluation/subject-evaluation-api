import React from "react";
import {SubjectForm} from "../subject_form/SubjectForm";

const NewSubject: React.FC = () => {
    return <SubjectForm headerName="Создание курса"></SubjectForm>;
};

export default NewSubject;
