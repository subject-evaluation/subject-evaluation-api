import React from "react";
import {Header} from "../course_form/Header";
import SimpleTabs from "../view_course/TabPanel";

export enum about {
    Description,
    Polls,
    FeedBack,
}

interface ViewCourseProps {
    about: number;
    courseId: number;
}

export default function ViewCourse(props: ViewCourseProps) {
    return (
        <div className="main-container">
            <Header headerName={"Управление ИТ-проектами"}/>
            <SimpleTabs tabIndex={props.about} courseId={props.courseId}/>
        </div>
    );
}
