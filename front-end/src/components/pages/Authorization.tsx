import React from "react";
import "../styles/Authorization.css";
import Typography from "@material-ui/core/Typography";
import SignInButton from "../Auth/SignInButton";

const Authorization: React.FC = () => {
    return (
        <div className="Authorization">
            <div className="title">
                <Typography variant="h4" gutterBottom>
                    Студенческая оценка курсов
                </Typography>
            </div>

            <SignInButton/>

            <div className="description">
                <Typography gutterBottom>
                    Для аутентификации нужно использовать <br/>
                    университетский аккаунт в домене nsu.ru
                </Typography>
            </div>
        </div>
    );
};

export default Authorization;
