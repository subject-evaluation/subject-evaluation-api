import React from "react";
import Header from "../Header";

interface AppProps {
}

interface AppState {
}

class Home extends React.Component<AppProps, AppState> {
    render() {
        return (
            <div>
                <Header/>
                {this.props.children}
            </div>
        );
    }
}

export default Home;
