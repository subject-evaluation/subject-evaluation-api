import React, {useEffect, useState} from "react";
import Button from "@material-ui/core/Button";
import "../styles/ViewCourse.css";
import {CourseResponseDto} from "../../api/RestApi";
import Api from "../../api/Api";
import {getToken} from "../Auth/TokenProvider";
import {toAuthorizationHeaderValue} from "../Utils";
import {Link} from "react-router-dom";
import {TextField} from "@material-ui/core";

interface DescriptionCourseTabProps {
    courseId: number;
}

function DescriptionCourseTab(props: DescriptionCourseTabProps) {
    const [course, setCourse] = useState<CourseResponseDto>();

    useEffect(() => {
        getToken().then(accessToken =>
            Api.getCourse(props.courseId, {headers: {Authorization: toAuthorizationHeaderValue(accessToken)}})
                .then((response) => {
                    setCourse(response.data)
                })
        );
    },);

    let courseDescription: string = '';
    if (course?.description !== undefined)
        courseDescription = course?.description;

    const textFieldStyle = {
        width: 900,
        height: 200,
        marginTop: 60,
        right: 20
    };

    const textFieldProps = {
        style: textFieldStyle,
        multiline: true,
        variant: "outlined",
        rows: 200 / 21,
        readOnly: true
    };

    return (
        <div>
            <Button className={"shift-pos"} variant="contained" color="primary">
                <Link to={'/courses/update_course/' + props.courseId}
                      style={{textDecoration: 'none', color: 'white'}}>Изменить</Link>
            </Button>
            <label className={"shift-pos shift"}>Описание</label>
            <TextField
                InputProps={textFieldProps}
                variant={"outlined"}
                defaultValue={courseDescription}
            />
        </div>
    );
};

export default DescriptionCourseTab;
