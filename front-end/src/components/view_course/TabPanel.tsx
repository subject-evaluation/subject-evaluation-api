import React from "react";
import {makeStyles, Theme} from "@material-ui/core/styles";
import AppBar from "@material-ui/core/AppBar";
import Tabs from "@material-ui/core/Tabs";
import Tab from "@material-ui/core/Tab";
import Box from "@material-ui/core/Box";
import "../styles/SubjectForm.css";
import DescriptionCourseTab from "../view_course/DescriptionCourseTab";
import {Button} from "@material-ui/core";

interface TabPanelProps {
    children?: React.ReactNode;
    index: any;
    value: any;
}

interface SimpleTabProps {
    tabIndex: number;
    courseId: number;
}

function TabPanel(props: TabPanelProps) {
    const {children, value, index, ...other} = props;

    return (
        <div
            role="tabpanel"
            hidden={value !== index}
            id={`simple-tabpanel-${index}`}
            aria-labelledby={`simple-tab-${index}`}
            {...other}
        >
            {value === index && (
                <Box p={3}>
                    <span>{children}</span>
                </Box>
            )}
        </div>
    );
}

function a11yProps(index: any) {
    return {
        id: `simple-tab-${index}`,
        "aria-controls": `simple-tabpanel-${index}`,
    };
}

const useStyles = makeStyles((theme: Theme) => ({
    root: {
        width: "1000px",
        marginTop: "20px",
        marginLeft: "450px",
        backgroundColor: theme.palette.background.paper,
    },
}));

export default function SimpleTabs(props: SimpleTabProps) {
    const classes = useStyles();
    const [value, setValue] = React.useState(props.tabIndex);

    const handleChange = (event: React.ChangeEvent<{}>, newValue: number) => {
        setValue(newValue);
    };

    const redirect = () => {
        window.location.href = "http://localhost:3000/new-survey";
    };

    return (
        <div className={classes.root}>
            <AppBar position="relative">
                <Tabs
                    value={value}
                    onChange={handleChange}
                    aria-label="simple tabs example"
                >
                    <Tab label="Описание" {...a11yProps(0)} />
                    <Tab label="Опросы" {...a11yProps(1)} />
                    <Tab label="Отзывы" {...a11yProps(2)} />
                </Tabs>
            </AppBar>
            <TabPanel value={value} index={0}>
                <DescriptionCourseTab courseId={props.courseId}/>
            </TabPanel>
            <TabPanel value={value} index={1}>
                <Button variant="contained" color="primary" onClick={redirect}>
                    Создать опрос
                </Button>
            </TabPanel>
            <TabPanel value={value} index={2}>
                Item Three
            </TabPanel>
        </div>
    );
}
