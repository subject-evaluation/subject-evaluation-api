import {UserDto} from "../api/RestApi";
import moment from "moment";

export const toAuthorizationHeaderValue = (accessToken: string | null) => {
    return accessToken ? "Bearer " + accessToken : null;
}

export const userInfo = (user: UserDto) => {
    return `${user.lastName} ${user.firstName} ${user.patronymic !== null ? user.patronymic : ""} (${user.email})`;
}

export const DateFormat = (epochSeconds: number) => {
    return moment(epochSeconds * 1000).format("DD.MM.YYYY");
}
