import React, {useState} from 'react';
import PropTypes from 'prop-types';
import AnswerInput from "./AnswerInput";
import {QuestionType} from "../../api/RestApi";

// @ts-ignore
const QuestionInput = ({idx, questionState, handleQuestionChange, handleAddAnswers}) => {
    const textId = `text-${idx}`;
    const typeId = `type-${idx}`;

    const blankAnswer = {answerFormulation: ''};
    const [answerState, setAnswerState] = useState([
        {...blankAnswer},
    ]);

    const addAnswer = () => {
        setAnswerState([...answerState, {...blankAnswer}]);
    };

    const handleAnswerChange = (e: any) => {
        const updatedAnswers = [...answerState];
        // @ts-ignore
        updatedAnswers[e.target.dataset.idx][e.target.className] = e.target.value;
        setAnswerState(updatedAnswers);
        handleAddAnswers(answerState, idx);
    };

    return (
        <div key={`question-${idx}`}>
            <label htmlFor={textId}>{`Вопрос №${idx + 1}`}</label>
            <input
                type="text"
                name={textId}
                data-idx={idx}
                id={textId}
                className="questionFormulation"
                value={questionState[idx].questionFormulation}
                onChange={handleQuestionChange}
            />

            <label htmlFor={typeId}>Тип</label><br/>
            <select
                name={typeId}
                data-idx={idx}
                id={typeId}
                className="answerType"
                onChange={handleQuestionChange}
            >
                <option value={QuestionType.OPEN}>Ⓐ Текст</option>
                <option value={QuestionType.SINGLE_OPTION}>◯ Один вариант</option>
                <option value={QuestionType.MULTIPLE_OPTIONS}>▢ Несколько вариантов</option>
            </select>

            <input
                type="button"
                value="Добавить вариант ответа"
                onClick={addAnswer}
            />

            {
                answerState.map((val, answerIdx) => (
                    <AnswerInput
                        key={`answer-${answerIdx}`}
                        idx={answerIdx}
                        // questionIdx={idx}
                        answerState={answerState}
                        handleAnswerChange={handleAnswerChange}
                    />
                ))
            }

        </div>
    );
};

QuestionInput.propTypes = {
    idx: PropTypes.number,
    questionState: PropTypes.array,
    handleQuestionChange: PropTypes.func,
};

export default QuestionInput;
