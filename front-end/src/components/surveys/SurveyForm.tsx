import React, {useState} from 'react';
import QuestionInput from './QuestionInput';
import '../styles/SurveyForm.css';
import {toAuthorizationHeaderValue} from "../Utils";
import {getToken} from "../Auth/TokenProvider";
import {QuestionType, SurveyRequestDto} from "../../api/RestApi";
import Api from "../../api/Api";

const SurveyForm = () => {
    const [surveyState, setSurveyState] = useState({
        courseId: 0,
        authorId: 0,
        startDate: "2000-01-01",
        endDate: "2000-01-01",
        year: 'yyyy - yyyy',
        semester: 0,
        studentIds: [],
        teacherIds: [],
    });

    const handleSurveyFieldsChange = (e: any) => setSurveyState({
        ...surveyState,
        [e.target.name]: [e.target.value],
    });

    const blankQuestion = {
        questionFormulation: '',
        answerType: QuestionType.SINGLE_OPTION,
        answerChoices: []
    };
    const [questionState, setQuestionState] = useState([
        {...blankQuestion},
    ]);

    const addQuestion = () => {
        setQuestionState([...questionState, {...blankQuestion}]);
    };

    const handleQuestionChange = (e: any) => {
        const updatedQuestions = [...questionState];
        // @ts-ignore
        updatedQuestions[e.target.dataset.idx][e.target.className] = e.target.value;
        setQuestionState(updatedQuestions);
    };

    const addAnswersToQuestionState = (answerState: any, idx: number) => {
        const updatedQuestions = [...questionState];
        // @ts-ignore
        updatedQuestions[idx]['answerChoices'] = answerState;
        setQuestionState(updatedQuestions);
    };

    const statesToSurveyRequestDto = (): SurveyRequestDto => {
        return {
            courseId: surveyState.courseId,
            authorId: surveyState.authorId,
            startDate: new Date(surveyState.startDate),
            endDate: new Date(surveyState.endDate),
            year: surveyState.year,
            semester: surveyState.semester,
            studentIds: [],
            teacherIds: [],
            questions: questionState.map(q => ({
                type: q.answerType,
                title: q.questionFormulation,
                answerChoices: q.answerChoices
            }))
        }
    };

    const handleSubmit = (event: any) => {
        event.preventDefault();
        getToken().then(accessToken =>
            Api.createSurvey(
                statesToSurveyRequestDto(),
                {headers: {Authorization: toAuthorizationHeaderValue(accessToken)}}
            ).then(res => console.log(res))
        );
        window.location.reload();
    };

    return (
        <form onSubmit={handleSubmit}>
            <label htmlFor="courseId">ID курса</label>
            <input
                type="text"
                name="courseId"
                id="courseId"
                value={surveyState.courseId}
                onChange={handleSurveyFieldsChange}
            />
            <label htmlFor="authorId">Ваш ID</label>
            <input
                type="text"
                name="authorId"
                id="authorId"
                value={surveyState.authorId}
                onChange={handleSurveyFieldsChange}
            />
            <label htmlFor="startDate">Дата начала опроса</label>
            <input
                type="date"
                name="startDate"
                id="startDate"
                value={surveyState.startDate}
                onChange={handleSurveyFieldsChange}
            />
            <label htmlFor="endDate">Дата завершения опроса</label>
            <input
                type="date"
                name="endDate"
                id="endDate"
                value={surveyState.endDate}
                onChange={handleSurveyFieldsChange}
            />
            <label htmlFor="year">Учебный год в формате "yyyy - yyyy"</label>
            <input
                type="text"
                name="year"
                id="year"
                value={surveyState.year}
                onChange={handleSurveyFieldsChange}
            />
            <label htmlFor="semester">Номер семестра</label>
            <input
                type="text"
                name="semester"
                id="semester"
                value={surveyState.semester}
                onChange={handleSurveyFieldsChange}
            />
            {
                questionState.map((val, idx) => (
                    <QuestionInput
                        key={`question-${idx}`}
                        idx={idx}
                        questionState={questionState}
                        handleQuestionChange={handleQuestionChange}
                        handleAddAnswers={(event: any, idx: number) => addAnswersToQuestionState(event, idx)}
                    />
                ))
            }
            <input
                type="button"
                value="Добавить вопрос"
                onClick={addQuestion}
            />
            <input type="submit" value="Сохранить"/>
        </form>
    );
};

export default SurveyForm;
