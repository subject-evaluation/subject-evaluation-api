import React from "react";
import {Button} from "@material-ui/core";
import "../styles/Surveys.css";

interface AppProps {
}

interface AppState {
}

class Surveys extends React.Component<AppProps, AppState> {

    redirect() {
        window.location.href = "http://localhost:3000/new-survey";
    }

    render() {
        return (
            <div className="Surveys">
                <div className="Surveys__new_survey">
                    <Button variant="contained" color="primary" onClick={this.redirect}>
                        Создать опрос
                    </Button>
                </div>
            </div>
        );
    }
}

export default Surveys;
