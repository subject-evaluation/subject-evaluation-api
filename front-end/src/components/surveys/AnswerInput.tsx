import React from 'react';
import PropTypes from 'prop-types';

// @ts-ignore
const AnswerInput = ({idx, answerState, handleAnswerChange}) => {
    const answerTextId = `answerText-${idx}`;
    return (

        <div key={`answer-${idx}`}>
            <label htmlFor={answerTextId}>{`Вариант ответа №${idx + 1}`}</label>
            <input
                type="text"
                name={answerTextId}
                data-idx={idx}
                id={answerTextId}
                className="answerFormulation"
                value={answerState[idx].answerFormulation}
                onChange={handleAnswerChange}
            />
        </div>
    );
};

AnswerInput.propTypes = {
    idx: PropTypes.number,
    answerState: PropTypes.array,
    handleQuestionChange: PropTypes.func,
};

export default AnswerInput;
