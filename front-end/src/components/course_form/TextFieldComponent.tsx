import React, {ChangeEvent} from "react";
import {TextField} from "@material-ui/core";
import "../styles/SubjectForm.css";

interface BaseTextFieldProps {
    height: string;
    width: string;
    text: string;
    required: boolean;
    rows: number;
    onChange: any;
    value: string;
}

export const TextFieldComponent = (props: BaseTextFieldProps) => {
    const textFieldStyle = {
        width: props.width,
        height: props.height,
    };

    const textFieldProps = {
        style: textFieldStyle,
        multiline: true,
        variant: "outlined",
        rows: props.rows,
    };

    function onChange(e: ChangeEvent<HTMLInputElement>) {
        props.onChange(e.target.value);
    }

    return (
        <div className="div-tf">
            <label className={"label"}>{props.text}</label>
            <TextField
                InputProps={textFieldProps}
                required={props.required}
                variant="outlined"
                onChange={onChange}
                value={props.value}
            ></TextField>
        </div>
    );
};
