import React, {useEffect, useState} from "react";
import {TextFieldComponent} from "./TextFieldComponent";
import "../styles/SubjectForm.css";
import Button from "@material-ui/core/Button";
import FileUploader from "./FileUploader";
import {Header} from "./Header";
import {Link} from "react-router-dom";
import {CourseLevel, CourseRequestDto, CourseStatus} from "../../api/RestApi";
import Api from "../../api/Api";
import {getToken} from "../Auth/TokenProvider";
import {toAuthorizationHeaderValue} from "../Utils";
import {FormControl, MenuItem, Select} from "@material-ui/core";

interface CourseFormProps {
    headerName: string;
    courseId: number;
}

const buttonStyle = {
    marginTop: "20x",
    marginLeft: "10px",
    width: "100px",
    height: "25px",
    fontSize: "8pt",
    left: "80px",
};

const CourseForm: React.FC<CourseFormProps> = (props) => {
    const [name, setName] = useState("");
    const [description, setDescription] = useState("");
    const [level, setLevel] = useState(CourseLevel.UNDERGRADUATE);
    const [status, setStatus] = useState(CourseStatus.ACTIVE);

    useEffect(() => {
        if (props.courseId !== 0)
            getToken().then(accessToken =>
                Api.getCourse(props.courseId, {headers: {Authorization: toAuthorizationHeaderValue(accessToken)}})
                    .then((response) => {
                        setName(response.data.name);
                        setDescription(response.data.description);
                        setLevel(response.data.level);
                        setStatus(response.data.status);
                    })
            );
    }, []);

    const changeLevelValue = (event: React.ChangeEvent<{ value: unknown }>) => {
        setLevel(event.target.value as CourseLevel);
    };

    const changeNameField = (nameValue: string) => {
        setName(nameValue);
    }

    const changeDescriptionField = (descriptionValue: string) => {
        setDescription(descriptionValue);
    }

    const createOrUpdateCourse = () => {
        if (name === "")
            alert('Поле "Название" обязательно для заполнения!');
        else {
            const courseRequestDto: CourseRequestDto = {
                name: name,
                description: description,
                level: level,
                status: CourseStatus.ACTIVE
            };
            if (props.courseId === 0)
                addNewCourse(courseRequestDto);
            else
                updateCourse(courseRequestDto);
        }
    }

    const addNewCourse = (courseRequestDto: CourseRequestDto) => {
        getToken().then(accessToken =>
            Api.createCourse(
                courseRequestDto,
                {headers: {Authorization: toAuthorizationHeaderValue(accessToken)}}).then(() => {
            })
        )
    }

    const updateCourse = (courseRequestDto: CourseRequestDto) => {
        getToken().then(accessToken =>
            Api.updateCourse(props.courseId, courseRequestDto,
                {headers: {Authorization: toAuthorizationHeaderValue(accessToken)}})
                .then(() => {
                })
        )
    }

    let pathAfterSaving: string = "/courses/new_course";
    if (name !== '')
        pathAfterSaving = "/courses";

    return (
        <div className="main-container">
            <Header headerName={props.headerName}/>
            <TextFieldComponent
                rows={1}
                height={"40px"}
                width={"600px"}
                required={true}
                text={"Название"}
                value={name}
                onChange={changeNameField}
            />
            <label className={'label level-label'}>Ступень курса</label>
            <FormControl>
                <Select className={'select'}
                        labelId="demo-customized-select-label"
                        id="demo-customized-select"
                        value={level}
                        onChange={changeLevelValue}
                >
                    <MenuItem value={CourseLevel.UNDERGRADUATE}>Бакалавриат</MenuItem>
                    <MenuItem value={CourseLevel.MAGISTRACY}>Магистратура</MenuItem>
                    <MenuItem value={CourseLevel.POSTGRADUATE}>Аспирантура</MenuItem>
                </Select>
            </FormControl>
            <TextFieldComponent
                rows={300 / 21}
                height={"300px"}
                width={"600px"}
                required={false}
                text={"Описание"}
                value={description}
                onChange={changeDescriptionField}
            />
            <FileUploader/>
            <Button variant="outlined" style={buttonStyle}>
                <Link to={"/courses"} style={{textDecoration: 'none', color: 'black'}}>Отмена</Link>
            </Button>
            <Button onClick={createOrUpdateCourse} variant="contained" style={buttonStyle} color="primary">
                <Link to={pathAfterSaving} style={{textDecoration: 'none', color: 'white'}}>Сохранить</Link>
            </Button>
        </div>
    );
};

export default CourseForm;
