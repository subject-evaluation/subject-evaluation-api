import {IconButton} from "@material-ui/core";
import {ArrowBackIos} from "@material-ui/icons";
import React from "react";
import {Link} from "react-router-dom";

interface SubjectFormProps {
    headerName: string;
}

export const Header = ({headerName}: SubjectFormProps) => {
    return (
        <div className="header-container">
            <Link to={"/courses"} style={{textDecoration: 'none', color: 'black'}}>
                <IconButton>
                    <ArrowBackIos/>
                </IconButton>
            </Link>
            <p className="header">{headerName}</p>
        </div>
    );
};
