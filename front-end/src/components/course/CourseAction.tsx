import React, {useCallback, useEffect, useState} from "react";
import {Button} from "@material-ui/core";
import {useSelection} from "./SelectionContext";
import {getToken} from "../Auth/TokenProvider";
import {toAuthorizationHeaderValue} from "../Utils";
import Api from "../../api/Api";
import {CourseStatus} from "../../api/RestApi";

interface CourseActionProps {
    update: () => Promise<any>;
}

const CourseAction: React.FC<CourseActionProps> = ({update}) => {
    const {selected, onSelectAll} = useSelection();
    const [status, setStatus] = useState<CourseStatus | undefined>(undefined);

    useEffect(() => {
        if (selected.length === 0) {
            setStatus(undefined);
        } else {
            getToken()
                .then(accessToken => Api.defineCourseStatus(selected,
                    {headers: {Authorization: toAuthorizationHeaderValue(accessToken)}}))
                .then((response) => {
                    if (response.data.length === 0) {
                        setStatus(undefined);
                    } else {
                        setStatus(response.data)
                    }
                });
        }
    }, [selected]);

    const onClick = useCallback(() => {
        const promise = getToken();
        if (status === CourseStatus.ARCHIVED) {
            promise.then(accessToken => Api.unarchiveCourses(selected,
                {headers: {Authorization: toAuthorizationHeaderValue(accessToken)}}))
                .then(update)
                .then(() => onSelectAll(false));
        } else if (status === CourseStatus.ACTIVE) {
            promise.then(accessToken => Api.archiveCourses(selected,
                {headers: {Authorization: toAuthorizationHeaderValue(accessToken)}}))
                .then(update)
                .then(() => onSelectAll(false));
        }
    }, [update, selected, status, onSelectAll]);

    return (
        <Button variant="contained" color="primary" disabled={status === undefined} onClick={onClick}>
            {status === CourseStatus.ACTIVE ? 'Перенести в архив' : null}
            {status === CourseStatus.ARCHIVED ? 'Восстановить из архива' : null}
            {selected.length === 0 ? 'Выберите курсы' : (status === undefined ? 'Несовместимые статусы' : null)}
        </Button>
    );
}

export default CourseAction;
