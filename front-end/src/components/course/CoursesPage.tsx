import React, {ChangeEvent, useCallback, useEffect, useReducer, useState} from "react";
import CourseTable from "./CourseTable";
import {Button, Grid, InputAdornment, Paper, TextField, Toolbar,} from "@material-ui/core";
import CourseFilters from "./CourseFilters";
import {makeStyles} from "@material-ui/core/styles";
import SearchIcon from "@material-ui/icons/Search";
import {toAuthorizationHeaderValue} from "../Utils";
import {InitialPageState, PageActionType, PageReducer} from "./PageStore";
import Api from "../../api/Api";
import {getToken, hasSomeRoles} from "../Auth/TokenProvider";
import {CourseFilterActionType, CourseFilterReducer, InitialCourseFilter} from "./FilterStore";
import {CourseResponseDto, Role} from "../../api/RestApi";
import TablePagination from "@material-ui/core/TablePagination";
import {SelectionProvider} from "./SelectionContext";
import CourseAction from "./CourseAction";

const useStyles = makeStyles({
    root: {
        marginTop: "1vh"
    },
    column: {
        "& > * + *": {
            marginTop: "4vh",
        },
    },
    toolbar: {
        display: "flex",
        justifyContent: "space-between",
        alignItems: "center",
        '& > * + *': {
            marginLeft: '10%',
        }
    },
    search: {
        flexGrow: 1,
    },
});

const CoursesPage: React.FC = () => {
    const [filter, dispatchFilter] = useReducer(CourseFilterReducer, InitialCourseFilter);
    const [paging, dispatchPaging] = useReducer(PageReducer, InitialPageState);
    const [courses, setCourses] = useState<CourseResponseDto[]>([]);

    const isAllowed = hasSomeRoles(Role.EMPLOYEE, Role.TEACHER);

    const update = useCallback(() => {
        return getToken()
            .then(accessToken =>
                Api.searchCourses(filter, {
                    page: paging.page,
                    size: paging.rowsPerPage,
                }, {headers: {Authorization: toAuthorizationHeaderValue(accessToken)}}))
            .then((response) => {
                setCourses(response.data.content);
                dispatchPaging({
                    type: PageActionType.totalElements,
                    payload: response.data.totalElements,
                });
            });
    }, [filter, paging.page, paging.rowsPerPage]);

    useEffect(() => {
        update();
    }, [update]);

    const onChangeName = (event: ChangeEvent<HTMLInputElement>) => {
        dispatchFilter({
            type: CourseFilterActionType.courseName,
            payload: event.target.value,
        });
        dispatchPaging({
            type: PageActionType.page,
            payload: 0,
        });
    }

    const classes = useStyles();
    return (
        <SelectionProvider>
            <Grid className={classes.root} container spacing={5}>
                <Grid className={classes.column} item xs={10}>
                    <Paper>
                        <Toolbar className={classes.toolbar}>
                            {isAllowed && <CourseAction update={update}/>}
                            <TextField
                                className={classes.search}
                                placeholder="Поиск курса по названию"
                                InputProps={{
                                    startAdornment: (
                                        <InputAdornment position="start">
                                            <SearchIcon/>
                                        </InputAdornment>
                                    ),
                                }}
                                value={filter.name}
                                onChange={onChangeName}
                            />
                        </Toolbar>
                        <CourseTable courses={courses} paging={paging}/>
                        <TablePagination
                            rowsPerPageOptions={paging.defaultRowsPerPageOptions}
                            component="div"
                            count={paging.totalElements}
                            rowsPerPage={paging.rowsPerPage}
                            page={paging.page}
                            onChangePage={(e, newPage) =>
                                dispatchPaging({type: PageActionType.page, payload: newPage})
                            }
                            onChangeRowsPerPage={(e) =>
                                dispatchPaging({
                                    type: PageActionType.rowsPerPage,
                                    payload: +e.target.value,
                                })
                            }
                        />
                    </Paper>
                </Grid>
                <Grid className={classes.column} item xs={2}>
                    {isAllowed &&
                    <Button variant="contained" color="primary">
                        Добавить курс
                    </Button>}
                    <CourseFilters filter={filter} dispatch={dispatchFilter}/>
                </Grid>
            </Grid>
        </SelectionProvider>
    );
};

export default CoursesPage;
