import React, {useContext, useState} from 'react';
import {getToken} from "../Auth/TokenProvider";
import Api from "../../api/Api";
import {toAuthorizationHeaderValue} from "../Utils";

export interface Selection {
    selected: number[];
    isSelected: (id: number) => boolean;
    onSelect: (id: number, checked: boolean) => void;
    onSelectAll: (checked: boolean) => void;
}

const SelectionContext = React.createContext<Selection>({
    selected: [],
    isSelected: (id) => false,
    onSelect: (id, checked) => {
    },
    onSelectAll: (checked) => {
    },
});

export const useSelection = () => {
    return useContext(SelectionContext);
};

export const SelectionProvider: React.FC = ({children}) => {
    const [selected, setSelected] = useState<number[]>([]);

    const isSelected = (id: number) => selected.indexOf(id) !== -1;

    const onSelect = (newId: number, checked: boolean) => {
        if (checked) {
            setSelected((prev) => prev.filter((id) => id !== newId));
        } else {
            setSelected((prev) => [newId, ...prev]);
        }
    };

    const onSelectAll = (checked: boolean) => {
        if (checked) {
            getToken()
                .then(accessToken => Api.getAllCourseIds({headers: {Authorization: toAuthorizationHeaderValue(accessToken)}}))
                .then(response => setSelected(response.data))
        } else {
            setSelected([]);
        }
    };

    return (
        <SelectionContext.Provider value={{selected, isSelected, onSelect, onSelectAll}}>
            {children}
        </SelectionContext.Provider>
    );
};
