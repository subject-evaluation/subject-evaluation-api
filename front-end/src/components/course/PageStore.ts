import React from "react";

export const InitialPageState: PageState = {
    totalElements: 0,
    page: 0,
    rowsPerPage: 10,
    defaultRowsPerPageOptions: [10, 25, 50]
};

export enum PageActionType {
    page,
    rowsPerPage,
    totalElements
}

export interface PageState {
    totalElements: number;
    page: number;
    rowsPerPage: number;
    defaultRowsPerPageOptions: number[];
}

export interface PageAction {
    type: PageActionType;
    payload: any;
}

export const PageReducer: React.Reducer<PageState, PageAction> = (
    state,
    action
) => {
    switch (action.type) {
        case PageActionType.page:
            return {...state, page: +action.payload};
        case PageActionType.rowsPerPage:
            const newRowsPerPage = +action.payload;
            return {
                ...state,
                page: pageIfChangeRowsPerPage(state.page, state.rowsPerPage, newRowsPerPage),
                rowsPerPage: newRowsPerPage,
            };
        case PageActionType.totalElements:
            return {...state, totalElements: +action.payload};
    }
    return state;
};

const pageIfChangeRowsPerPage = (
    oldPage: number,
    oldRowsPerPage: number,
    newRowsPerPage: number
) => {
    return Math.floor((oldRowsPerPage * oldPage) / newRowsPerPage);
};