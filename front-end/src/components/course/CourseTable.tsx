import React from "react";
import {Checkbox, Table, TableBody, TableCell, TableContainer, TableHead, TableRow,} from "@material-ui/core";
import {CourseResponseDto, Role} from "../../api/RestApi";
import {$tl, $ts} from "./CourseTranslate";
import {PageState} from "./PageStore";
import {useSelection} from "./SelectionContext";
import {makeStyles} from "@material-ui/core/styles";
import {Link} from "react-router-dom";
import {hasSomeRoles} from "../Auth/TokenProvider";

const useStyles = makeStyles({
    root: {
        maxHeight: '66vh'
    },
    link: {
        textDecoration: 'none',
        color: 'black'
    }
});

interface CoursesProps {
    courses: CourseResponseDto[];
    paging: PageState;
}

const CourseTable: React.FC<CoursesProps> = ({courses, paging}) => {
    const {selected, isSelected, onSelect, onSelectAll} = useSelection();

    const isAllowed = hasSomeRoles(Role.EMPLOYEE, Role.TEACHER);

    const isAllSelected = () => selected.length > 0 && selected.length === paging.totalElements;

    const isIndeterminate = () => selected.length > 0 && selected.length < paging.totalElements;

    const classes = useStyles();
    return (
        <TableContainer className={classes.root}>
            <Table stickyHeader>
                <TableHead>
                    <TableRow>
                        {isAllowed &&
                        <TableCell padding="checkbox">
                            <Checkbox
                                indeterminate={isIndeterminate()}
                                checked={isAllSelected()}
                                onChange={(event) => onSelectAll(event.target.checked)}
                            />
                        </TableCell>}
                        <TableCell>Название</TableCell>
                        <TableCell>Ступень</TableCell>
                        <TableCell>Статус</TableCell>
                    </TableRow>
                </TableHead>
                <TableBody>
                    {courses.map((course) => {
                        const checked = isSelected(course.id);
                        return (
                            <TableRow
                                hover
                                onClick={() => onSelect(course.id, checked)}
                                role="checkbox"
                                aria-checked={checked}
                                tabIndex={-1}
                                key={course.id}
                                selected={checked}
                            >
                                {isAllowed &&
                                <TableCell padding="checkbox">
                                    <Checkbox checked={checked}/>
                                </TableCell>}
                                <TableCell>
                                    <Link to={`/courses/${course.id}`} className={classes.link}>{course.name}</Link>
                                </TableCell>
                                <TableCell>{$tl(course.level)}</TableCell>
                                <TableCell>{$ts(course.status)}</TableCell>
                            </TableRow>
                        );
                    })}
                </TableBody>
            </Table>
        </TableContainer>
    );
};

export default CourseTable;
