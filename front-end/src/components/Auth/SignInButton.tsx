import React from "react";
import {environment} from "../../enviroment";
import {login} from "./AuthProvider";
import {GoogleLogin, GoogleLoginResponse, GoogleLoginResponseOffline,} from "react-google-login";
import Api from "../../api/Api";
import {TokensDto} from "../../api/RestApi";
import {GenericAxiosResponse} from "axios";

interface AppProps {
}

interface AppState {
}

class SignInButton extends React.Component<AppProps, AppState> {
    async signIn(response: GoogleLoginResponse | GoogleLoginResponseOffline) {
        let tokensRespone: GenericAxiosResponse<TokensDto> | any;
        if (response.code !== undefined) {
            tokensRespone = await Api.authorise({
                authCode: response.code,
            });
        }

        const expDate = new Date(tokensRespone.data.accessTokenExpireDate);
        if (tokensRespone && tokensRespone.status === 200) {
            login(tokensRespone.data, expDate);
        }
    }

    handleLoginFailure() {
        alert("Failed to log in");
    }

    render() {
        return (
            <GoogleLogin
                clientId={environment.google_credentials.client_id}
                buttonText="Войти с помощью Google"
                onSuccess={this.signIn}
                onFailure={this.handleLoginFailure}
                cookiePolicy={"single_host_origin"}
                responseType="code"
                accessType="offline"
            />
        );
    }
}

export default SignInButton;
