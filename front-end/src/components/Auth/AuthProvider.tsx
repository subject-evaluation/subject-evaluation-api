import {useEffect, useState} from "react";
import {
    isLoggedIn,
    setAccessToken,
    setExpireDate,
    setRefreshToken,
    setRoles,
    subscribe,
    unsubscribe
} from "./TokenProvider";
import {TokensDto} from "../../api/RestApi";

export const createAuthProvider = () => {
    const tokenProvider = {
        isLoggedIn,
        setAccessToken,
        setRefreshToken,
        setExpireDate,
        subscribe,
        unsubscribe,
        setRoles,
    };

    const login = (token: TokensDto, accessTokenExpireDate: Date) => {
        tokenProvider.setRoles(token.roles);
        tokenProvider.setAccessToken(token.accessToken);
        tokenProvider.setRefreshToken(token.refreshToken);
        tokenProvider.setExpireDate(accessTokenExpireDate);
    };

    const logout = () => {
        tokenProvider.setRoles([]);
        tokenProvider.setAccessToken(null);
        tokenProvider.setRefreshToken(null);
        tokenProvider.setExpireDate(null);
    };

    const useAuth = () => {
        const [isLogged, setIsLogged] = useState(tokenProvider.isLoggedIn());

        useEffect(() => {
            const listener = (newIsLogged: boolean) => {
                setIsLogged(newIsLogged);
            };

            tokenProvider.subscribe(listener);
            return () => {
                tokenProvider.unsubscribe(listener);
            };
        }, []);

        return [isLogged] as [typeof isLogged];
    };

    return {
        useAuth,
        login,
        logout,
    };
};

export const {useAuth, login, logout} = createAuthProvider();
