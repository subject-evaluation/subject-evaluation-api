import React from "react";
import {logout} from "./AuthProvider";
import {environment} from "../../enviroment";
import {GoogleLogout} from "react-google-login";
import Api from "../../api/Api";
import {getToken} from "./TokenProvider";
import {toAuthorizationHeaderValue} from "../Utils";

const SignOutButton: React.FC = () => {
    const signOut = async () => {
        getToken().then(accessToken =>
            Api.logout({headers: {Authorization: toAuthorizationHeaderValue(accessToken)}})
                .then(() => logout())
        );
    };

    const handleLogoutFailure = () => {
        alert("Failed to log out");
    };

    return (
        <GoogleLogout
            clientId={environment.google_credentials.client_id}
            buttonText="Выйти"
            onLogoutSuccess={signOut}
            onFailure={handleLogoutFailure}
        />
    );
};

export default SignOutButton;
