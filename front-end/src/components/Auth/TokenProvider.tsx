import Api from "../../api/Api";
import {Role} from "../../api/RestApi";

const createTokenProvider = () => {
    let accessToken: string | null =
        localStorage.getItem("REACT_ACCESS_TOKEN_AUTH") || "" || null;
    let refreshToken: string | null =
        localStorage.getItem("REACT_REFRESH_TOKEN_AUTH") || "" || null;
    let accessTokenExpireDate: string | null =
        localStorage.getItem("REACT_EXPIRE_DATE") || "" || null;

    const isExpired = () => {
        let exp = 160554300196; // Feb 02 1975
        if (accessTokenExpireDate) {
            exp = parseInt(accessTokenExpireDate) * 1000;
        }
        return Date.now() > exp;
    };

    // Возвращает Promise<string> или null
    const getToken = async () => {
        if (!accessToken) {
            return null;
        }

        if (isExpired()) {
            if (refreshToken) {
                Api.refreshTokens({
                    refreshToken: refreshToken
                }).then((res) => {
                    setAccessToken(res.data.accessToken);
                    setExpireDate(new Date(res.data.accessTokenExpireDate));
                });
            }
        }

        return accessToken;
    };

    const isLoggedIn = () => {
        return !!accessToken;
    };

    let observers: Array<(isLogged: boolean) => void> = [];

    const subscribe = (observer: (isLogged: boolean) => void) => {
        observers.push(observer);
    };

    const unsubscribe = (observer: (isLogged: boolean) => void) => {
        observers = observers.filter((_observer) => _observer !== observer);
    };

    const notify = () => {
        const isLogged = isLoggedIn();
        observers.forEach((observer) => observer(isLogged));
    };

    const setAccessToken = (newToken: any) => {
        if (newToken) {
            localStorage.setItem("REACT_ACCESS_TOKEN_AUTH", newToken);
        } else {
            localStorage.removeItem("REACT_ACCESS_TOKEN_AUTH");
        }
        accessToken = newToken;
        notify();
    };

    const setRefreshToken = (newToken: any) => {
        if (newToken) {
            localStorage.setItem(
                "REACT_REFRESH_TOKEN_AUTH",
                newToken
            );
        } else {
            localStorage.removeItem("REACT_REFRESH_TOKEN_AUTH");
        }
        refreshToken = newToken;
    };

    const setExpireDate = (newDate: Date | null) => {
        if (newDate) {
            localStorage.setItem("REACT_EXPIRE_DATE", newDate.getTime().toString());
            accessTokenExpireDate = newDate.getTime().toString();
        } else {
            localStorage.removeItem("REACT_EXPIRE_DATE");
        }
    };

    const setRoles = (roles: Role[]) => {
        localStorage.setItem("ROLES", JSON.stringify(roles));
    }

    const hasSomeRoles = (...roles: Role[]): boolean => {
        const currentRoles: Role[] = JSON.parse(localStorage.getItem("ROLES") || '""') || [];
        return currentRoles.some(role => roles.includes(role));
    }

    return {
        getToken,
        isLoggedIn,
        setAccessToken,
        setRefreshToken,
        setExpireDate,
        subscribe,
        unsubscribe,
        setRoles,
        hasSomeRoles
    };
};

export const {
    getToken,
    isLoggedIn,
    setAccessToken,
    setRefreshToken,
    setExpireDate,
    subscribe,
    unsubscribe,
    setRoles,
    hasSomeRoles
} = createTokenProvider();
