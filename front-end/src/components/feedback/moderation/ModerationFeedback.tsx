import React from "react";
import {Card, CardContent, Divider, Typography} from "@material-ui/core";
import {makeStyles} from "@material-ui/core/styles";
import {FeedbackDto} from "../../../api/RestApi";
import {DateFormat, userInfo} from "../../Utils";
import FeedbackAction from "./FeedbackAction";

const useStyles = makeStyles({
    root: {
        margin: "auto",
        width: "80vw"
    },
    secondary: {
        fontWeight: 400,
    },
    divider: {
        marginBottom: '2vh'
    },
    feedback: {
        display: 'flex',
        justifyContent: 'space-between',
    },
    questions: {
        '& > * + *': {
            marginTop: '1vh'
        }
    }
});

interface InfoRowProps {
    title: string;
    content: string;
}

const InfoRow: React.FC<InfoRowProps> = ({title, content}) => {
    return (
        <Typography variant="h6" color="primary">
            {title}: <Typography variant="inherit" color="textPrimary">{content}</Typography>
        </Typography>
    )
}

const FeedbackRow: React.FC<InfoRowProps> = ({title, content}) => {
    return (
        <div>
            <Typography variant="h6" color="primary">{title}</Typography>
            <Typography variant="h6" color="textPrimary">{content}</Typography>
        </div>
    )
}

interface ModerationFeedbackProps {
    feedback: FeedbackDto;
    update: () => void;
}

const ModerationFeedback: React.FC<ModerationFeedbackProps> = ({feedback, update}) => {
    const classes = useStyles();
    return (
        <div className={classes.root}>
            <Card>
                <CardContent>
                    <InfoRow title="Курс" content={feedback.courseName}/>
                    <InfoRow title="Период проведения курса"
                             content={`${feedback.semester} семестр ${feedback.year} учебного года`}/>
                    <InfoRow title="Период проведения опроса"
                             content={`${DateFormat(feedback.startSurveyDate)} - ${DateFormat(feedback.endSurveyDate)}`}/>
                    <InfoRow title={feedback.teachers.length === 1 ? "Преподаватель" : "Преподаватели"}
                             content={feedback.teachers.map(teacher => userInfo(teacher)).join(", ")}/>
                </CardContent>
            </Card>
            <Divider/>
            <Card>
                <CardContent>
                    <InfoRow title="Автор" content={userInfo(feedback.author)}/>
                    <Divider className={classes.divider}/>
                    <div className={classes.feedback}>
                        <div className={classes.questions}>
                            {feedback.answers.map((answer, index) =>
                                <FeedbackRow key={index} title={`${index + 1}. ${answer.questionTitle}`}
                                             content={answer.answerText}/>)}
                        </div>
                        <FeedbackAction feedbackId={feedback.id} update={update}/>
                    </div>
                </CardContent>
            </Card>
        </div>
    )
}

export default ModerationFeedback;

