import React from "react";
import {Button} from "@material-ui/core";
import {makeStyles} from "@material-ui/core/styles";
import Api from "../../../api/Api";
import {FeedbackStatus} from "../../../api/RestApi";
import {getToken} from "../../Auth/TokenProvider";
import {toAuthorizationHeaderValue} from "../../Utils";

const useStyles = makeStyles({
    actions: {
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'flex-end',
        '& > * + *': {
            marginTop: '2vh'
        }
    }
});

interface FeedbackActionProps {
    feedbackId: number;
    update: () => void;
}

const FeedbackAction: React.FC<FeedbackActionProps> = ({feedbackId, update}) => {

    const updateStatus = (status: FeedbackStatus) => {
        getToken().then(accessToken => Api.updateStatus(feedbackId, {status},
            {headers: {Authorization: toAuthorizationHeaderValue(accessToken)}}))
            .then(update);
    }

    const classes = useStyles();
    return (
        <div className={classes.actions}>
            <Button variant="contained" color="primary" size="large"
                    onClick={() => updateStatus(FeedbackStatus.PUBLISHED)}>Опубликовать</Button>
            <Button variant="contained" color="secondary"
                    onClick={() => updateStatus(FeedbackStatus.DELETED)}>Удалить</Button>
        </div>
    )
}

export default FeedbackAction;
