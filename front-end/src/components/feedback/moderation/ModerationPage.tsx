import React, {ChangeEvent, useEffect, useState} from "react";
import {Button, TextField, Typography, useTheme} from "@material-ui/core";
import {KeyboardArrowLeft, KeyboardArrowRight} from "@material-ui/icons";
import {makeStyles} from "@material-ui/core/styles";
import {FeedbackDto, FeedbackStatus} from "../../../api/RestApi";
import Api from "../../../api/Api";
import {getToken} from "../../Auth/TokenProvider";
import {toAuthorizationHeaderValue} from "../../Utils";
import ModerationFeedback from "./ModerationFeedback";

const useStyles = makeStyles({
    paging: {
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        '& > * + *': {
            marginLeft: '2vw'
        }
    },
    root: {
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
        '& > * + *': {
            marginTop: '2vh'
        }
    },
    arrow: {
        fontSize: '1em'
    },
    page: {
        width: '70px'
    }
});

const ModerationPage: React.FC = () => {
    const [page, setPage] = useState<number | undefined>(0);
    const [totalElements, setTotalElements] = useState(0);
    const [feedback, setFeedback] = useState<FeedbackDto | undefined>(undefined);

    const update = () => {
        if (page !== undefined) {
            getToken()
                .then(accessToken =>
                    // @ts-ignore
                    Api.searchFeedbacks({statuses: [FeedbackStatus.SUBMITTED]}, {page: page, size: 1},
                        {headers: {Authorization: toAuthorizationHeaderValue(accessToken)}}))
                .then(response => {
                    setTotalElements(response.data.totalElements);
                    setFeedback(response.data.content[0]);
                })
        }
    }

    useEffect(update, [page])

    function onChangePage(event: ChangeEvent<HTMLInputElement | HTMLTextAreaElement>) {
        const newPage = +event.target.value;
        if (event.target.value.length === 0) {
            setPage(undefined);
        } else if (newPage < 1) {
            setPage(0);
        } else if (newPage > totalElements) {
            setPage(totalElements - 1);
        } else {
            setPage(newPage - 1);
        }
    }

    const theme = useTheme();
    const classes = useStyles();
    return (
        <div className={classes.root}>
            {feedback ? <ModerationFeedback feedback={feedback} update={update}/> :
                <Typography variant="h6">Пока нет отзывов</Typography>}
            <div className={classes.paging}>
                <Button className={classes.arrow} onClick={() => {
                    setPage(prev => prev! - 1)
                }} disabled={page === undefined || page <= 0}>
                    {theme.direction === 'rtl' ? <KeyboardArrowRight/> : <KeyboardArrowLeft/>}
                    Назад
                </Button>
                <TextField
                    className={classes.page}
                    type="number"
                    variant="outlined"
                    size="small"
                    value={page !== undefined ? page + 1 : ""}
                    onChange={onChangePage}
                />
                <Typography variant="h5">из</Typography>
                <Typography variant="h5">{totalElements}</Typography>
                <Button className={classes.arrow} onClick={() => {
                    setPage(prev => prev! + 1)
                }} disabled={page === undefined || page >= totalElements - 1}>
                    Вперед
                    {theme.direction === 'rtl' ? <KeyboardArrowLeft/> : <KeyboardArrowRight/>}
                </Button>
            </div>
        </div>
    )
}

export default ModerationPage;
