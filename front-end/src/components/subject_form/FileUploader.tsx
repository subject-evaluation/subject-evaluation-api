import React, {useState} from "react";
import {useDropzone} from "react-dropzone";
import ClearIcon from "@material-ui/icons/Clear";

function FileUploader() {
    const [files, setFiles] = useState([] as File[]);

    const onDrop = (acceptedFiles: File[]) => {
        setFiles([...files, ...acceptedFiles]);
    };

    const {getRootProps, getInputProps, open} = useDropzone({
        noClick: true,
        noKeyboard: true,
        onDrop: onDrop,
    });

    const dropzoneStyle = {
        width: "600px",
        height: "200px",
        border: "1px solid lightgrey",
        marginRight: "350px",
    };

    const removeFile = (file: File) => {
        const localFiles = [...files];
        localFiles.splice(localFiles.indexOf(file), 1);
        setFiles(localFiles);
    };

    return (
        <div>
            <div style={{marginLeft: "336px"}} className="div-tf">
                <label className="label">Программа</label>
                <div style={dropzoneStyle} {...getRootProps({className: "dropzone"})}>
                    <input {...getInputProps()} />
                    <span className="dropzone-text">
            Перетащите или
            <span className="link-elem" onClick={open}>
              {" "}
                выберите{" "}
            </span>
            файл
          </span>
                </div>
            </div>
            <div
                style={{width: "600px", textAlign: "left", marginTop: "20px"}}
                onClick={open}
            >
                {files.map((file) => (
                    <div className="file" key={"i"}>
            <span className="link-elem" key={file.name}>
              {file.name}
            </span>
                        <span> - {file.size} bytes</span>
                        <ClearIcon
                            className="cancel-load"
                            onClick={(e) => {
                                removeFile(file);
                                e.stopPropagation();
                            }}
                        >
                            Open File Dialog
                        </ClearIcon>
                    </div>
                ))}
            </div>
        </div>
    );
}

export default FileUploader;
