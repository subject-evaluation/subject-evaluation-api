import React from "react";
import {TextField} from "@material-ui/core";

interface BaseTextFieldProps {
    height: string;
    text: string;
    required: boolean;
    rows: number;
}

export const TextFieldComponent = ({
                                       text,
                                       height,
                                       required,
                                       rows,
                                   }: BaseTextFieldProps) => {
    const textFieldStyle = {
        width: 600,
        height: height,
    };

    const props = {
        style: textFieldStyle,
        multiline: true,
        variant: "outlined",
        rows: rows,
    };

    return (
        <div className="div-tf">
            <label className={"label"}>{text}</label>
            <TextField
                InputProps={props}
                required={required}
                variant="outlined"
            ></TextField>
        </div>
    );
};
