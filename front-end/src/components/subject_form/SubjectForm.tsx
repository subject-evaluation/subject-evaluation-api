import React from "react";
import {TextFieldComponent} from "./TextFieldComponent";
import "../styles/SubjectForm.css";
import {IconButton} from "@material-ui/core";
import {ArrowBackIos} from "@material-ui/icons";
import Button from "@material-ui/core/Button";
import FileUploader from "../subject_form/FileUploader";

interface SubjectFormProps {
    headerName: string;
}

const buttonStyle = {
    marginTop: "2%",
    marginLeft: "1%",
    width: "100px",
    height: "25px",
    fontSize: "8pt",
    left: "15%",
};

export const SubjectForm = ({headerName}: SubjectFormProps) => {
    return (
        <div className="main-container">
            <div className="header-container">
                <IconButton>
                    <ArrowBackIos/>
                </IconButton>
                <p className="header">{headerName}</p>
            </div>
            <TextFieldComponent
                rows={1}
                height={"40px"}
                required={true}
                text={"Название"}
            ></TextFieldComponent>
            <TextFieldComponent
                rows={300 / 21}
                height={"300px"}
                required={false}
                text={"Описание"}
            ></TextFieldComponent>
            <FileUploader/>
            <Button variant="outlined" style={buttonStyle}>
                Отмена
            </Button>
            <Button variant="contained" style={buttonStyle} color="primary">
                Сохранить
            </Button>
        </div>
    );
};
