import React from "react";
import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import Typography from "@material-ui/core/Typography";
import SignOutButton from "./Auth/SignOutButton";
import "./styles/Header.css";
import {Link} from "react-router-dom";
import {makeStyles} from "@material-ui/core/styles";
import {List, ListItem} from "@material-ui/core";
import {hasSomeRoles} from "./Auth/TokenProvider";
import {Role} from "../api/RestApi";

const useStyles = makeStyles({
    root: {
        marginBottom: '2vh'
    },
    link: {
        fontWeight: 500,
        fontSize: "1.25em",
        color: "#f8f5f2",
        textDecoration: "none",
        textAlign: "center",

        "&:hover": {
            textDecoration: "underline",
        },
    },
    list: {
        display: "flex",
        flexDirection: "row",
        justifyContent: "flex-start",
        '& > *': {
            width: 'auto'
        }
    },
});

const Header: React.FC = () => {
    const classes = useStyles();

    const isAllowedModeration = hasSomeRoles(Role.TEACHER, Role.EMPLOYEE);

    return (
        <AppBar position="static" className={classes.root}>
            <Toolbar>
                <Typography variant="h6" className="app-name">
                    Subject Evaluation
                </Typography>
                <List className={classes.list}>
                    <ListItem>
                        <Link className={classes.link} to="/courses">
                            Курсы
                        </Link>
                    </ListItem>
                    <ListItem>
                        <Link className={classes.link} to="/user-page">
                            Пользователи
                        </Link>
                    </ListItem>
                    {isAllowedModeration &&
                    <ListItem>
                        <Link className={classes.link} to="/feedback/moderate">
                            Модерация отзывов
                        </Link>
                    </ListItem>}
                    <ListItem>
                        <Link className={classes.link} to="/syrveys">
                            Опросы
                        </Link>
                    </ListItem>
                    <ListItem>
                        <SignOutButton/>
                    </ListItem>
                </List>
            </Toolbar>
        </AppBar>
    );
};

export default Header;
