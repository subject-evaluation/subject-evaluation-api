/* tslint:disable */

/* eslint-disable */

export interface Error {
    errorMessage: string;
}

export interface ValidationError extends Error {
    fieldErrors: { [index: string]: string };
    constraintViolations: string[];
}

/**
 * Код авторизации google sign-in
 */
export interface AuthCodeDto {
    authCode: string;
}

/**
 * Обертка для refresh токена
 */
export interface RefreshTokenDto {
    refreshToken: string;
}

/**
 * Токен авторизации
 */
export interface TokensDto {
    roles: Role[];
    accessToken: string;
    refreshToken: string;
    accessTokenExpireDate: number;
}

export interface CourseRequestDto {
    name: string;
    description: string;
    level: CourseLevel;
    status: CourseStatus;
}

export interface CourseResponseDto {
    id: number;
    name: string;
    description: string;
    level: CourseLevel;
    status: CourseStatus;
}

export interface FeedbackCreateDto {
    creationDate: number;
    publishingDate: number;
    status: FeedbackStatus;
    userId: number;
    surveyId: number;
    userAnswers: CreateAnswer[];
}

export interface FeedbackDto {
    id: number;
    creationDate: number;
    publishingDate: number;
    courseName: string;
    year: string;
    semester: number;
    feedbackStatus: FeedbackStatus;
    author: UserDto;
    teachers: UserDto[];
    answers: Answer[];
    startSurveyDate: number;
    endSurveyDate: number;
}

export interface FeedbackFilter {
    courseIds: number[];
    statuses: FeedbackStatus[];
}

export interface FeedbackStatusUpdateDto {
    status: FeedbackStatus;
}

export interface FeedbackUpdateDto {
    creationDate: number;
    publishingDate: number;
    status: FeedbackStatus;
    userId: number;
    surveyId: number;
    userAnswers: CreateAnswer[];
}

export interface FileCreateDto {
    name: string;
    mimeType: string;
    type: FileType;
    url: string;
    content: any;
}

export interface FileResponseDto {
    id: number;
    name: string;
    mimeType: string;
    type: FileType;
    url: string;
    content: any;
}

export interface CourseFilter {
    name: string;
    statuses: CourseStatus[];
    levels: CourseLevel[];
}

export interface SurveyFilter {
    courseId: number;
    studentId: number;
}

export interface UserFilter {
    statuses: UserStatus[];
    roles: Role[];
    line: string;
}

export interface AnswerChoiceRequestDto {
    title: string;
}

export interface AnswerChoiceResponseDto {
    id: number;
    title: string;
}

export interface AnswerChoiceUpdateDto {
    id: number;
    title: string;
}

export interface QuestionRequestDto {
    type: QuestionType;
    title: string;
    answerChoices: AnswerChoiceRequestDto[];
}

export interface QuestionResponseDto {
    id: number;
    type: QuestionType;
    title: string;
    answerChoices: AnswerChoiceResponseDto[];
}

export interface QuestionUpdateDto {
    id: number;
    type: QuestionType;
    title: string;
    answerChoices: AnswerChoiceUpdateDto[];
}

export interface SurveyRequestDto {
    courseId: number;
    authorId: number;
    startDate: Date;
    endDate: Date;
    year: string;
    semester: number;
    studentIds: number[];
    teacherIds: number[];
    questions: QuestionRequestDto[];
}

export interface SurveyResponseDto {
    id: number;
    courseId: number;
    authorId: number;
    startDate: string;
    endDate: string;
    year: string;
    semester: number;
    studentIds: number[];
    teacherIds: number[];
    questions: QuestionResponseDto[];
}

export interface SurveyUpdateDto {
    authorId: number;
    courseId: number;
    startDate: Date;
    endDate: Date;
    year: string;
    semester: number;
    studentIds: number[];
    teacherIds: number[];
}

export interface GoogleUser {
    email: string;
    firstName: string;
    lastName: string;
    googleId: string;
    accessToken: string;
    refreshToken: string;
    accessTokenExpireDate: number;
}

export interface UserRequestDto {
    email: string;
    firstName: string;
    lastName: string;
    patronymic: string;
    groupNumber: string;
    status: UserStatus;
    roles: Role[];
}

export interface UserResponseDto {
    id: number;
    lastName: string;
    firstName: string;
    patronymic: string;
    email: string;
    roles: Role[];
    groupNumber: string;
    status: UserStatus;
}

export interface StudentGroupNumberValidator extends ConstraintValidator<ValidStudentGroupNumber, UserRequestDto> {
}

export interface CreateAnswer {
    questionId: number;
    answerText: string;
    answerChoiceId: number[];
}

export interface UserDto {
    firstName: string;
    lastName: string;
    patronymic: string;
    email: string;
}

export interface Answer {
    answerText: string;
    questionId: number;
    questionTitle: string;
    answerChoice: string[];
}

export interface Page<T> extends Slice<T> {
    totalPages: number;
    totalElements: number;
}

export interface ConstraintValidator<A, T> {
}

export interface ValidStudentGroupNumber extends Annotation {
}

export interface Sort extends Streamable<Order>, Serializable {
    unsorted: boolean;
    sorted: boolean;
}

export interface Pageable {
    offset: number;
    sort: Sort;
    paged: boolean;
    unpaged: boolean;
    pageNumber: number;
    pageSize: number;
}

export interface Annotation {
}

export interface Serializable {
}

export interface Slice<T> extends Streamable<T> {
    size: number;
    content: T[];
    number: number;
    sort: Sort;
    first: boolean;
    last: boolean;
    numberOfElements: number;
    pageable: Pageable;
}

export interface Streamable<T> extends Iterable<T>, Supplier<Stream<T>> {
    empty: boolean;
}

export interface Order extends Serializable {
    direction: Direction;
    property: string;
    ignoreCase: boolean;
    nullHandling: NullHandling;
    ascending: boolean;
    descending: boolean;
}

export interface Iterable<T> {
}

export interface Supplier<T> {
}

export interface Stream<T> extends BaseStream<T, Stream<T>> {
}

export interface BaseStream<T, S> extends AutoCloseable {
    parallel: boolean;
}

export interface AutoCloseable {
}

export interface HttpClient<O> {

    request<R>(requestConfig: { method: string; url: string; queryParams?: any; data?: any; copyFn?: (data: R) => R; options?: O; }): RestResponse<R>;
}

export class RestApplicationClient<O> {

    constructor(protected httpClient: HttpClient<O>) {
    }

    /**
     * HTTP POST /api/v1/auth
     * Java method: ru.nsu.fit.controller.AuthorizationController.authorise
     */
    authorise(authCodeDto: AuthCodeDto, options?: O): RestResponse<TokensDto> {
        return this.httpClient.request({
            method: "POST",
            url: uriEncoding`api/v1/auth`,
            data: authCodeDto,
            options: options
        });
    }

    /**
     * HTTP POST /api/v1/files/save
     * Java method: ru.nsu.fit.controller.FileController.createFile
     */
    createFile(fileCreateDto: FileCreateDto, options?: O): RestResponse<number> {
        return this.httpClient.request({
            method: "POST",
            url: uriEncoding`api/v1/files/save`,
            data: fileCreateDto,
            options: options
        });
    }

    /**
     * HTTP POST /api/v1/files/upload
     * Java method: ru.nsu.fit.controller.FileController.uploadFile
     */
    uploadFile(file: File, queryParams: { fileType: FileType; }, options?: O): RestResponse<FileResponseDto> {
        return this.httpClient.request({
            method: "POST",
            url: uriEncoding`api/v1/files/upload`,
            queryParams: queryParams,
            data: file,
            options: options
        });
    }

    /**
     * HTTP GET /api/v1/files/{id}
     * Java method: ru.nsu.fit.controller.FileController.getFile
     */
    getFile(id: number, options?: O): RestResponse<FileResponseDto> {
        return this.httpClient.request({method: "GET", url: uriEncoding`api/v1/files/${id}`, options: options});
    }

    /**
     * HTTP GET /api/v1/files/{id}/download
     * Java method: ru.nsu.fit.controller.FileController.downloadFile
     */
    downloadFile(id: number, options?: O): RestResponse<any> {
        return this.httpClient.request({
            method: "GET",
            url: uriEncoding`api/v1/files/${id}/download`,
            options: options
        });
    }

    /**
     * HTTP POST /api/v1/logout
     * Java method: ru.nsu.fit.controller.AuthorizationController.logout
     */
    logout(options?: O): RestResponse<void> {
        return this.httpClient.request({method: "POST", url: uriEncoding`api/v1/logout`, options: options});
    }

    /**
     * HTTP POST /api/v1/refresh
     * Java method: ru.nsu.fit.controller.AuthorizationController.refreshTokens
     */
    refreshTokens(tokenDto: RefreshTokenDto, options?: O): RestResponse<TokensDto> {
        return this.httpClient.request({
            method: "POST",
            url: uriEncoding`api/v1/refresh`,
            data: tokenDto,
            options: options
        });
    }

    /**
     * HTTP POST /api/v1/secure/courses
     * Java method: ru.nsu.fit.controller.CourseController.createCourse
     */
    createCourse(course: CourseRequestDto, options?: O): RestResponse<CourseResponseDto> {
        return this.httpClient.request({
            method: "POST",
            url: uriEncoding`api/v1/secure/courses`,
            data: course,
            options: options
        });
    }

    /**
     * HTTP GET /api/v1/secure/courses/all-ids
     * Java method: ru.nsu.fit.controller.CourseController.getAllCourseIds
     */
    getAllCourseIds(options?: O): RestResponse<number[]> {
        return this.httpClient.request({
            method: "GET",
            url: uriEncoding`api/v1/secure/courses/all-ids`,
            options: options
        });
    }

    /**
     * HTTP POST /api/v1/secure/courses/archive
     * Java method: ru.nsu.fit.controller.CourseController.archiveCourses
     */
    archiveCourses(idCourses: number[], options?: O): RestResponse<void> {
        return this.httpClient.request({
            method: "POST",
            url: uriEncoding`api/v1/secure/courses/archive`,
            data: idCourses,
            options: options
        });
    }

    /**
     * HTTP PUT /api/v1/secure/courses/search
     * Java method: ru.nsu.fit.controller.CourseController.searchCourses
     */
    searchCourses(filter: CourseFilter, queryParams?: { page?: number; size?: number; sort?: string; }, options?: O): RestResponse<Page<CourseResponseDto>> {
        return this.httpClient.request({
            method: "PUT",
            url: uriEncoding`api/v1/secure/courses/search`,
            queryParams: queryParams,
            data: filter,
            options: options
        });
    }

    /**
     * HTTP GET /api/v1/secure/courses/status/{courseIds}
     * Java method: ru.nsu.fit.controller.CourseController.defineCourseStatus
     */
    defineCourseStatus(courseIds: number[], options?: O): RestResponse<CourseStatus> {
        return this.httpClient.request({
            method: "GET",
            url: uriEncoding`api/v1/secure/courses/status/${courseIds}`,
            options: options
        });
    }

    /**
     * HTTP POST /api/v1/secure/courses/unarchive
     * Java method: ru.nsu.fit.controller.CourseController.unarchiveCourses
     */
    unarchiveCourses(idCourses: number[], options?: O): RestResponse<void> {
        return this.httpClient.request({
            method: "POST",
            url: uriEncoding`api/v1/secure/courses/unarchive`,
            data: idCourses,
            options: options
        });
    }

    /**
     * HTTP GET /api/v1/secure/courses/{id}
     * Java method: ru.nsu.fit.controller.CourseController.getCourse
     */
    getCourse(id: number, options?: O): RestResponse<CourseResponseDto> {
        return this.httpClient.request({
            method: "GET",
            url: uriEncoding`api/v1/secure/courses/${id}`,
            options: options
        });
    }

    /**
     * HTTP PUT /api/v1/secure/courses/{id}
     * Java method: ru.nsu.fit.controller.CourseController.updateCourse
     */
    updateCourse(id: number, course: CourseRequestDto, options?: O): RestResponse<CourseResponseDto> {
        return this.httpClient.request({
            method: "PUT",
            url: uriEncoding`api/v1/secure/courses/${id}`,
            data: course,
            options: options
        });
    }

    /**
     * HTTP POST /api/v1/secure/feedback/create-feedback
     * Java method: ru.nsu.fit.controller.FeedbackController.createFeedback
     */
    createFeedback(queryParams: { feedbackCreateDto: FeedbackCreateDto; }, options?: O): RestResponse<number> {
        return this.httpClient.request({
            method: "POST",
            url: uriEncoding`api/v1/secure/feedback/create-feedback`,
            queryParams: queryParams,
            options: options
        });
    }

    /**
     * HTTP GET /api/v1/secure/feedback/get-feedback
     * Java method: ru.nsu.fit.controller.FeedbackController.getFeedback
     */
    getFeedback(queryParams: { feedbackFilter: FeedbackFilter; page?: number; size?: number; sort?: string; }, options?: O): RestResponse<Page<FeedbackDto>> {
        return this.httpClient.request({
            method: "GET",
            url: uriEncoding`api/v1/secure/feedback/get-feedback`,
            queryParams: queryParams,
            options: options
        });
    }

    /**
     * HTTP GET /api/v1/secure/feedback/get-user-feedback
     * Java method: ru.nsu.fit.controller.FeedbackController.getUserFeedback
     */
    getUserFeedback(queryParams: { userId: number; surveyId: number; }, options?: O): RestResponse<FeedbackDto> {
        return this.httpClient.request({
            method: "GET",
            url: uriEncoding`api/v1/secure/feedback/get-user-feedback`,
            queryParams: queryParams,
            options: options
        });
    }

    /**
     * HTTP PUT /api/v1/secure/feedback/search
     * Java method: ru.nsu.fit.controller.FeedbackController.searchFeedbacks
     */
    searchFeedbacks(feedbackFilter: FeedbackFilter, queryParams?: { page?: number; size?: number; sort?: string; }, options?: O): RestResponse<Page<FeedbackDto>> {
        return this.httpClient.request({
            method: "PUT",
            url: uriEncoding`api/v1/secure/feedback/search`,
            queryParams: queryParams,
            data: feedbackFilter,
            options: options
        });
    }

    /**
     * HTTP POST /api/v1/secure/feedback/update-feedback
     * Java method: ru.nsu.fit.controller.FeedbackController.updateFeedback
     */
    updateFeedback(queryParams: { feedbackUpdateDto: FeedbackUpdateDto; }, options?: O): RestResponse<FeedbackDto> {
        return this.httpClient.request({
            method: "POST",
            url: uriEncoding`api/v1/secure/feedback/update-feedback`,
            queryParams: queryParams,
            options: options
        });
    }

    /**
     * HTTP POST /api/v1/secure/feedback/update-status/{feedbackId}
     * Java method: ru.nsu.fit.controller.FeedbackController.updateStatus
     */
    updateStatus(feedbackId: number, body: FeedbackStatusUpdateDto, options?: O): RestResponse<void> {
        return this.httpClient.request({
            method: "POST",
            url: uriEncoding`api/v1/secure/feedback/update-status/${feedbackId}`,
            data: body,
            options: options
        });
    }

    /**
     * HTTP POST /api/v1/secure/survey
     * Java method: ru.nsu.fit.controller.SurveyController.createSurvey
     */
    createSurvey(surveyRequestDto: SurveyRequestDto, options?: O): RestResponse<SurveyResponseDto> {
        return this.httpClient.request({
            method: "POST",
            url: uriEncoding`api/v1/secure/survey`,
            data: surveyRequestDto,
            options: options
        });
    }

    /**
     * HTTP PUT /api/v1/secure/survey/questions/{id}
     * Java method: ru.nsu.fit.controller.SurveyController.updateSurveyQuestions
     */
    updateSurveyQuestions(id: number, questions: QuestionUpdateDto[], options?: O): RestResponse<QuestionResponseDto[]> {
        return this.httpClient.request({
            method: "PUT",
            url: uriEncoding`api/v1/secure/survey/questions/${id}`,
            data: questions,
            options: options
        });
    }

    /**
     * HTTP POST /api/v1/secure/survey/search
     * Java method: ru.nsu.fit.controller.SurveyController.searchSurveys
     */
    searchSurveys(filter: SurveyFilter, queryParams?: { page?: number; size?: number; sort?: string; }, options?: O): RestResponse<Page<SurveyResponseDto>> {
        return this.httpClient.request({
            method: "POST",
            url: uriEncoding`api/v1/secure/survey/search`,
            queryParams: queryParams,
            data: filter,
            options: options
        });
    }

    /**
     * HTTP GET /api/v1/secure/survey/{id}
     * Java method: ru.nsu.fit.controller.SurveyController.getSurvey
     */
    getSurvey(id: number, options?: O): RestResponse<SurveyResponseDto> {
        return this.httpClient.request({method: "GET", url: uriEncoding`api/v1/secure/survey/${id}`, options: options});
    }

    /**
     * HTTP PUT /api/v1/secure/survey/{id}
     * Java method: ru.nsu.fit.controller.SurveyController.updateSurvey
     */
    updateSurvey(id: number, surveyRequestDto: SurveyUpdateDto, options?: O): RestResponse<SurveyResponseDto> {
        return this.httpClient.request({
            method: "PUT",
            url: uriEncoding`api/v1/secure/survey/${id}`,
            data: surveyRequestDto,
            options: options
        });
    }

    /**
     * HTTP POST /api/v1/secure/users
     * Java method: ru.nsu.fit.controller.UserController.createUser
     */
    createUser(userRequestDto: UserRequestDto, options?: O): RestResponse<UserResponseDto> {
        return this.httpClient.request({
            method: "POST",
            url: uriEncoding`api/v1/secure/users`,
            data: userRequestDto,
            options: options
        });
    }

    /**
     * HTTP POST /api/v1/secure/users/activate
     * Java method: ru.nsu.fit.controller.UserController.activateUsers
     */
    activateUsers(idUsers: number[], options?: O): RestResponse<void> {
        return this.httpClient.request({
            method: "POST",
            url: uriEncoding`api/v1/secure/users/activate`,
            data: idUsers,
            options: options
        });
    }

    /**
     * HTTP POST /api/v1/secure/users/block
     * Java method: ru.nsu.fit.controller.UserController.blockUsers
     */
    blockUsers(idUsers: number[], options?: O): RestResponse<void> {
        return this.httpClient.request({
            method: "POST",
            url: uriEncoding`api/v1/secure/users/block`,
            data: idUsers,
            options: options
        });
    }

    /**
     * HTTP PUT /api/v1/secure/users/search
     * Java method: ru.nsu.fit.controller.UserController.searchUsers
     */
    searchUsers(filter: UserFilter, queryParams?: { page?: number; size?: number; sort?: string; }, options?: O): RestResponse<Page<UserResponseDto>> {
        return this.httpClient.request({
            method: "PUT",
            url: uriEncoding`api/v1/secure/users/search`,
            queryParams: queryParams,
            data: filter,
            options: options
        });
    }

    /**
     * HTTP GET /api/v1/secure/users/{id}
     * Java method: ru.nsu.fit.controller.UserController.getUser
     */
    getUser(id: number, options?: O): RestResponse<UserResponseDto> {
        return this.httpClient.request({method: "GET", url: uriEncoding`api/v1/secure/users/${id}`, options: options});
    }

    /**
     * HTTP PUT /api/v1/secure/users/{userId}
     * Java method: ru.nsu.fit.controller.UserController.updateUser
     */
    updateUser(userId: number, userRequestDto: UserRequestDto, options?: O): RestResponse<UserResponseDto> {
        return this.httpClient.request({
            method: "PUT",
            url: uriEncoding`api/v1/secure/users/${userId}`,
            data: userRequestDto,
            options: options
        });
    }
}

export type RestResponse<R> = Promise<Axios.GenericAxiosResponse<R>>;

export enum CourseStatus {
    ACTIVE = "ACTIVE",
    ARCHIVED = "ARCHIVED",
}

export enum FileType {
    COURSE_PROGRAM = "COURSE_PROGRAM",
}

export enum Role {
    STUDENT = "STUDENT",
    TEACHER = "TEACHER",
    EMPLOYEE = "EMPLOYEE",
}

export enum CourseLevel {
    UNDERGRADUATE = "UNDERGRADUATE",
    MAGISTRACY = "MAGISTRACY",
    POSTGRADUATE = "POSTGRADUATE",
}

export enum FeedbackStatus {
    DRAFT = "DRAFT",
    SUBMITTED = "SUBMITTED",
    PUBLISHED = "PUBLISHED",
    DELETED = "DELETED",
}

export enum UserStatus {
    ACTIVE = "ACTIVE",
    BLOCKED = "BLOCKED",
}

export enum QuestionType {
    OPEN = "OPEN",
    SINGLE_OPTION = "SINGLE_OPTION",
    MULTIPLE_OPTIONS = "MULTIPLE_OPTIONS",
}

export enum Direction {
    ASC = "ASC",
    DESC = "DESC",
}

export enum NullHandling {
    NATIVE = "NATIVE",
    NULLS_FIRST = "NULLS_FIRST",
    NULLS_LAST = "NULLS_LAST",
}

function uriEncoding(template: TemplateStringsArray, ...substitutions: any[]): string {
    let result = "";
    for (let i = 0; i < substitutions.length; i++) {
        result += template[i];
        result += encodeURIComponent(substitutions[i]);
    }
    result += template[template.length - 1];
    return result;
}


// Added by 'AxiosClientExtension' extension

import axios, * as Axios from "axios";

declare module "axios" {
    export interface GenericAxiosResponse<R> extends Axios.AxiosResponse {
        data: R;
    }
}

class AxiosHttpClient implements HttpClient<Axios.AxiosRequestConfig> {

    constructor(private axios: Axios.AxiosInstance) {
    }

    request<R>(requestConfig: { method: string; url: string; queryParams?: any; data?: any; copyFn?: (data: R) => R; options?: Axios.AxiosRequestConfig; }): RestResponse<R> {
        function assign(target: any, source?: any) {
            if (source != undefined) {
                for (const key in source) {
                    if (source.hasOwnProperty(key)) {
                        target[key] = source[key];
                    }
                }
            }
            return target;
        }

        const config: Axios.AxiosRequestConfig = {};
        config.method = requestConfig.method as typeof config.method;  // `string` in axios 0.16.0, `Method` in axios 0.19.0
        config.url = requestConfig.url;
        config.params = requestConfig.queryParams;
        config.data = requestConfig.data;
        assign(config, requestConfig.options);
        const copyFn = requestConfig.copyFn;

        const axiosResponse = this.axios.request(config);
        return axiosResponse.then(axiosResponse => {
            if (copyFn && axiosResponse.data) {
                (axiosResponse as any).originalData = axiosResponse.data;
                axiosResponse.data = copyFn(axiosResponse.data);
            }
            return axiosResponse;
        });
    }
}

export class AxiosRestApplicationClient extends RestApplicationClient<Axios.AxiosRequestConfig> {

    constructor(baseURL: string, axiosInstance: Axios.AxiosInstance = axios.create()) {
        axiosInstance.defaults.baseURL = baseURL;
        super(new AxiosHttpClient(axiosInstance));
    }
}
