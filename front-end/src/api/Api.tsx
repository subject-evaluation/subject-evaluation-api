import {AxiosRestApplicationClient} from "./RestApi";

const API_ENDPOINT = process.env.REACT_APP_API_ENDPOINT;

const Api = new AxiosRestApplicationClient(API_ENDPOINT || "/api");

export default Api;
