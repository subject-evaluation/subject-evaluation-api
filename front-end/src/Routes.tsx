import React from "react";
import {BrowserRouter, Redirect, Route, Switch} from "react-router-dom";
import {useAuth} from "./components/Auth/AuthProvider";
import Authorization from "./components/pages/Authorization";
import Home from "./components/pages/Home";
import UsersPage from "./components/pages/Users/UsersPage";
import CreateUser from "./components/pages/Users/CreateUser";
import ViewCourse, {about} from "./components/pages/ViewCourse";
import CourseForm from "./components/course_form/CourseForm";
import CoursesPage from "./components/course/CoursesPage";
import Surveys from "./components/surveys/Surveys";
import SurveyForm from "./components/surveys/SurveyForm";
import ModerationPage from "./components/feedback/moderation/ModerationPage";

const Routes: React.FC = () => {
    const [logged] = useAuth();

    return (
        <BrowserRouter>
            <Switch>
                {!logged && (
                    <>
                        <Route exact path="/auth" component={Authorization}/>
                        <Redirect to="/auth"/>
                    </>
                )}
                {logged && (
                    <Home>
                        <Switch>
                            <Route exact path="/courses" component={CoursesPage}/>
                            <Route exact path="/courses/new_course" render={(props) => (
                                <CourseForm headerName={"Создание курса"} courseId={0} {...props}/>)}
                            />
                            <Route exact path="/courses/:id"
                                   render={(props) => <ViewCourse about={about.Description}
                                                                  courseId={props.match.params.id}/>}
                            />
                            <Route path="/courses/update_course/:id" render={(props) => (
                                <CourseForm headerName={"Изменение курса"}
                                            courseId={props.match.params.id} {...props}/>)}
                            />
                        </Switch>
                        <Route path="/user-page" component={UsersPage}/>
                        <Route path="/syrveys" component={Surveys}/>
                        <Route path="/new-survey" component={SurveyForm}/>
                        <Route
                            path="/courses/:id"
                            render={(props) => <ViewCourse about={about.Description} courseId={props.match.params.id}/>}
                        />
                        <Route path="/feedback/moderate" component={ModerationPage}/>
                        <Route path="/user-page" component={UsersPage}/>
                        <Route path="/create-user" component={CreateUser}/>
                        <Redirect to="/courses"/>
                    </Home>
                )}
            </Switch>
        </BrowserRouter>
    );
};

export default Routes;
