package ru.nsu.fit.service.email;

import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import ru.nsu.fit.AbstractContextualTest;

import static java.util.Collections.singletonList;


class EmailServiceImplTest extends AbstractContextualTest {

    private EmailService emailService;

    @Autowired
    private JavaMailSender mailSender;

    @Autowired
    private Environment environment;

    private final static String EMAIL = "subject.evaluation@gmail.com";
    private final static String MESSAGE_SUBJECT = "hello from integration test";
    private final static String MESSAGE_TEXT = "text from integration test";

    @Test
    void sendMessage() {
        emailService = new EmailServiceImpl(mailSender, environment);

        emailService.sendMessage(singletonList(EMAIL), MESSAGE_SUBJECT, MESSAGE_TEXT);

        SimpleMailMessage mailMessage = new SimpleMailMessage();
        mailMessage.setFrom(EMAIL);
        mailMessage.setTo(EMAIL);
        mailMessage.setSubject(MESSAGE_SUBJECT);
        mailMessage.setText(MESSAGE_TEXT);

        Mockito.verify(mailSender, Mockito.times(1)).send(mailMessage);
    }
}