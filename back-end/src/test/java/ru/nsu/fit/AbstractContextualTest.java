package ru.nsu.fit;

import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DbUnitConfiguration;
import com.github.springtestdbunit.dataset.ReplacementDataSetLoader;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.web.servlet.MockMvc;
import ru.nsu.fit.configuration.IntegrationTestConfiguration;
import ru.nsu.fit.utils.CleanDatabaseListener;

@ExtendWith(SpringExtension.class)
@SpringBootTest(
    classes = IntegrationTestConfiguration.class,
    webEnvironment = SpringBootTest.WebEnvironment.MOCK
)
@AutoConfigureMockMvc
@TestPropertySource(locations = "classpath:application-integration-test.properties")
@ComponentScan({
    "ru.nsu.fit.controller",
    "ru.nsu.fit.dao",
    "ru.nsu.fit.events",
    "ru.nsu.fit.facade",
    "ru.nsu.fit.mapper",
    "ru.nsu.fit.service",
    "ru.nsu.fit.specification",
})
@DbUnitConfiguration(dataSetLoader = ReplacementDataSetLoader.class)
@TestExecutionListeners({
    CleanDatabaseListener.class,
    DependencyInjectionTestExecutionListener.class,
    DbUnitTestExecutionListener.class,
})
public class AbstractContextualTest {
    @Autowired
    protected MockMvc mockMvc;
}
