package ru.nsu.fit.utils;

import java.util.Collection;
import java.util.List;

import javax.annotation.Nonnull;

import com.github.springtestdbunit.annotation.DatabaseSetup;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.annotation.AnnotationUtils;
import org.springframework.test.context.TestContext;
import org.springframework.test.context.support.AbstractTestExecutionListener;

@Slf4j
public class CleanDatabaseListener extends AbstractTestExecutionListener {
    @Override
    public void beforeTestMethod(final TestContext testContext) {
        DatabaseSetup methodDataSet = testContext.getTestMethod().getAnnotation(DatabaseSetup.class);
        DatabaseSetup classDataSet = AnnotationUtils.findAnnotation(testContext.getTestClass(), DatabaseSetup.class);
        if (methodDataSet != null || classDataSet != null) {
            getDatabaseCleaners(testContext).forEach(DatabaseCleaner::clearDatabase);
        }
    }

    @Nonnull
    private Collection<DatabaseCleaner> getDatabaseCleaners(TestContext testContext) {
        try {
            return testContext.getApplicationContext().getBeansOfType(DatabaseCleaner.class).values();
        } catch (Exception e) {
            log.error("Error occurred during DatabaseCleaner retrieval ", e);
            return List.of();
        }
    }
}
