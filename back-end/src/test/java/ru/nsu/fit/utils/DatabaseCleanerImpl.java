package ru.nsu.fit.utils;

import java.util.Objects;
import java.util.stream.Collectors;

import javax.annotation.Nonnull;
import javax.persistence.EntityManager;
import javax.persistence.Table;
import javax.persistence.metamodel.Metamodel;
import javax.persistence.metamodel.Type;
import javax.transaction.Transactional;

import lombok.RequiredArgsConstructor;

@Transactional
@RequiredArgsConstructor
public class DatabaseCleanerImpl implements DatabaseCleaner {
    private final EntityManager entityManager;

    @Override
    public void clearDatabase() {
        Metamodel metamodel = entityManager.getMetamodel();

        String query = metamodel.getManagedTypes()
            .stream()
            .map(Type::getJavaType)
            .map(c -> c.getAnnotation(Table.class))
            .filter(Objects::nonNull)
            .map(this::getTableName)
            .collect(Collectors.joining(", ", "TRUNCATE TABLE ", " RESTART IDENTITY CASCADE"));

        entityManager.createNativeQuery(query).executeUpdate();
    }

    @Nonnull
    private String getTableName(Table table) {
        return table.schema().isEmpty()
            ? table.name()
            : String.join(".", table.schema(), table.name());
    }
}
