package ru.nsu.fit.utils;

public interface DatabaseCleaner {
    /**
     * Очистить базу данных.
     */
    void clearDatabase();
}
