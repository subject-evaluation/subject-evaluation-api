package ru.nsu.fit.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.github.springtestdbunit.annotation.ExpectedDatabase;
import com.github.springtestdbunit.assertion.DatabaseAssertionMode;
import org.apache.commons.lang3.tuple.Triple;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import ru.nsu.fit.AbstractContextualTest;
import ru.nsu.fit.dto.course.CourseRequestDto;
import ru.nsu.fit.dto.filter.CourseFilter;
import ru.nsu.fit.entity.enums.CourseLevel;
import ru.nsu.fit.entity.enums.CourseStatus;

import javax.annotation.Nonnull;
import java.io.IOException;
import java.util.EnumSet;
import java.util.Set;
import java.util.function.UnaryOperator;
import java.util.stream.Stream;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static ru.nsu.fit.util.paths.CoursePaths.COURSES_ROOT_PATH;
import static ru.nsu.fit.utils.IntegrationTestUtils.jsonContent;

@DisplayName("Тесты курсов")
@DatabaseSetup(value = "/controller/course/before/setup.xml")
public class CourseControllerTest extends AbstractContextualTest {
    private static final String GET_COURSE_BY_ID = COURSES_ROOT_PATH + "/{id}";
    private static final String SEARCH_COURSES = COURSES_ROOT_PATH + "/search";
    private static final String UPDATE_COURSE = COURSES_ROOT_PATH + "/{id}";
    private static final String ARCHIVE_COURSE = COURSES_ROOT_PATH + "/archive";
    private static final String UNARCHIVE_COURSE = COURSES_ROOT_PATH + "/unarchive";
    private static final int EXISTING_ID = 101;
    private static final int ABSENT_ID = 100;

    @Autowired
    protected ObjectMapper objectMapper;

    @Nonnull
    private static Stream<Arguments> validateArguments() {
        return Stream.<Triple<String, String, UnaryOperator<CourseRequestDto>>>of(
            Triple.of("name", "must not be blank", c -> c.setName(null)),
            Triple.of("name", "must not be blank", c -> c.setName("")),
            Triple.of("name", "must not be blank", c -> c.setName(" \t \n\r")),
            Triple.of("level", "must not be null", c -> c.setLevel(null)),
            Triple.of("status", "must not be null", c -> c.setStatus(null))
        )
            .map(t -> Arguments.of(t.getLeft(), t.getMiddle(), t.getRight()));
    }

    @Test
    @DisplayName("Успешное получение курса по идентификатору")
    @ExpectedDatabase(
        value = "/controller/course/before/setup.xml",
        assertionMode = DatabaseAssertionMode.NON_STRICT_UNORDERED
    )
    public void getCourse() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get(GET_COURSE_BY_ID, EXISTING_ID))
            .andExpect(status().isOk())
            .andExpect(jsonContent("controller/course/response/get_id101.json"));
    }

    @Test
    @DisplayName("Получение курса по несуществующему идентификатору")
    @ExpectedDatabase(
        value = "/controller/course/before/setup.xml",
        assertionMode = DatabaseAssertionMode.NON_STRICT_UNORDERED
    )
    public void getNotExistingCourse() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get(GET_COURSE_BY_ID, ABSENT_ID))
            .andExpect(status().isNotFound())
            .andExpect(jsonContent("controller/course/response/not_existing_course.json"));
    }

    @Test
    @DisplayName("Поиск курсов по заданным параметрам")
    @ExpectedDatabase(
        value = "/controller/course/before/setup.xml",
        assertionMode = DatabaseAssertionMode.NON_STRICT_UNORDERED
    )
    public void searchCourses() throws Exception {
        mockMvc.perform(
            MockMvcRequestBuilders.put(SEARCH_COURSES)
                .contentType(MediaType.APPLICATION_JSON)
                .content(toJson(courseFilter()))
        )
            .andExpect(status().isOk())
            .andExpect(jsonContent("controller/course/response/search_all.json"));
    }

    @Test
    @DisplayName("Изменение курса")
    @ExpectedDatabase(
        value = "/controller/course/after/update_id1.xml",
        assertionMode = DatabaseAssertionMode.NON_STRICT_UNORDERED
    )
    public void updateCourse() throws Exception {
        mockMvc.perform(
            MockMvcRequestBuilders.put(UPDATE_COURSE, EXISTING_ID)
                .contentType(MediaType.APPLICATION_JSON)
                .content(toJson(updateCourseRequestDto()))
        )
            .andExpect(status().isOk())
            .andExpect(jsonContent("controller/course/response/update_id101.json"));
    }

    @Test
    @DisplayName("Изменение несуществующего курса")
    @ExpectedDatabase(
        value = "/controller/course/before/setup.xml",
        assertionMode = DatabaseAssertionMode.NON_STRICT_UNORDERED
    )
    public void updateNotExistingCourse() throws Exception {
        mockMvc.perform(
            MockMvcRequestBuilders.put(UPDATE_COURSE, ABSENT_ID)
                .contentType(MediaType.APPLICATION_JSON)
                .content(toJson(updateCourseRequestDto()))
        )
            .andExpect(status().isNotFound())
            .andExpect(jsonContent("controller/course/response/not_existing_course.json"));
    }

    @Test
    @DisplayName("Сохранение курса")
    @ExpectedDatabase(
        value = "/controller/course/after/create_course.xml",
        assertionMode = DatabaseAssertionMode.NON_STRICT_UNORDERED
    )
    public void createCourse() throws Exception {
        mockMvc.perform(
            MockMvcRequestBuilders.post(COURSES_ROOT_PATH)
                .contentType(MediaType.APPLICATION_JSON)
                .content(toJson(createCourseRequestDto()))
        )
            .andExpect(status().isOk())
            .andExpect(jsonContent("controller/course/response/create_course.json"));
    }

    @Test
    @DisplayName("Архивация курсов")
    @ExpectedDatabase(
        value = "/controller/course/after/archive_courses.xml",
        assertionMode = DatabaseAssertionMode.NON_STRICT_UNORDERED
    )
    public void archiveCourses() throws Exception {
        Set<Integer> idCourses = Set.of(EXISTING_ID);
        mockMvc.perform(
            MockMvcRequestBuilders.post(ARCHIVE_COURSE)
                .contentType(MediaType.APPLICATION_JSON)
                .content(toJson(idCourses))
        )
            .andExpect(status().isOk());
    }

    @Test
    @DisplayName("Разархивация курсов")
    @ExpectedDatabase(
        value = "/controller/course/after/unarchive_courses.xml",
        assertionMode = DatabaseAssertionMode.NON_STRICT_UNORDERED
    )
    public void unarchiveCourses() throws Exception {
        Set<Integer> idCourses = Set.of(103);
        mockMvc.perform(
            MockMvcRequestBuilders.post(UNARCHIVE_COURSE)
                .contentType(MediaType.APPLICATION_JSON)
                .content(toJson(idCourses))
        )
            .andExpect(status().isOk());
    }

    @Test
    @DisplayName("Архивация с несуществующим курсом")
    @ExpectedDatabase(
        value = "/controller/course/after/archive_not_exist_courses.xml",
        assertionMode = DatabaseAssertionMode.NON_STRICT_UNORDERED
    )
    public void archiveNotExistCourses() throws Exception {
        Set<Integer> idCourses = Set.of(EXISTING_ID, ABSENT_ID);
        mockMvc.perform(
            MockMvcRequestBuilders.post(ARCHIVE_COURSE)
                .contentType(MediaType.APPLICATION_JSON)
                .content(toJson(idCourses))
        )
            .andExpect(status().isOk());
    }

    @ParameterizedTest
    @MethodSource("validateArguments")
    public void validate(
        String fieldName,
        String errorMessage,
        UnaryOperator<CourseRequestDto> courseModifier
    ) throws Exception {
        mockMvc.perform(
            MockMvcRequestBuilders.post(COURSES_ROOT_PATH)
                .contentType(MediaType.APPLICATION_JSON)
                .content(toJson(courseModifier.apply(createCourseRequestDto())))
        )
            .andExpect(status().isBadRequest())
            .andExpect(jsonPath("errorMessage").value("Validation error"))
            .andExpect(jsonPath("fieldErrors." + fieldName).value(errorMessage));
    }

    @Nonnull
    private CourseRequestDto createCourseRequestDto() {
        return new CourseRequestDto()
            .setName("Created Name")
            .setDescription("Created description")
            .setStatus(CourseStatus.ACTIVE)
            .setLevel(CourseLevel.MAGISTRACY);
    }

    @Nonnull
    private CourseRequestDto updateCourseRequestDto() {
        return new CourseRequestDto()
            .setName("Updated Name")
            .setDescription("Updated description")
            .setStatus(CourseStatus.ARCHIVED)
            .setLevel(CourseLevel.MAGISTRACY);
    }

    @Nonnull
    private CourseFilter courseFilter() {
        return new CourseFilter()
            .setName("")
            .setLevels(EnumSet.allOf(CourseLevel.class))
            .setStatuses(EnumSet.allOf(CourseStatus.class));
    }

    @Nonnull
    private String toJson(Object object) throws IOException {
        return objectMapper.writeValueAsString(object);
    }
}
