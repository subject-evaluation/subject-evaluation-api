package ru.nsu.fit.controller;

import java.io.IOException;

import javax.annotation.Nonnull;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.github.springtestdbunit.annotation.DatabaseOperation;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.github.springtestdbunit.annotation.DatabaseTearDown;
import com.github.springtestdbunit.annotation.ExpectedDatabase;
import com.github.springtestdbunit.assertion.DatabaseAssertionMode;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import ru.nsu.fit.AbstractContextualTest;
import ru.nsu.fit.dto.feedback.FeedbackStatusUpdateDto;
import ru.nsu.fit.entity.enums.FeedbackStatus;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static ru.nsu.fit.util.paths.FeedbackPaths.FEEDBACK_ROOT_PATH;
import static ru.nsu.fit.util.paths.FeedbackPaths.FEEDBACK_UPDATE_STATUS;
import static ru.nsu.fit.utils.IntegrationTestUtils.jsonContent;

@DisplayName("Тесты отзывов")
@DatabaseSetup(value = "/controller/course/before/setup.xml")
class FeedbackControllerTest extends AbstractContextualTest {

    private final static int EXISTING_ID = 1;
    private final static int NON_EXISTING_ID = 101;

    @Autowired
    protected ObjectMapper objectMapper;

    @Test
    @DisplayName("Успешное изменение статуса отзыва по идентификатору")
    @DatabaseSetup("/controller/feedback/before/setup_update_status.xml")
    @ExpectedDatabase(
        value = "/controller/feedback/after/update_status.xml",
        table = "feedbacks",
        assertionMode = DatabaseAssertionMode.NON_STRICT_UNORDERED
    )
    void updateStatus() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.post(FEEDBACK_ROOT_PATH + FEEDBACK_UPDATE_STATUS, EXISTING_ID)
            .contentType(MediaType.APPLICATION_JSON)
            .content(toJson(new FeedbackStatusUpdateDto().setStatus(FeedbackStatus.PUBLISHED))))
            .andExpect(status().isOk());
    }

    @Test
    @DisplayName("Изменение статуса отзыва по несуществующему идентификатору")
    void updateStatusWithNonExistingId() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.post(FEEDBACK_ROOT_PATH + FEEDBACK_UPDATE_STATUS, NON_EXISTING_ID)
            .contentType(MediaType.APPLICATION_JSON)
            .content(toJson(new FeedbackStatusUpdateDto().setStatus(FeedbackStatus.PUBLISHED))))
            .andExpect(status().isNotFound())
            .andExpect(jsonContent("controller/feedback/response/non_existing_feedback.json"));
    }

    @Nonnull
    private String toJson(Object object) throws IOException {
        return objectMapper.writeValueAsString(object);
    }
}
