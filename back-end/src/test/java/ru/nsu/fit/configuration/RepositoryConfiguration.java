package ru.nsu.fit.configuration;

import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@Configuration
@EnableAutoConfiguration
@EnableJpaRepositories("ru.nsu.fit.dao")
@EntityScan("ru.nsu.fit.entity")
public class RepositoryConfiguration {
}
