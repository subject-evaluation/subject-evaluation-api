package ru.nsu.fit.specification;

import java.util.Optional;
import java.util.stream.Stream;

import javax.annotation.Nonnull;
import javax.annotation.ParametersAreNonnullByDefault;
import javax.persistence.criteria.Predicate;

import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Component;
import ru.nsu.fit.dto.feedback.FeedbackFilter;
import ru.nsu.fit.entity.Feedback;

@Component
@ParametersAreNonnullByDefault
public class FeedbackSpecificationFactoryImpl implements FeedbackSpecificationFactory {
    @Nonnull
    @Override
    public Specification<Feedback> fromFilter(FeedbackFilter filter) {
        return (root, query, criteriaBuilder) -> criteriaBuilder.and(
            Stream.of(
                Optional.ofNullable(filter.getCourseIds())
                    .map(courseIds -> root.get("survey").get("course").get("id").in(courseIds)),
                Optional.ofNullable(filter.getStatuses())
                    .map(statuses -> root.get("status").in(statuses))
            )
                .flatMap(Optional::stream)
                .toArray(Predicate[]::new)
        );
    }
}
