package ru.nsu.fit.controller;

import java.util.Set;

import javax.annotation.ParametersAreNonnullByDefault;
import javax.validation.Valid;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.nsu.fit.dto.filter.UserFilter;
import ru.nsu.fit.dto.user.UserRequestDto;
import ru.nsu.fit.dto.user.UserResponseDto;
import ru.nsu.fit.entity.enums.UserStatus;
import ru.nsu.fit.facade.user.UserFacade;

import static ru.nsu.fit.util.paths.UserPaths.USERS_ROOT_PATH;

@Api("API для работы с пользователями")
@CrossOrigin
@RestController
@RequiredArgsConstructor
@RequestMapping(USERS_ROOT_PATH)
@ParametersAreNonnullByDefault
public class UserController {
    private final UserFacade userFacade;

    @GetMapping("{id}")
    @ApiOperation(value = "Получение пользователя по его идентификатору")
    public UserResponseDto getUser(@ApiParam(required = true, value = "Идентификатор пользователя") @PathVariable int id) {
        return userFacade.getUser(id);
    }

    @PutMapping("search")
    @ApiOperation(value = "Получение списка пользователей")
    public Page<UserResponseDto> searchUsers(
        @ApiParam(required = true, value = "Фильтр для поиска пользователей") @RequestBody UserFilter filter,
        @PageableDefault(sort = {"status", "lastName"}, direction = Sort.Direction.ASC)
        @ApiParam(value = "Параметры для пагинации") Pageable pageable
    ) {
        return userFacade.searchUsers(filter, pageable);
    }

    @PostMapping("block")
    @ApiOperation(value = "Блокировка пользователей")
    public void blockUsers(@RequestBody Set<Integer> idUsers) {
        userFacade.updateUsersStatus(idUsers, UserStatus.BLOCKED);
    }

    @PostMapping("activate")
    @ApiOperation(value = "Активирование пользователей")
    public void activateUsers(@RequestBody Set<Integer> idUsers) {
        userFacade.updateUsersStatus(idUsers, UserStatus.ACTIVE);
    }

    @PostMapping
    public UserResponseDto createUser(@RequestBody @Valid UserRequestDto userRequestDto) {
        return userFacade.createUser(userRequestDto);
    }

    @PutMapping("{userId}")
    public UserResponseDto updateUser(
        @PathVariable Integer userId,
        @RequestBody @Valid UserRequestDto userRequestDto
    ) {
        return userFacade.updateUser(userId, userRequestDto);
    }
}
