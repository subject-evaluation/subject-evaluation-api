package ru.nsu.fit.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.nsu.fit.dto.survey.QuestionResponseDto;
import ru.nsu.fit.dto.survey.QuestionUpdateDto;
import ru.nsu.fit.dto.filter.SurveyFilter;
import ru.nsu.fit.dto.survey.SurveyRequestDto;
import ru.nsu.fit.dto.survey.SurveyResponseDto;
import ru.nsu.fit.dto.survey.SurveyUpdateDto;
import ru.nsu.fit.facade.survey.SurveyFacade;
import springfox.documentation.annotations.ApiIgnore;

import javax.annotation.ParametersAreNonnullByDefault;
import javax.validation.Valid;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import java.security.Principal;
import java.util.List;

import static ru.nsu.fit.util.paths.SurveyPaths.SEARCH_PATH;
import static ru.nsu.fit.util.paths.SurveyPaths.SURVEY_ROOT_PATH;

@Slf4j
@CrossOrigin
@RestController
@RequiredArgsConstructor
@ParametersAreNonnullByDefault
@RequestMapping(SURVEY_ROOT_PATH)
@Api("API для работы с опросами")
public class SurveyController {

    private final SurveyFacade surveyFacade;

    @GetMapping("{id}")
    @ApiOperation(value = "Получение опроса по его идентификатору")
    public SurveyResponseDto getSurvey(
        @ApiParam(required = true, value = "Идентификатор опроса") @PathVariable int id) {
        return surveyFacade.getSurvey(id);
    }

    @PostMapping
    @ApiOperation(value = "Создание опроса")
    public SurveyResponseDto createSurvey(@RequestBody @Valid @NotNull SurveyRequestDto surveyRequestDto) {
        log.debug("Create survey method queried with dto {}", surveyRequestDto);
        return surveyFacade.createSurvey(surveyRequestDto);
    }

    @PostMapping(SEARCH_PATH)
    @ApiOperation(value = "Получение списка опросов")
    public Page<SurveyResponseDto> searchSurveys(
        @ApiParam(required = true, value = "Фильтр для поиска опросов") @RequestBody SurveyFilter filter,
        @PageableDefault(sort = {"startDate"}, direction = Sort.Direction.ASC)
        @ApiParam(value = "Параметры для постраничной навигации") Pageable pageable
    ) {
        return surveyFacade.searchSurvey(filter, pageable);
    }

    @PutMapping("{id}")
    @ApiOperation(value = "Изменение опроса")
    public SurveyResponseDto updateSurvey(
        @ApiParam(required = true, value = "Идентификатор опроса") @PathVariable int id,
        @RequestBody @Valid @NotNull SurveyUpdateDto surveyRequestDto,
        @ApiIgnore Principal principal
    ) {
        log.debug("Update survey with id {} method queried with dto {}", id, surveyRequestDto);
        return surveyFacade.updateSurvey(principal, id, surveyRequestDto);
    }

    @PutMapping("questions/{id}")
    @ApiOperation(value = "Изменение вопросов опроса")
    public List<QuestionResponseDto> updateSurveyQuestions(
        @ApiParam(required = true, value = "Идентификатор опроса") @PathVariable int id,
        @ApiParam(required = true, value = "Список вопросов опроса")
        @RequestBody @Valid @NotEmpty List<QuestionUpdateDto> questions,
        @ApiIgnore Principal principal
    ) {
        log.debug("Update survey with id {} method queried with dto {}", id, questions);
        return surveyFacade.updateQuestions(principal, id, questions);
    }
}
