package ru.nsu.fit.controller;

import java.io.IOException;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import ru.nsu.fit.dto.file.FileCreateDto;
import ru.nsu.fit.dto.file.FileResponseDto;
import ru.nsu.fit.entity.enums.FileType;
import ru.nsu.fit.facade.file.FileFacade;

import static ru.nsu.fit.util.paths.FilePaths.FILES_ROOT_PATH;

@Slf4j
@RestController
@RequiredArgsConstructor
@Api("API для работы с файлами")
@RequestMapping(FILES_ROOT_PATH)
public class FileController {
    private final FileFacade fileFacade;

    @PostMapping("save")
    @ApiOperation(value = "Создание файла")
    public ResponseEntity<Integer> createFile(
        @ApiParam(required = true, value = "Файл курса")
        @Valid @NotNull @RequestBody FileCreateDto fileCreateDto
    ) {
        return ResponseEntity.ok(fileFacade.saveFile(fileCreateDto));
    }

    @PostMapping("upload")
    @ApiOperation(value = "Загрузка файла")
    public ResponseEntity<FileResponseDto> uploadFile(
        @ApiParam(required = true, value = "Файл для сохранения")
        @RequestBody @Valid @NotBlank MultipartFile file,
        @RequestParam("fileType") FileType fileType
    ) throws IOException {
        if (file.isEmpty()) {
            return ResponseEntity.badRequest().build();
        }
        return ResponseEntity.ok(fileFacade.uploadFile(file, fileType));
    }

    @GetMapping("{id}")
    @ApiOperation(value = "Получение файла курса")
    public FileResponseDto getFile(
        @ApiParam(required = true, value = "Идентификатор файла курса") @PathVariable int id
    ) {
        return fileFacade.getFile(id);
    }

    @GetMapping("{id}/download")
    @ApiOperation(value = "Скачивание файла")
    public ResponseEntity<?> downloadFile(
        @ApiParam(required = true, value = "Идентификатор файла") @PathVariable int id
    ) {
        FileResponseDto file = fileFacade.getFile(id);
        return ResponseEntity.ok()
            .contentType(MediaType.parseMediaType(file.getMimeType()))
            .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; fileName=\"" + file.getName() + "\"")
            .body(file.getContent());
    }
}
