package ru.nsu.fit.controller;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.nsu.fit.dto.auth.AuthCodeDto;
import ru.nsu.fit.dto.auth.RefreshTokenDto;
import ru.nsu.fit.dto.auth.TokensDto;
import ru.nsu.fit.service.user.UserService;
import springfox.documentation.annotations.ApiIgnore;

import javax.annotation.ParametersAreNonnullByDefault;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import static ru.nsu.fit.util.paths.AuthPaths.*;
import static ru.nsu.fit.util.paths.RootPaths.API_VERSION_PREFIX;

@Slf4j
@RestController
@RequiredArgsConstructor
@ParametersAreNonnullByDefault
@RequestMapping(API_VERSION_PREFIX)
public class AuthorizationController {

    private final UserService userService;

    @PostMapping(AUTH_PATH)
    @ApiOperation("Авторизация в приложении с помощью кода авторизации Google")
    public TokensDto authorise(@RequestBody @Valid @ApiParam("Код авторизации Google") AuthCodeDto authCodeDto) {
        log.debug("Authorize method queried");
        return userService.authorize(authCodeDto.getAuthCode());
    }

    @PostMapping(REFRESH_PATH)
    @ApiOperation("Обновление access и refresh токена")
    public TokensDto refreshTokens(@RequestBody @Valid @ApiParam("Refresh и access токен") RefreshTokenDto tokenDto) {
        log.debug("Refresh token method queried");
        return userService.refreshTokens(tokenDto.getRefreshToken());
    }

    @PostMapping(LOGOUT_PATH)
    @ApiOperation("Выход из профиля")
    public void logout(@ApiIgnore HttpServletRequest request) {
        log.debug("Logout method queried");
        userService.logout(request);
    }
}
