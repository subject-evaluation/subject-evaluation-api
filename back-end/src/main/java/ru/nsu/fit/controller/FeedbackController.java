package ru.nsu.fit.controller;

import javax.annotation.ParametersAreNonnullByDefault;
import javax.validation.Valid;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import ru.nsu.fit.dto.feedback.FeedbackCreateDto;
import ru.nsu.fit.dto.feedback.FeedbackDto;
import ru.nsu.fit.dto.feedback.FeedbackFilter;
import ru.nsu.fit.dto.feedback.FeedbackStatusUpdateDto;
import ru.nsu.fit.dto.feedback.FeedbackUpdateDto;
import ru.nsu.fit.facade.feedback.FeedbackFacade;
import ru.nsu.fit.util.paths.FeedbackPaths;

@Api("API для работы с отзывами")
@CrossOrigin
@RestController
@RequiredArgsConstructor
@RequestMapping(FeedbackPaths.FEEDBACK_ROOT_PATH)
@ParametersAreNonnullByDefault
public class FeedbackController {
    private final FeedbackFacade feedbackFacade;

    @PostMapping(FeedbackPaths.FEEDBACK_UPDATE_STATUS)
    @ApiOperation(value = "Обновление статуса отзыва")
    public void updateStatus(
        @ApiParam(required = true, value = "Идентификатор отзыва") @PathVariable int feedbackId,
        @ApiParam(required = true, value = "Новый статус") @RequestBody FeedbackStatusUpdateDto body
    ) {
        feedbackFacade.updateStatus(feedbackId, body.getStatus());
    }

    @PutMapping("search")
    public Page<FeedbackDto> searchFeedbacks(
        @Valid @RequestBody FeedbackFilter feedbackFilter,
        @PageableDefault(sort = {"creationDate"}, direction = Sort.Direction.DESC)
        @ApiParam(value = "Параметры для пагинации") Pageable pageable
    ) {
        return feedbackFacade.searchFeedbacks(feedbackFilter, pageable);
    }

    @GetMapping("/get-feedback")
    Page<FeedbackDto> getFeedback(
        @ApiParam(required = true, value = "Идентификатор курса и статус") @RequestParam FeedbackFilter feedbackFilter,
        @PageableDefault(
            sort = "creationDate",
            direction = Sort.Direction.DESC
        ) @ApiParam(value = "Параметры для пагинации") Pageable pageable
    ) {
        return feedbackFacade.getFeedback(feedbackFilter, pageable);
    }

    @PostMapping("/create-feedback")
    long createFeedback(
        @ApiParam(required = true, value = "Отзыв") @RequestParam FeedbackCreateDto feedbackCreateDto
    ) {
        return feedbackFacade.createFeedback(feedbackCreateDto);
    }

    @PostMapping("update-feedback")
    FeedbackDto updateFeedback(
        @ApiParam(required = true, value = "Отзыв") @RequestParam FeedbackUpdateDto feedbackUpdateDto
    ) {
        return feedbackFacade.updateFeedback(feedbackUpdateDto);
    }

    @GetMapping("/get-user-feedback")
    FeedbackDto getUserFeedback(
        @ApiParam(required = true, value = "Идентификатор курса и статус") @RequestParam Integer userId,
        @RequestParam Integer surveyId
    ) {
        return feedbackFacade.getUserFeedback(userId, surveyId);
    }
}
