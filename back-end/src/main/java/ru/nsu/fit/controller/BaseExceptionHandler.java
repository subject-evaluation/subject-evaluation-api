package ru.nsu.fit.controller;

import java.time.format.DateTimeParseException;
import java.util.List;
import java.util.Map;
import java.util.SortedMap;
import java.util.stream.Collectors;

import javax.annotation.ParametersAreNonnullByDefault;
import javax.ws.rs.ForbiddenException;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.google.api.client.auth.oauth2.TokenResponseException;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import one.util.streamex.StreamEx;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.validation.FieldError;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import ru.nsu.fit.exceptions.ConflictException;
import ru.nsu.fit.exceptions.DataNotFoundException;
import ru.nsu.fit.exceptions.SEAccessDeniedException;

@ControllerAdvice
@ParametersAreNonnullByDefault
public class BaseExceptionHandler {

    @Data
    @Accessors(chain = true)
    public static class Error {
        @ApiModelProperty(value = "Сообщение об ошибке", example = "User 111 not found")
        private String errorMessage;
    }

    @Data
    @Accessors(chain = true)
    @JsonInclude(JsonInclude.Include.NON_NULL)
    @EqualsAndHashCode(callSuper = true)
    public static class ValidationError extends Error {
        private Map<String, String> fieldErrors;
        private List<String> constraintViolations;
    }

    @ExceptionHandler(ConflictException.class)
    public ResponseEntity<Error> handleConflictException(ConflictException ex) {
        return new ResponseEntity<>(
            new Error().setErrorMessage(ex.getLocalizedMessage()),
            HttpStatus.CONFLICT
        );
    }

    @ExceptionHandler(DataNotFoundException.class)
    public ResponseEntity<Error> handleDataNotFoundException(DataNotFoundException ex) {
        return new ResponseEntity<>(
            new Error().setErrorMessage(ex.getLocalizedMessage()),
            HttpStatus.NOT_FOUND
        );
    }

    @ExceptionHandler(IllegalArgumentException.class)
    public ResponseEntity<Error> handleIllegalArgument(IllegalArgumentException exception) {
        return new ResponseEntity<>(
            new Error().setErrorMessage(exception.getLocalizedMessage()),
            HttpStatus.BAD_REQUEST
        );
    }

    @ExceptionHandler(UsernameNotFoundException.class)
    public ResponseEntity<Error> handleUsernameNotFound(UsernameNotFoundException exception) {
        return new ResponseEntity<>(
            new Error().setErrorMessage(exception.getLocalizedMessage()),
            HttpStatus.UNAUTHORIZED
        );
    }

    @ExceptionHandler(TokenResponseException.class)
    public ResponseEntity<Error> handleTokenResponseException(TokenResponseException exception) {
        return new ResponseEntity<>(
            new Error().setErrorMessage(exception.getLocalizedMessage()),
            HttpStatus.UNAUTHORIZED
        );
    }

    @ExceptionHandler(HttpMessageNotReadableException.class)
    public ResponseEntity<Error> handleDateTimeParseException(HttpMessageNotReadableException exception) {
        String message = "Validation error";
        Throwable cause = exception.getCause();
        while (cause != null && !cause.equals(cause.getCause())) {
            if (cause instanceof DateTimeParseException) {
                message = "Неправильный формат даты";
                break;
            }
            cause = cause.getCause();
        }
        return new ResponseEntity<>(
            new Error().setErrorMessage(message),
            HttpStatus.BAD_REQUEST
        );
    }

    @ExceptionHandler(MethodArgumentNotValidException.class)
    public ResponseEntity<Error> handle(MethodArgumentNotValidException exception) {
        SortedMap<String, String> fieldErrors = StreamEx.of(exception.getBindingResult().getAllErrors())
            .filter(error -> error instanceof FieldError)
            .map(FieldError.class::cast)
            .mapToEntry(FieldError::getField, FieldError::getDefaultMessage)
            .toSortedMap();
        List<String> constraintViolationErrors = StreamEx.of(exception.getBindingResult().getAllErrors())
            .filter(objectError -> !(objectError instanceof FieldError))
            .map(ObjectError::getDefaultMessage)
            .collect(Collectors.toList());
        ValidationError validationError = new ValidationError();
        if (!fieldErrors.isEmpty()) {
            validationError.setFieldErrors(fieldErrors);
        }
        if (!constraintViolationErrors.isEmpty()) {
            validationError.setConstraintViolations(constraintViolationErrors);
        }
        validationError.setErrorMessage("Validation error");
        return new ResponseEntity<>(
            validationError,
            HttpStatus.BAD_REQUEST
        );
    }

    @ExceptionHandler(ForbiddenException.class)
    public ResponseEntity<Error> handle(ForbiddenException exception) {
        return new ResponseEntity<>(
            new Error().setErrorMessage(exception.getLocalizedMessage()),
            HttpStatus.FORBIDDEN
        );
    }

    @ExceptionHandler(SEAccessDeniedException.class)
    public ResponseEntity<Error> handleAccessDeniedException(SEAccessDeniedException ex) {
        return new ResponseEntity<>(
            new Error().setErrorMessage(ex.getLocalizedMessage()),
            HttpStatus.FORBIDDEN
        );
    }

}
