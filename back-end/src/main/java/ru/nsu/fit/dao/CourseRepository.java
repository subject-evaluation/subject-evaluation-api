package ru.nsu.fit.dao;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.nsu.fit.entity.Course;
import ru.nsu.fit.entity.enums.CourseLevel;
import ru.nsu.fit.entity.enums.CourseStatus;

import javax.annotation.ParametersAreNonnullByDefault;
import java.util.List;
import java.util.Set;

@Repository
@ParametersAreNonnullByDefault
public interface CourseRepository extends JpaRepository<Course, Integer> {
    Page<Course> findByStatusInAndLevelInAndNameContainingIgnoreCase(
            Set<CourseStatus> status,
            Set<CourseLevel> level,
            String name,
            Pageable pageable
    );

    List<Course> getByIdIn(Set<Integer> id);
}