package ru.nsu.fit.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.nsu.fit.entity.question.AnswerChoice;

import javax.annotation.ParametersAreNonnullByDefault;
import java.util.Optional;

@Repository
@ParametersAreNonnullByDefault
public interface AnswerChoiceRepository extends JpaRepository<AnswerChoice, Integer> {

    Optional<AnswerChoice> findById(Integer id);

}
