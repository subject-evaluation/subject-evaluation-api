package ru.nsu.fit.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.nsu.fit.entity.question.Question;

import javax.annotation.ParametersAreNonnullByDefault;
import java.util.Optional;

@Repository
@ParametersAreNonnullByDefault
public interface QuestionRepository extends JpaRepository<Question, Integer> {

    Optional<Question> findById(Integer id);

}
