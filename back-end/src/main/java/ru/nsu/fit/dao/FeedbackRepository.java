package ru.nsu.fit.dao;

import java.util.Optional;

import javax.annotation.ParametersAreNonnullByDefault;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;
import ru.nsu.fit.entity.Feedback;

@Repository
@ParametersAreNonnullByDefault
public interface FeedbackRepository extends JpaRepository<Feedback, Integer>, JpaSpecificationExecutor<Feedback> {

    Page<Feedback> findAll(Specification<Feedback> specification, Pageable pageable);

    Optional<Feedback> findByAuthor_IdAndSurvey_Id(Integer authorId, Integer surveyId);

    Optional<Feedback> findById(Integer id);

}
