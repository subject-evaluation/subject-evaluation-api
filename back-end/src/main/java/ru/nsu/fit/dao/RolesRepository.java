package ru.nsu.fit.dao;

import javax.annotation.ParametersAreNonnullByDefault;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.nsu.fit.entity.UserRole;
import ru.nsu.fit.entity.enums.Role;

@Repository
@ParametersAreNonnullByDefault
public interface RolesRepository extends JpaRepository<UserRole, Integer> {
    UserRole findByName(Role name);
}
