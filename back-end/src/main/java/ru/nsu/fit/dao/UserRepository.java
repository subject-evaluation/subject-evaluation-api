package ru.nsu.fit.dao;

import java.util.List;
import java.util.Optional;
import java.util.Set;

import javax.annotation.ParametersAreNonnullByDefault;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import ru.nsu.fit.entity.User;
import ru.nsu.fit.entity.enums.UserStatus;

@Repository
@ParametersAreNonnullByDefault
public interface UserRepository extends JpaRepository<User, Integer> {
    Optional<User> findByEmailIgnoreCase(String email);

    Optional<User> findByEmail(String email);

    Optional<User> findByGoogleId(String googleId);

    Optional<User> findByAccessToken(String accessToken);

    Optional<User> findByRefreshToken(String refreshToken);

    Optional<User> findById(Integer id);

    List<User> getByIdIn(Set<Integer> id);

    /*
    Page<User> findByStatusInAndRolesInAndFirstNameContainingIgnoreCase(
            Set<UserStatus> status,
            Set<Role> role,
            String firstName,
            String lastName,
            String patronymic,
            String email,
            Pageable pageable
    );*/

    @Query("select u from User u where u.status in (:status) and (u.email like :line or u.firstName like :line or u.lastName like :line or u.patronymic like :line)")
    Page<User> findByStatusInAndLineIn(
            Set<UserStatus> status,
            String line,
            Pageable pageable
    );

}
