package ru.nsu.fit.facade.feedback;

import javax.annotation.ParametersAreNonnullByDefault;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import ru.nsu.fit.dto.feedback.FeedbackCreateDto;
import ru.nsu.fit.dto.feedback.FeedbackDto;
import ru.nsu.fit.dto.feedback.FeedbackFilter;
import ru.nsu.fit.dto.feedback.FeedbackUpdateDto;
import ru.nsu.fit.entity.enums.FeedbackStatus;

/**
 * Сервис для работы с отзывами
 */
@ParametersAreNonnullByDefault
public interface FeedbackFacade {

    /**
     * Обновление статуса отзыва
     *
     * @param feedbackId идентификатор отзыва
     * @param status     новый статус
     */
    void updateStatus(int feedbackId, FeedbackStatus status);

    Page<FeedbackDto> getFeedback(FeedbackFilter feedbackFilter, Pageable pageable);

    long createFeedback(FeedbackCreateDto feedbackCreateDto);

    FeedbackDto updateFeedback(FeedbackUpdateDto feedbackUpdateDto);

    /**
     * Поиск отзывов.
     *
     * @param filter   параметры фильтрации
     * @param pageable параметры для пагинации
     * @return страница отзывов
     */
    Page<FeedbackDto> searchFeedbacks(FeedbackFilter filter, Pageable pageable);

    FeedbackDto getUserFeedback(Integer userId, Integer surveyId);

}
