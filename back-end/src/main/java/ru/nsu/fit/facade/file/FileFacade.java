package ru.nsu.fit.facade.file;

import org.springframework.web.multipart.MultipartFile;
import ru.nsu.fit.dto.file.FileCreateDto;
import ru.nsu.fit.dto.file.FileResponseDto;
import ru.nsu.fit.entity.enums.FileType;

import java.io.IOException;

/**
 * Сервис для работы с файлами
 */
public interface FileFacade {
    /**
     * Получение файла курса по его идентификатору
     *
     * @param id идентификатор файла курса
     * @return файл курса
     */
    FileResponseDto getFile(int id);
    /**
     * Сохранение файла курса
     *
     * @param fileCreateDto файл курса
     */
    int saveFile(FileCreateDto fileCreateDto);
    /**
     * Сохранение файла курса из файла
     *
     * @param file файл
     * @param fileType тип файла курса
     * @return файл курса
     */
    FileResponseDto uploadFile(MultipartFile file, FileType fileType) throws IOException;
}
