package ru.nsu.fit.facade.feedback;

import javax.annotation.ParametersAreNonnullByDefault;

import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.nsu.fit.dto.feedback.FeedbackCreateDto;
import ru.nsu.fit.dto.feedback.FeedbackDto;
import ru.nsu.fit.dto.feedback.FeedbackFilter;
import ru.nsu.fit.dto.feedback.FeedbackUpdateDto;
import ru.nsu.fit.entity.enums.FeedbackStatus;
import ru.nsu.fit.mapper.feedback.FeedbackMapper;
import ru.nsu.fit.service.feedback.FeedbackService;

@Service
@Transactional
@RequiredArgsConstructor
@ParametersAreNonnullByDefault
public class FeedbackFacadeImpl implements FeedbackFacade {

    private final FeedbackService feedbackService;
    private final FeedbackMapper feedbackMapper;

    @Override
    public void updateStatus(int feedbackId, FeedbackStatus status) {
        feedbackService.updateStatus(feedbackId, status);
    }

    @Override
    public Page<FeedbackDto> getFeedback(FeedbackFilter feedbackFilter, Pageable pageable) {
        return feedbackService.getFeedback(feedbackFilter, pageable);
    }

    @Override
    public long createFeedback(FeedbackCreateDto feedbackCreateDto) {
        return feedbackService.createFeedback(feedbackCreateDto);
    }

    @Override
    public FeedbackDto updateFeedback(FeedbackUpdateDto feedbackUpdateDto) {
        return feedbackService.updateFeedback(feedbackUpdateDto);
    }

    @Override
    public Page<FeedbackDto> searchFeedbacks(FeedbackFilter filter, Pageable pageable) {
        return feedbackService.searchFeedbacks(filter, pageable).map(feedbackMapper::map);
    }

    @Override
    public FeedbackDto getUserFeedback(Integer userId, Integer surveyId) {
        return feedbackService.getUserFeedback(userId, surveyId);
    }

}
