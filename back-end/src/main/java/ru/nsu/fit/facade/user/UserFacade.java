package ru.nsu.fit.facade.user;

import java.util.Set;

import javax.annotation.Nonnull;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import ru.nsu.fit.dto.filter.UserFilter;
import ru.nsu.fit.dto.user.UserRequestDto;
import ru.nsu.fit.dto.user.UserResponseDto;
import ru.nsu.fit.entity.enums.UserStatus;

public interface UserFacade {
    /**
     * Получение пользователя по его идентификатору
     *
     * @param id идентификатор пользователя
     * @return пользователь
     */
    @Nonnull
    UserResponseDto getUser(int id);

    /**
     * Поиск пользователей по заданным параметрам
     *
     * @param filter   фильтр, содержащий параметры для поиска
     * @param pageable настройки для пагинации
     * @return страница пользователей
     */
    @Nonnull
    Page<UserResponseDto> searchUsers(UserFilter filter, Pageable pageable);

    /**
     * Изменение статуса пользователя
     *
     * @param idUsers    - идентификаторы пользователей
     * @param userStatus - статус пользователя, на который нужно обновить текущий статус
     */
    void updateUsersStatus(Set<Integer> idUsers, UserStatus userStatus);

    /**
     * Создать пользователя.
     *
     * @param userRequestDto информация о новом пользователе
     * @return информация о созданном пользователе
     */
    UserResponseDto createUser(UserRequestDto userRequestDto);

    /**
     * Обновить пользователя
     *
     * @param userId         идентификатор пользователя
     * @param userRequestDto информация для обновления пользователя
     * @return информация об обновленном пользователе
     */
    UserResponseDto updateUser(Integer userId, UserRequestDto userRequestDto);
}
