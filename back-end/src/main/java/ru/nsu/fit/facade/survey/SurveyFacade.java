package ru.nsu.fit.facade.survey;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import ru.nsu.fit.dto.filter.SurveyFilter;
import ru.nsu.fit.dto.survey.QuestionResponseDto;
import ru.nsu.fit.dto.survey.QuestionUpdateDto;
import ru.nsu.fit.dto.survey.SurveyRequestDto;
import ru.nsu.fit.dto.survey.SurveyResponseDto;
import ru.nsu.fit.dto.survey.SurveyUpdateDto;

import javax.annotation.Nonnull;
import javax.annotation.ParametersAreNonnullByDefault;
import java.security.Principal;
import java.util.List;

@ParametersAreNonnullByDefault
public interface SurveyFacade {
    /**
     * Создание опроса
     *
     * @param surveyRequestDto данные опроса
     * @return сохраенный опрос со всеми id
     */
    @Nonnull
    SurveyResponseDto createSurvey(SurveyRequestDto surveyRequestDto);

    /**
     * Получение опроса по его идентификатору
     *
     * @param id идентификатор опроса
     * @return информация об опросе
     */
    @Nonnull
    SurveyResponseDto getSurvey(int id);


    /**
     * Обновление информации о курсе
     *
     * @param id - идентификатор опроса
     * @param surveyUpdateDto обновляемые данные
     * @param principal сущность spring security, из которой можно полуить юзера, инициировавшего запрос
     * @return обновленные данные по опросу
     */
    @Nonnull
    SurveyResponseDto updateSurvey(Principal principal, int id, SurveyUpdateDto surveyUpdateDto);

    /**
     *
     * @param principal юзер, инициировааший запрос
     * @param id идентификатор опроса
     * @param questionUpdateDtos обновляемые вопросы опроса
     * @return обновленные вопросы
     */
    @Nonnull
    List<QuestionResponseDto> updateQuestions(Principal principal, int id, List<QuestionUpdateDto> questionUpdateDtos);

    /**
     * Поиск опросов по заданным параметрам
     *
     * @param filter   фильтр, содержащий параметры поиска
     * @param pageable настройки постраничной навигации
     * @return страница опросов
     */
    @Nonnull
    Page<SurveyResponseDto> searchSurvey(SurveyFilter filter, Pageable pageable);
}
