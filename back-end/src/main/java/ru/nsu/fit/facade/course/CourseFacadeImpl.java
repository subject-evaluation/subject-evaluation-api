package ru.nsu.fit.facade;

import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.nsu.fit.dto.course.CourseRequestDto;
import ru.nsu.fit.dto.course.CourseResponseDto;
import ru.nsu.fit.dto.filter.CourseFilter;
import ru.nsu.fit.entity.Course;
import ru.nsu.fit.entity.enums.CourseStatus;
import ru.nsu.fit.facade.course.CourseFacade;
import ru.nsu.fit.mapper.CourseMapper;
import ru.nsu.fit.service.course.CourseService;

import javax.annotation.Nonnull;
import javax.annotation.ParametersAreNonnullByDefault;
import java.util.List;
import java.util.Set;

@Service
@Transactional
@RequiredArgsConstructor
@ParametersAreNonnullByDefault
public class CourseFacadeImpl implements CourseFacade {
    private final CourseService courseService;
    private final CourseMapper mapper;

    @Nonnull
    @Override
    public CourseResponseDto getCourse(int id) {
        return mapper.courseToDto(courseService.getCourse(id));
    }

    @Override
    public CourseResponseDto createCourse(CourseRequestDto course) {
        Course creatingCourse = mapper.dtoToCourse(course);
        Course createdCourse = courseService.createCourse(creatingCourse);
        return mapper.courseToDto(createdCourse);
    }

    @Override
    public CourseResponseDto updateCourse(int id, CourseRequestDto course) {
        Course updatingCourse = mapper.dtoToCourse(course);
        Course updatedCourse = courseService.updateCourse(id, updatingCourse);
        return mapper.courseToDto(updatedCourse);
    }

    @Nonnull
    @Override
    public Page<CourseResponseDto> searchCourses(CourseFilter filter, Pageable pageable) {
        return mapper.coursesToDtos(courseService.searchCourses(filter, pageable));
    }

    @Override
    public void updateCoursesStatus(Set<Integer> idCourses, CourseStatus courseStatus) {
        courseService.updateCourseStatus(idCourses, courseStatus);
    }

    @Override
    public List<Integer> getAllCourseIds() {
        return courseService.getAllCourseIds();
    }

    @Override
    public CourseStatus defineStatusByIds(List<Integer> courseIds) {
        return courseService.defineStatusByIds(courseIds);
    }
}
