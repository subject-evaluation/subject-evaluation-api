package ru.nsu.fit.facade.survey;

import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Component;
import ru.nsu.fit.dto.survey.QuestionResponseDto;
import ru.nsu.fit.dto.survey.QuestionUpdateDto;
import ru.nsu.fit.dto.filter.SurveyFilter;
import ru.nsu.fit.dto.survey.SurveyRequestDto;
import ru.nsu.fit.dto.survey.SurveyResponseDto;
import ru.nsu.fit.dto.survey.SurveyUpdateDto;
import ru.nsu.fit.entity.Course;
import ru.nsu.fit.entity.Survey;
import ru.nsu.fit.entity.User;
import ru.nsu.fit.entity.question.Question;
import ru.nsu.fit.mapper.survey.QuestionMapper;
import ru.nsu.fit.mapper.survey.SurveyMapper;
import ru.nsu.fit.service.course.CourseService;
import ru.nsu.fit.service.survey.SurveyService;
import ru.nsu.fit.service.user.UserService;

import javax.annotation.Nonnull;
import javax.annotation.ParametersAreNonnullByDefault;
import javax.transaction.Transactional;
import java.security.Principal;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Component
@Transactional
@RequiredArgsConstructor
@ParametersAreNonnullByDefault
public class SurveyFacadeImpl implements SurveyFacade {

    private final UserService userService;
    private final CourseService courseService;
    private final SurveyService surveyService;
    private final SurveyMapper surveyMapper;
    private final QuestionMapper questionMapper;

    @Nonnull
    @Override
    public SurveyResponseDto getSurvey(int id) {
        Survey survey = surveyService.getSurvey(id);
        return surveyMapper.survey2Dto(survey);
    }

    @Nonnull
    @Override
    public SurveyResponseDto createSurvey(SurveyRequestDto surveyRequestDto) {
        Survey creatingSurvey = surveyMapper.dtoToSurvey(surveyRequestDto);
        mapSurveyIds(creatingSurvey, surveyRequestDto.getAuthorId(), surveyRequestDto.getCourseId(),
            surveyRequestDto.getStudentIds(), surveyRequestDto.getTeacherIds());
        Survey savedSurvey = surveyService.createSurvey(creatingSurvey);
        SurveyResponseDto responseDto = surveyMapper.survey2ResponseDto(savedSurvey);
        return responseDto
            .setAuthorId(surveyRequestDto.getAuthorId())
            .setCourseId(surveyRequestDto.getCourseId())
            .setStudentIds(surveyRequestDto.getStudentIds())
            .setTeacherIds(surveyRequestDto.getTeacherIds());
    }

    @Nonnull
    @Override
    public SurveyResponseDto updateSurvey(Principal principal, int id, SurveyUpdateDto surveyUpdateDto) {
        User user = userService.getUserWithCheck(principal);
        Survey updatingSurvey = surveyMapper.dtoToSurvey(surveyUpdateDto);
        mapSurveyIds(updatingSurvey, surveyUpdateDto.getAuthorId(), surveyUpdateDto.getCourseId(),
            surveyUpdateDto.getStudentIds(), surveyUpdateDto.getTeacherIds());
        Survey updatedSurvey = surveyService.updateSurvey(user, id, updatingSurvey);
        return surveyMapper.survey2Dto(updatedSurvey)
            .setAuthorId(surveyUpdateDto.getAuthorId())
            .setCourseId(surveyUpdateDto.getCourseId())
            .setStudentIds(surveyUpdateDto.getStudentIds())
            .setTeacherIds(surveyUpdateDto.getTeacherIds());
    }

    @Nonnull
    @Override
    public List<QuestionResponseDto> updateQuestions(Principal principal, int id, List<QuestionUpdateDto> questionUpdateDtos) {
        User user = userService.getUserWithCheck(principal);
        List<Question> updatingQuestions = questionMapper.dtosUpdateToQuestions(questionUpdateDtos);
        List<Question> updatedQuestions = surveyService.updateQuestions(user, id, updatingQuestions);
        return questionMapper.questionsToDtos(updatedQuestions);
    }

    private void mapSurveyIds(Survey survey, int authorId, int courseId, List<Integer> studentIds, List<Integer> teacherIds) {
        Course course = courseService.getCourse(courseId);
        User author = userService.getById(authorId);
        Set<User> students = studentIds.stream()
            .map(userService::getById)
            .collect(Collectors.toSet());
        Set<User> teachers = teacherIds.stream()
            .map(userService::getById)
            .collect(Collectors.toSet());
        survey
            .setCourse(course)
            .setAuthor(author)
            .setStudents(students)
            .setTeachers(teachers);
    }

    @Nonnull
    @Override
    public Page<SurveyResponseDto> searchSurvey(SurveyFilter filter, Pageable pageable) {
        Page<Survey> surveys = surveyService.searchSurvey(filter, pageable);
        return surveyMapper.surveys2Dtos(surveys);
    }
}
