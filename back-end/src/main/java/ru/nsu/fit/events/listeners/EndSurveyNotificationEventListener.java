package ru.nsu.fit.events.listeners;

import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Component;
import ru.nsu.fit.events.model.EndSurveyNotificationEvent;
import ru.nsu.fit.events.publishers.SurveyNotificationSuccessEventPublisher;
import ru.nsu.fit.service.email.EmailService;

import javax.annotation.ParametersAreNonnullByDefault;

import static ru.nsu.fit.events.model.NotificationType.END_SURVEY;

@Log4j2
@Component
@RequiredArgsConstructor
@ParametersAreNonnullByDefault
public class EndSurveyNotificationEventListener
    implements ApplicationListener<EndSurveyNotificationEvent> {

    private final EmailService emailService;
    private final SurveyNotificationSuccessEventPublisher successNotificationEventPublisher;

    private final static String EMAIL_SUBJECT = "Завершен опрос по курсу ";
    private final static String EMAIL_MESSAGE = "Опрос по курсу %s завершен";

    @Override
    public void onApplicationEvent(EndSurveyNotificationEvent event) {
        log.info("Listener got end survey notification event");
        String subject = EMAIL_SUBJECT.concat(event.getCourseName());
        String message = String.format(EMAIL_MESSAGE, event.getCourseName());
        emailService.sendMessage(event.getEmails(), subject, message);
        log.info("Email about survey's ending was sent");
        successNotificationEventPublisher.publishSurveyNotificationSuccess(event.getSurveyId(), END_SURVEY);
    }
}
