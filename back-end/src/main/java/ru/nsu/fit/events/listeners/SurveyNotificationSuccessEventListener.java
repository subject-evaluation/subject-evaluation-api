package ru.nsu.fit.events.listeners;

import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Component;
import ru.nsu.fit.events.model.SurveyNotificationSuccessEvent;
import ru.nsu.fit.service.survey.SurveyService;

import javax.annotation.ParametersAreNonnullByDefault;

import static ru.nsu.fit.events.model.NotificationType.END_SURVEY;

@Log4j2
@Component
@RequiredArgsConstructor
@ParametersAreNonnullByDefault
public class SurveyNotificationSuccessEventListener
    implements ApplicationListener<SurveyNotificationSuccessEvent> {
    private final SurveyService surveyService;

    @Override
    public void onApplicationEvent(SurveyNotificationSuccessEvent event) {
        log.info("Listener got survey notification success event");
        if (END_SURVEY.equals(event.getNotificationType())) {
            surveyService.setSurveyEndNotificated(event.getSurveyId());
        } else {
            surveyService.setSurveyStartNotificated(event.getSurveyId());
        }
    }
}
