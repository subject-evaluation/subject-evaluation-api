package ru.nsu.fit.events.listeners;

import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Component;
import ru.nsu.fit.events.model.StartSurveyNotificationEvent;
import ru.nsu.fit.events.publishers.SurveyNotificationSuccessEventPublisher;
import ru.nsu.fit.service.email.EmailService;

import javax.annotation.ParametersAreNonnullByDefault;

import static ru.nsu.fit.events.model.NotificationType.START_SURVEY;

@Log4j2
@Component
@RequiredArgsConstructor
@ParametersAreNonnullByDefault
public class StartSurveyNotificationEventListener
    implements ApplicationListener<StartSurveyNotificationEvent> {

    private final EmailService emailService;
    private final SurveyNotificationSuccessEventPublisher successNotificationEventPublisher;

    private final static String EMAIL_SUBJECT = "Опрос по курсу ";
    private final static String EMAIL_MESSAGE = "Расскажите, как вам курс? Что было полезного, интересного? " +
        "Напишите отзыв или оставьте благодарность преподавателям - ";

    @Override
    public void onApplicationEvent(StartSurveyNotificationEvent event) {
        log.info("Listener got start survey notification event");
        String subject = EMAIL_SUBJECT.concat(event.getCourseName());
        emailService.sendMessage(event.getEmails(), subject, EMAIL_MESSAGE);
        log.info("Email about starting survey was sent");
        successNotificationEventPublisher.publishSurveyNotificationSuccess(event.getSurveyId(), START_SURVEY);
    }
}
