package ru.nsu.fit.events.model;

public enum NotificationType {
    START_SURVEY,
    END_SURVEY
}
