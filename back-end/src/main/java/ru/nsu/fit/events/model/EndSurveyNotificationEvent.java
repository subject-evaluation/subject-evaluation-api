package ru.nsu.fit.events.model;

import javax.annotation.ParametersAreNonnullByDefault;
import java.util.List;

@ParametersAreNonnullByDefault
public class EndSurveyNotificationEvent extends SurveyNotificationEvent {
    public EndSurveyNotificationEvent(List<String> emails, String courseName, int surveyId) {
        super(emails, courseName, surveyId);
    }
}
