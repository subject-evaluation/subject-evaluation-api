package ru.nsu.fit.events.model;

import lombok.Getter;
import org.springframework.context.ApplicationEvent;

import javax.annotation.ParametersAreNonnullByDefault;
import java.util.List;

@Getter
@ParametersAreNonnullByDefault
public class SurveyNotificationEvent extends ApplicationEvent {
    private final String courseName;
    private final int surveyId;
    private final List<String> emails;

    public SurveyNotificationEvent(List<String> emails, String courseName, int surveyId) {
        super(courseName);
        this.courseName = courseName;
        this.surveyId = surveyId;
        this.emails = emails;
    }
}
