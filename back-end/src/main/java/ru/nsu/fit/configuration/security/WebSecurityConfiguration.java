package ru.nsu.fit.configuration.security;

import lombok.RequiredArgsConstructor;
import one.util.streamex.StreamEx;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import ru.nsu.fit.entity.enums.Role;

import javax.annotation.Nonnull;
import javax.annotation.ParametersAreNonnullByDefault;

import static ru.nsu.fit.entity.enums.Role.*;
import static ru.nsu.fit.util.paths.AuthPaths.*;
import static ru.nsu.fit.util.paths.CoursePaths.COURSES_ROOT_PATH;
import static ru.nsu.fit.util.paths.FeedbackPaths.FEEDBACK_ROOT_PATH;
import static ru.nsu.fit.util.paths.RootPaths.API_VERSION_PREFIX;
import static ru.nsu.fit.util.paths.RootPaths.API_VERSION_SECURE_PREFIX;
import static ru.nsu.fit.util.paths.SurveyPaths.SEARCH_PATH;
import static ru.nsu.fit.util.paths.SurveyPaths.SURVEY_ROOT_PATH;
import static ru.nsu.fit.util.paths.UserPaths.USERS_ROOT_PATH;

/**
 * Класс, являющийся конфигурацией Spring Security.
 * Здесь добавляется {@link GoogleOauth2RequestFilter} к стандартным фильтрам спринга,
 * назначаются роли, необходимые для доступа к определенным методам.
 */
@Configuration
@EnableWebSecurity
@RequiredArgsConstructor
@ParametersAreNonnullByDefault
public class WebSecurityConfiguration extends WebSecurityConfigurerAdapter {
    private final CustomAuthenticationEntryPoint authenticationEntryPoint;
    private final GoogleOauth2RequestFilter googleOauth2RequestFilter;
    private final ExceptionHandlerFilter exceptionHandlerFilter;

    @Bean
    @Override
    public AuthenticationManager authenticationManagerBean() throws Exception {
        return super.authenticationManagerBean();
    }

    @Override
    protected void configure(HttpSecurity httpSecurity) throws Exception {
        httpSecurity
            .cors()
            .and()
            .antMatcher(API_VERSION_SECURE_PREFIX + "/**")
            .authorizeRequests()
                .antMatchers(ROOT_AUTH_PATH, ROOT_REFRESH_PATH).permitAll()
                .antMatchers(ROOT_LOGOUT_PATH).authenticated()
                .antMatchers(HttpMethod.GET, COURSES_ROOT_PATH).hasAnyAuthority(authorities(STUDENT, TEACHER, EMPLOYEE))
                .antMatchers(HttpMethod.PUT, COURSES_ROOT_PATH).hasAnyAuthority(authorities(TEACHER, EMPLOYEE))
                .antMatchers(HttpMethod.POST, COURSES_ROOT_PATH).hasAnyAuthority(authorities(TEACHER, EMPLOYEE))
                .antMatchers(COURSES_ROOT_PATH + "/search").hasAnyAuthority(authorities(STUDENT, TEACHER, EMPLOYEE))
                .antMatchers(COURSES_ROOT_PATH + "/archive").hasAnyAuthority(authorities(TEACHER, EMPLOYEE))
                .antMatchers(COURSES_ROOT_PATH + "/unarchive").hasAnyAuthority(authorities(TEACHER, EMPLOYEE))
                .antMatchers(HttpMethod.GET, SURVEY_ROOT_PATH + "/*").hasAnyAuthority(authorities(TEACHER))
                .antMatchers(HttpMethod.POST, SURVEY_ROOT_PATH).hasAnyAuthority(authorities(TEACHER, EMPLOYEE))
                .antMatchers(HttpMethod.POST, SURVEY_ROOT_PATH + SEARCH_PATH).hasAnyAuthority(authorities(TEACHER, EMPLOYEE))
                .antMatchers(FEEDBACK_ROOT_PATH + "/update-status/**").hasAnyAuthority(authorities(TEACHER, EMPLOYEE))
                .antMatchers(USERS_ROOT_PATH + "/**").hasAnyAuthority(authorities(EMPLOYEE))
            .and()
            .addFilterBefore(googleOauth2RequestFilter, UsernamePasswordAuthenticationFilter.class)
            .addFilterBefore(exceptionHandlerFilter, GoogleOauth2RequestFilter.class);
        httpSecurity
            .csrf().disable()
            .authorizeRequests()
            .antMatchers(API_VERSION_PREFIX + "/*").permitAll()
            .antMatchers(HttpMethod.GET, COURSES_ROOT_PATH).hasAnyAuthority(authorities(STUDENT, TEACHER, EMPLOYEE))
            .antMatchers(HttpMethod.PUT, COURSES_ROOT_PATH).hasAnyAuthority(authorities(TEACHER, EMPLOYEE))
            .antMatchers(HttpMethod.POST, COURSES_ROOT_PATH).hasAnyAuthority(authorities(TEACHER, EMPLOYEE))
            .antMatchers(COURSES_ROOT_PATH + "/search").hasAnyAuthority(authorities(STUDENT, TEACHER, EMPLOYEE))
            .antMatchers(COURSES_ROOT_PATH + "/archive").hasAnyAuthority(authorities(TEACHER, EMPLOYEE))
            .antMatchers(COURSES_ROOT_PATH + "/unarchive").hasAnyAuthority(authorities(TEACHER, EMPLOYEE))
            .antMatchers(HttpMethod.GET, SURVEY_ROOT_PATH + "/*").hasAnyAuthority(authorities(TEACHER))
            .antMatchers(HttpMethod.POST, SURVEY_ROOT_PATH).hasAnyAuthority(authorities(TEACHER, EMPLOYEE))
            .antMatchers(FEEDBACK_ROOT_PATH + "/update-status/**").hasAnyAuthority(authorities(TEACHER, EMPLOYEE))
            .antMatchers(USERS_ROOT_PATH + "/**").hasAnyAuthority(authorities(EMPLOYEE))
            .and()
            .logout()
            .logoutUrl(ROOT_LOGOUT_PATH)
            .deleteCookies("JSESSIONID")
            .permitAll()
            .and()
            .exceptionHandling()
            .authenticationEntryPoint(authenticationEntryPoint);
    }

    @Nonnull
    private String[] authorities(Role role, Role... roles) {
        return StreamEx.of(role).append(roles).map(Role::getAuthority).toArray(String[]::new);
    }
}
