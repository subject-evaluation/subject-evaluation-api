package ru.nsu.fit.configuration;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.time.Clock;

/**
 * Предоставляет {@link Bean} для получения системного времени. <br/>
 * Этот {@link Bean} должен использоваться везде, где необходимо получение системного времени
 * для дальнейшего облегчения мокирования в тестах
 */
@Configuration
public class ClockConfiguration {
    @Bean
    public Clock clock() {
        return Clock.systemDefaultZone();
    }
}
