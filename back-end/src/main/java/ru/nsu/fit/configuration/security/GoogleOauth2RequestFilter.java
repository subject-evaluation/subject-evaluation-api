package ru.nsu.fit.configuration.security;

import java.io.IOException;

import javax.annotation.Nonnull;
import javax.annotation.ParametersAreNonnullByDefault;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import lombok.RequiredArgsConstructor;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;
import ru.nsu.fit.entity.User;
import ru.nsu.fit.service.user.UserService;

import static ru.nsu.fit.util.paths.AuthPaths.ROOT_LOGOUT_PATH;
import static ru.nsu.fit.util.paths.RootPaths.API_VERSION_SECURE_PREFIX;

/**
 * HTTP фильтр, выполняющий авторизацию с помощью OAuth2 от Google
 */
@Component
@RequiredArgsConstructor
@ParametersAreNonnullByDefault
public class GoogleOauth2RequestFilter extends OncePerRequestFilter {

    private static final String AUTH_HEADER = "Authorization";

    private static final String BEARER_PREFIX = "Bearer ";

    private final UserService userService;

    @Override
    protected void doFilterInternal(
        HttpServletRequest request,
        HttpServletResponse response,
        FilterChain filterChain
    ) throws ServletException, IOException {
        SecurityContext securityContext = SecurityContextHolder.getContext();
        String token = getTokenFromRequest(request);
        User user = userService.loadUserByAccessToken(token);
        if (isLoginNecessary(user.getUsername())) {
            if (user.isCredentialsNonExpired()) {
                securityContext.setAuthentication(getAuthentication(user));
            } else {
                throw new IllegalArgumentException("Token expired");
            }
        }
        filterChain.doFilter(request, response);
    }

    @Override
    protected boolean shouldNotFilter(HttpServletRequest request) throws ServletException {
        String requestUri = request.getRequestURI();
        return !(
            requestUri.startsWith(API_VERSION_SECURE_PREFIX)
                || ROOT_LOGOUT_PATH.equals(requestUri)
        );
    }

    @Nonnull
    private String getTokenFromRequest(HttpServletRequest request) {
        String bearerToken = request.getHeader(AUTH_HEADER);
        if (bearerToken != null && bearerToken.startsWith(BEARER_PREFIX)) {
            return bearerToken.substring(BEARER_PREFIX.length());
        } else {
            throw new IllegalArgumentException("Wrong HTTP Auth header value: " + bearerToken);
        }
    }

    @Nonnull
    private Authentication getAuthentication(User user) {
        return new UsernamePasswordAuthenticationToken(user, null, user.getAuthorities());
    }

    private boolean isLoginNecessary(String username) {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        return authentication == null || !username.equals(authentication.getName());
    }
}
