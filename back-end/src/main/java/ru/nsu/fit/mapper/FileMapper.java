package ru.nsu.fit.mapper;

import org.mapstruct.InjectionStrategy;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import ru.nsu.fit.dto.file.FileCreateDto;
import ru.nsu.fit.dto.file.FileResponseDto;
import ru.nsu.fit.entity.File;

@Mapper(componentModel = "spring", injectionStrategy = InjectionStrategy.CONSTRUCTOR)
public interface FileMapper {
    FileCreateDto fileToCreateDto(File file);
    @Mapping(target = "id", ignore = true)
    File dtoCreateToFile(FileCreateDto fileCreateDto);
    FileResponseDto fileToResponseDto(File file);
    File dtoResponseToFile(FileResponseDto fileResponseDto);
}
