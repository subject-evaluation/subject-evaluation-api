package ru.nsu.fit.mapper.survey;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import ru.nsu.fit.dto.survey.QuestionRequestDto;
import ru.nsu.fit.dto.survey.QuestionResponseDto;
import ru.nsu.fit.dto.survey.QuestionUpdateDto;
import ru.nsu.fit.entity.enums.QuestionType;
import ru.nsu.fit.entity.question.AnswerChoice;
import ru.nsu.fit.entity.question.MultipleOptionsQuestion;
import ru.nsu.fit.entity.question.OpenQuestion;
import ru.nsu.fit.entity.question.Question;
import ru.nsu.fit.entity.question.SingleOptionQuestion;

import javax.annotation.Nonnull;
import javax.annotation.ParametersAreNonnullByDefault;
import java.util.List;
import java.util.stream.Collectors;

@Component
@RequiredArgsConstructor
@ParametersAreNonnullByDefault
public class QuestionMapper {

    private final AnswerChoiceMapper answerChoiceMapper;

    @Nonnull
    public Question dtoToQuestion(QuestionRequestDto questionRequestDto) {
        return createQuestionByType(
            questionRequestDto.getType(),
            answerChoiceMapper.dtosToAnswerChoices(questionRequestDto.getAnswerChoices())
            )
            .setTitle(questionRequestDto.getTitle());
    }

    @Nonnull
    public List<Question> dtosToQuestions(List<QuestionRequestDto> questionRequestDtos) {
        return questionRequestDtos.stream()
            .map(this::dtoToQuestion)
            .collect(Collectors.toList());
    }

    @Nonnull
    public QuestionResponseDto questionToDto(Question question) {
        QuestionResponseDto dto = new QuestionResponseDto()
            .setId(question.getId())
            .setType(question.getQuestionType())
            .setTitle(question.getTitle());

        if (question instanceof SingleOptionQuestion) {
            SingleOptionQuestion singleOptionQuestion = (SingleOptionQuestion) question;
            dto.setAnswerChoices(
                answerChoiceMapper.answerChoicesToDtos(singleOptionQuestion.getAnswerChoices())
            );
        } else if (question instanceof MultipleOptionsQuestion) {
            MultipleOptionsQuestion singleOptionQuestion = (MultipleOptionsQuestion) question;
            dto.setAnswerChoices(
                answerChoiceMapper.answerChoicesToDtos(singleOptionQuestion.getAnswerChoices())
            );
        }
        return dto;
    }

    @Nonnull
    public List<QuestionResponseDto> questionsToDtos(List<Question> questions) {
        return questions.stream()
            .map(this::questionToDto)
            .collect(Collectors.toList());
    }

    @Nonnull
    public Question dtoToQuestion(QuestionUpdateDto questionUpdateDto) {
        Question question = createQuestionByType(
            questionUpdateDto.getType(),
            answerChoiceMapper.dtosUpdateToAnswerChoices(questionUpdateDto.getAnswerChoices())
        ).setTitle(questionUpdateDto.getTitle());
        question.setId(questionUpdateDto.getId());
        return question;
    }

    @Nonnull
    public List<Question> dtosUpdateToQuestions(List<QuestionUpdateDto> questionUpdateDtos) {
        return questionUpdateDtos.stream()
            .map(this::dtoToQuestion)
            .collect(Collectors.toList());
    }

    @Nonnull
    private Question createQuestionByType(QuestionType questionType, List<AnswerChoice> answerChoices) {
        Question question;
        switch (questionType) {
            case SINGLE_OPTION:
                SingleOptionQuestion singleOptionQuestion = new SingleOptionQuestion();
                singleOptionQuestion.setAnswerChoices(answerChoices);
                question = singleOptionQuestion;
                break;
            case MULTIPLE_OPTIONS:
                MultipleOptionsQuestion multipleOptionsQuestion = new MultipleOptionsQuestion();
                multipleOptionsQuestion.setAnswerChoices(answerChoices);
                question = multipleOptionsQuestion;
                break;
            case OPEN:
                question = new OpenQuestion();
                break;
            default:
                throw new IllegalStateException("Unknown question type");
        }
        return question;
    }
}
