package ru.nsu.fit.mapper.survey;

import org.mapstruct.InjectionStrategy;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import ru.nsu.fit.dto.survey.AnswerChoiceRequestDto;
import ru.nsu.fit.dto.survey.AnswerChoiceResponseDto;
import ru.nsu.fit.dto.survey.AnswerChoiceUpdateDto;
import ru.nsu.fit.entity.question.AnswerChoice;

import javax.annotation.Nonnull;
import javax.annotation.ParametersAreNonnullByDefault;
import java.util.List;

@ParametersAreNonnullByDefault
@Mapper(componentModel = "spring", injectionStrategy = InjectionStrategy.CONSTRUCTOR)
public interface AnswerChoiceMapper {

    @Nonnull
    @Mapping(target = "id", ignore = true)
    @Mapping(target = "question", ignore = true)
    AnswerChoice dtoToAnswerChoice(AnswerChoiceRequestDto answerChoiceRequestDto);

    @Nonnull
    List<AnswerChoice> dtosToAnswerChoices(List<AnswerChoiceRequestDto> answerChoiceRequestDtos);

    @Nonnull
    AnswerChoiceResponseDto answerChoiceToDto(AnswerChoice answerChoice);

    @Nonnull
    List<AnswerChoiceResponseDto> answerChoicesToDtos(List<AnswerChoice> answerChoices);

    @Nonnull
    @Mapping(target = "question", ignore = true)
    AnswerChoice dtoToAnswerChoice(AnswerChoiceUpdateDto answerChoiceUpdateDto);

    @Nonnull
    List<AnswerChoice> dtosUpdateToAnswerChoices(List<AnswerChoiceUpdateDto> answerChoiceUpdateDtos);
}
