package ru.nsu.fit.mapper.survey;

import org.mapstruct.InjectionStrategy;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.springframework.data.domain.Page;
import ru.nsu.fit.dto.survey.SurveyRequestDto;
import ru.nsu.fit.dto.survey.SurveyResponseDto;
import ru.nsu.fit.dto.survey.SurveyUpdateDto;
import ru.nsu.fit.entity.Identifiable;
import ru.nsu.fit.entity.Survey;

import javax.annotation.Nonnull;
import javax.annotation.ParametersAreNonnullByDefault;
import java.util.List;
import java.util.stream.Collectors;


@ParametersAreNonnullByDefault
@Mapper(uses = QuestionMapper.class, componentModel = "spring", injectionStrategy = InjectionStrategy.CONSTRUCTOR)
public abstract class SurveyMapper {

    @Nonnull
    @Mapping(target = "id", ignore = true)
    @Mapping(target = "author", ignore = true)
    @Mapping(target = "course", ignore = true)
    @Mapping(target = "teachers", ignore = true)
    @Mapping(target = "students", ignore = true)
    @Mapping(target = "feedbacks", ignore = true)
    @Mapping(target = "startNotificated", ignore = true)
    @Mapping(target = "endNotificated", ignore = true)
    public abstract Survey dtoToSurvey(@Nonnull SurveyRequestDto surveyRequestDto);

    @Nonnull
    @Mapping(target = "authorId", ignore = true)
    @Mapping(target = "courseId", ignore = true)
    @Mapping(target = "teacherIds", ignore = true)
    @Mapping(target = "studentIds", ignore = true)
    public abstract SurveyResponseDto survey2ResponseDto(@Nonnull Survey survey);

    @Nonnull
    public Page<SurveyResponseDto> surveys2Dtos(@Nonnull Page<Survey> surveyPage) {
        return surveyPage.map(this::survey2Dto);
    }

    @Nonnull
    public SurveyResponseDto survey2Dto(@Nonnull Survey survey) {
        List<Integer> studentIds = survey.getStudents().stream()
            .map(Identifiable::getId)
            .collect(Collectors.toList());
        List<Integer> teacherIds = survey.getTeachers().stream()
            .map(Identifiable::getId)
            .collect(Collectors.toList());
        return survey2ResponseDto(survey)
            .setAuthorId(survey.getAuthor().getId())
            .setCourseId(survey.getCourse().getId())
            .setStudentIds(studentIds)
            .setTeacherIds(teacherIds);
    }

    @Nonnull
    @Mapping(target = "id", ignore = true)
    @Mapping(target = "author", ignore = true)
    @Mapping(target = "course", ignore = true)
    @Mapping(target = "teachers", ignore = true)
    @Mapping(target = "students", ignore = true)
    @Mapping(target = "feedbacks", ignore = true)
    @Mapping(target = "questions", ignore = true)
    @Mapping(target = "startNotificated", ignore = true)
    @Mapping(target = "endNotificated", ignore = true)
    public abstract Survey dtoToSurvey(SurveyUpdateDto surveyUpdateDto);

}
