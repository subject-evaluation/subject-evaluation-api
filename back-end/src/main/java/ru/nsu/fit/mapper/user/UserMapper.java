package ru.nsu.fit.mapper.user;

import java.time.Clock;
import java.util.Map;
import java.util.stream.Collectors;

import javax.annotation.Nonnull;
import javax.annotation.ParametersAreNonnullByDefault;

import com.google.api.client.googleapis.auth.oauth2.GoogleIdToken;
import com.google.api.client.googleapis.auth.oauth2.GoogleTokenResponse;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import org.springframework.stereotype.Component;
import ru.nsu.fit.dto.user.GoogleUser;
import ru.nsu.fit.dto.user.UserRequestDto;
import ru.nsu.fit.dto.user.UserResponseDto;
import ru.nsu.fit.entity.User;
import ru.nsu.fit.entity.UserRole;
import ru.nsu.fit.entity.enums.Role;

@Component
@RequiredArgsConstructor
@ParametersAreNonnullByDefault
public class UserMapper {
    private final Clock clock;

    @Nonnull
    @SneakyThrows
    public GoogleUser map(GoogleTokenResponse tokenResponse) {
        GoogleIdToken.Payload payload = tokenResponse.parseIdToken().getPayload();
        return new GoogleUser()
            .setAccessToken(tokenResponse.getAccessToken())
            .setRefreshToken(tokenResponse.getRefreshToken())
            .setAccessTokenExpireDate(clock.instant().plusSeconds(tokenResponse.getExpiresInSeconds()))
            .setEmail(payload.getEmail())
            .setFirstName((String) payload.get("given_name"))
            .setLastName((String) payload.get("family_name"))
            .setGoogleId(payload.getSubject());
    }

    @Nonnull
    public User mapToEntity(User user, UserRequestDto dto, Map<Role, UserRole> roleMap) {
        return user
            .setEmail(dto.getEmail())
            .setFirstName(dto.getFirstName())
            .setLastName(dto.getLastName())
            .setPatronymic(dto.getPatronymic())
            .setRoles(dto.getRoles().stream().map(roleMap::get).collect(Collectors.toSet()))
            .setGroupNumber(dto.getGroupNumber());
    }

    @Nonnull
    public static UserRequestDto mapUser(User user) {
        UserRequestDto userRequestDto = new UserRequestDto();
        userRequestDto.setEmail(user.getEmail());
        userRequestDto.setFirstName(user.getFirstName());
        userRequestDto.setLastName(user.getLastName());
        userRequestDto.setPatronymic(user.getPatronymic());
        userRequestDto.setGroupNumber(user.getGroupNumber());
        userRequestDto.setStatus(user.getStatus());
        userRequestDto.setRoles(user.getRoles().stream().map(UserRole::getName).collect(Collectors.toSet()));
        return userRequestDto;
    }

    @Nonnull
    public UserResponseDto mapToApi(User user) {
        return new UserResponseDto()
            .setId(user.getId())
            .setEmail(user.getEmail())
            .setLastName(user.getLastName())
            .setFirstName(user.getFirstName())
            .setPatronymic(user.getPatronymic())
            .setGroupNumber(user.getGroupNumber())
            .setRoles(user.getRoles().stream().map(UserRole::getName).collect(Collectors.toSet()))
            .setStatus(user.getStatus());
    }
}
