package ru.nsu.fit.mapper.feedback;

import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import ru.nsu.fit.dto.feedback.FeedbackDto;
import ru.nsu.fit.entity.Feedback;
import ru.nsu.fit.entity.Survey;
import ru.nsu.fit.entity.User;
import ru.nsu.fit.entity.UserAnswer;
import ru.nsu.fit.util.feedback.Answer;
import ru.nsu.fit.util.feedback.UserDto;

@Component
@RequiredArgsConstructor
public class FeedbackDtoMapper {

    public static FeedbackDto map(Feedback feedback) {
        FeedbackDto feedbackDto = new FeedbackDto();
        feedbackDto.setCreationDate(feedback.getCreationDate());
        feedbackDto.setPublishingDate(feedback.getPublishingDate());
        Survey survey = feedback.getSurvey();
        feedbackDto.setCourseName(survey.getCourse().getName());
        feedbackDto.setYear(survey.getYear());
        feedbackDto.setSemester(survey.getSemester());
        feedbackDto.setFeedbackStatus(feedback.getStatus());
        feedbackDto.setAuthor(mapUserDto(feedback.getAuthor()));

        Set<User> users = survey.getTeachers();
        List<UserDto> teachers = new LinkedList<>();
        users.forEach(user -> teachers.add(mapUserDto(user)));
        feedbackDto.setTeachers(teachers);

        List<Answer> answers = new LinkedList<>();
        feedback.getUserAnswers().forEach(ans -> answers.add(mapAnswer(ans)));
        feedbackDto.setAnswers(answers);

        return feedbackDto;
    }

    public static UserDto mapUserDto(User user) {
        UserDto result = new UserDto();
        result.setEmail(user.getEmail());
        result.setFirstName(user.getFirstName());
        result.setLastName(user.getLastName());
        result.setPatronymic(user.getPatronymic());

        return result;
    }

    public static Answer mapAnswer(UserAnswer userAnswer) {
        Answer answer = new Answer();
        answer.setAnswerText(userAnswer.getAnswerText());
        answer.setQuestionId(userAnswer.getQuestion().getId());
        answer.setQuestionTitle(userAnswer.getQuestion().getTitle());
        List<String> answerChoice = new LinkedList<>();
        userAnswer.getPickedAnswerChoices().forEach(ans -> answerChoice.add(ans.getTitle()));
        answer.setAnswerChoice(answerChoice);

        return answer;
    }

}
