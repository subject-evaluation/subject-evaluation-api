package ru.nsu.fit.service.auth;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import ru.nsu.fit.entity.User;
import ru.nsu.fit.entity.enums.Role;

import java.util.Arrays;
import java.util.Collection;
import java.util.stream.Collectors;

@Service
public class AuthManager {

    public boolean hasRole(final Role role) {
        return hasAnyRole(role);
    }

    public boolean hasAnyRole(final Role... roles) {
        return hasAnyRole(Arrays.stream(roles).map(Role::name).collect(Collectors.toList()));
    }

    public User getUser() {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        if (auth == null) {
            return null;
        }

        if (auth.getPrincipal() instanceof User) {
            return (User) auth.getPrincipal();
        }

        return null;
    }

    private boolean hasAnyRole(final Collection<String> roles) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        return auth != null && auth.getAuthorities().stream().anyMatch(authority -> roles.contains(authority.getAuthority()));
    }

}
