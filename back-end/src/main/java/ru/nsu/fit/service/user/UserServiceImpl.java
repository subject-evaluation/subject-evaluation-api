package ru.nsu.fit.service.user;

import java.security.Principal;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.annotation.ParametersAreNonnullByDefault;
import javax.servlet.http.HttpServletRequest;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.web.authentication.logout.SecurityContextLogoutHandler;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.nsu.fit.dao.UserRepository;
import ru.nsu.fit.dto.auth.TokensDto;
import ru.nsu.fit.dto.filter.UserFilter;
import ru.nsu.fit.dto.user.GoogleUser;
import ru.nsu.fit.entity.User;
import ru.nsu.fit.entity.UserRole;
import ru.nsu.fit.entity.enums.Role;
import ru.nsu.fit.entity.enums.UserStatus;
import ru.nsu.fit.exceptions.DataNotFoundException;
import ru.nsu.fit.service.auth.GoogleAuthService;

@Slf4j
@Service
@Transactional
@RequiredArgsConstructor
@ParametersAreNonnullByDefault
public class UserServiceImpl implements UserService {

    private final UserRepository userRepository;
    private final GoogleAuthService googleAuthService;
    private final RoleService roleService;

    @Value("${user.email-domain}")
    private String validDomain;

    @Nonnull
    @Override
    public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
        Optional<User> optionalUser = userRepository.findByEmailIgnoreCase(email);
        if (optionalUser.isEmpty()) {
            throw new UsernameNotFoundException("Пользователь с email " + email + " не найден");
        }
        return optionalUser.get();
    }

    @Nonnull
    @Override
    public User loadUserByAccessToken(String accessToken) throws UsernameNotFoundException {
        Optional<User> optionalUser = userRepository.findByAccessToken(accessToken);
        if (optionalUser.isEmpty()) {
            throw new UsernameNotFoundException("Пользователь не найден");
        }
        return optionalUser.get();
    }

    @Nonnull
    @Override
    public TokensDto authorize(String authCode) {
        GoogleUser googleUser = googleAuthService.authorize(authCode);
        User user = createOrUpdateUser(googleUser);
        return new TokensDto(user.getEnumRoles(), user.getAccessToken(), user.getRefreshToken(), user.getAccessTokenExpireDate());
    }

    /**
     * Обновляет access и refresh токены пользователя
     */
    @Nonnull
    @Override
    public TokensDto refreshTokens(String refreshToken) {
        Optional<User> optionalUser = userRepository.findByRefreshToken(refreshToken);
        if (optionalUser.isEmpty()) {
            throw new UsernameNotFoundException("Пользователь не найден");
        }
        User user = optionalUser.get();
        GoogleUser googleUser = googleAuthService.refreshAccessToken(user);
        userRepository.save(
            user.setAccessToken(googleUser.getAccessToken())
                .setAccessTokenExpireDate(googleUser.getAccessTokenExpireDate())
        );
        log.info("Updated tokens for user {}", user.getEmail());
        return new TokensDto(user.getEnumRoles(), user.getAccessToken(), null, user.getAccessTokenExpireDate());
    }

    /**
     * В этом методе сначала удаляются токены, а потом происходит стандартный logout Spring Security
     */
    @Override
    public void logout(HttpServletRequest request) {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (authentication != null) {
            User user = getUserWithCheck(authentication);
            user.setAccessToken(null);
            user.setRefreshToken(null);
            user.setAccessTokenExpireDate(null);
            userRepository.save(user);
            new SecurityContextLogoutHandler().logout(request, null, authentication);
            log.info("User {} logged out", user.getEmail());
        }
    }

    /**
     * Получение пользователя из бд по спринговской сущности пользователя
     *
     * @param principal - сущность Spring, содержащая в себе информацию о текущем авторизованном пользователе
     * @throws UsernameNotFoundException если в системе нет авторизованного пользователя
     */
    @Nonnull
    @Override
    public User getUserWithCheck(@Nullable Principal principal) {
        if (principal == null) {
            throw new UsernameNotFoundException("Пользователь не найден");
        }
        return (User) loadUserByUsername(principal.getName());
    }

    @Nonnull
    @Override
    public User getById(int id) {
        return userRepository.findById(id)
            .orElseThrow(() -> new DataNotFoundException("Пользователь с id " + id + " не найден"));
    }

    @NotNull
    @Override
    public User save(User user) {
        return userRepository.save(user);
    }

    @Override
    public Optional<User> findByEmail(String email) {
        return userRepository.findByEmail(email);
    }

    @NotNull
    @Override
    public Page<User> searchUsers(UserFilter filter, Pageable pageable) {
        return userRepository.findByStatusInAndLineIn(
            filter.getStatuses(),
            filter.getLine(),
            pageable
        );
    }

    @Nonnull
    private User createOrUpdateUser(GoogleUser googleUser) {
        String email = googleUser.getEmail();
        Optional<User> optionalUser = userRepository.findByEmailIgnoreCase(email);

        if (optionalUser.isEmpty()) {
            String domain = email.substring(email.indexOf('@') + 1);
            if (!validDomain.equalsIgnoreCase(domain)) {
                throw new UsernameNotFoundException("User not found");
            }
            return createUser(googleUser);
        }

        User user = optionalUser.get();
        if (!user.isEnabled()) {
            throw new UsernameNotFoundException("User is blocked");
        }

        if (!user.getGoogleId().equals(googleUser.getGoogleId())) {
            throw new UsernameNotFoundException("User has different googleId");
        }
        return updateUser(googleUser, user);
    }

    private User updateUser(GoogleUser googleUser, User user) {
        user.setGoogleId(googleUser.getGoogleId())
            .setAccessToken(googleUser.getAccessToken())
            .setAccessTokenExpireDate(googleUser.getAccessTokenExpireDate());
        userRepository.save(user);
        log.info("Authorized user {}", user);
        return user;
    }

    @NotNull
    private User createUser(GoogleUser googleUser) {
        UserRole role = roleService.getRoleByName(Role.STUDENT);
        User newUser = new User()
            .setEmail(googleUser.getEmail())
            .setFirstName(googleUser.getFirstName())
            .setLastName(googleUser.getLastName())
            .setGroupNumber(null)
            .setGoogleId(googleUser.getGoogleId())
            .setAccessToken(googleUser.getAccessToken())
            .setRefreshToken(googleUser.getRefreshToken())
            .setAccessTokenExpireDate(googleUser.getAccessTokenExpireDate())
            .setStatus(UserStatus.ACTIVE)
            .setRoles(Set.of(role));
        User user = userRepository.save(newUser);
        log.info("Add new user {}", user);
        return user;
    }

    @Override
    public void updateStatusUser(Set<Integer> idUsers, UserStatus userStatus) {
        List<User> userList = userRepository.getByIdIn(idUsers);
        userList.forEach(user -> user.setStatus(userStatus));
        userRepository.saveAll(userList);
    }

}
