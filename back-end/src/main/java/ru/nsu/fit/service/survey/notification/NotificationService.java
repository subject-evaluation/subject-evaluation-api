package ru.nsu.fit.service.survey.notification;

public interface NotificationService {
    /**
     * Отправка email уведомлений о начале опроса
     */
    void notifyStartSurvey();

    /**
     * Отправка email уведомлений об окончании опроса
     */
    void notifyEndSurvey();
}
