package ru.nsu.fit.service.email;

import lombok.extern.log4j.Log4j2;
import org.springframework.core.env.Environment;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Service;

import javax.annotation.ParametersAreNonnullByDefault;
import java.util.List;

@Log4j2
@Service
@ParametersAreNonnullByDefault
public class EmailServiceImpl implements EmailService {
    private final JavaMailSender mailSender;

    private final String FROM_EMAIL;

    public EmailServiceImpl(JavaMailSender mailSender, Environment environment) {
        this.mailSender = mailSender;
        FROM_EMAIL = environment.getProperty("spring.mail.username");
    }

    @Override
    public void sendMessage(List<String> receiverEmails, String subject, String text) {
        SimpleMailMessage mailMessage = new SimpleMailMessage();
        mailMessage.setFrom(FROM_EMAIL);
        mailMessage.setTo(receiverEmails.toArray(new String[0]));
        mailMessage.setSubject(subject);
        mailMessage.setText(text);
        log.info("Sending email message to " + receiverEmails);
        mailSender.send(mailMessage);
    }
}
