package ru.nsu.fit.service.feedback;

import java.util.Collection;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.function.Function;
import java.util.stream.Stream;

import javax.annotation.ParametersAreNonnullByDefault;
import javax.transaction.Transactional;

import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import ru.nsu.fit.dao.AnswerChoiceRepository;
import ru.nsu.fit.dao.FeedbackRepository;
import ru.nsu.fit.dao.QuestionRepository;
import ru.nsu.fit.dao.SurveyRepository;
import ru.nsu.fit.dao.UserRepository;
import ru.nsu.fit.dto.feedback.FeedbackCreateDto;
import ru.nsu.fit.dto.feedback.FeedbackDto;
import ru.nsu.fit.dto.feedback.FeedbackFilter;
import ru.nsu.fit.dto.feedback.FeedbackUpdateDto;
import ru.nsu.fit.entity.Feedback;
import ru.nsu.fit.entity.Survey;
import ru.nsu.fit.entity.User;
import ru.nsu.fit.entity.UserAnswer;
import ru.nsu.fit.entity.enums.FeedbackStatus;
import ru.nsu.fit.entity.enums.Role;
import ru.nsu.fit.entity.question.AnswerChoice;
import ru.nsu.fit.entity.question.Question;
import ru.nsu.fit.exceptions.DataNotFoundException;
import ru.nsu.fit.exceptions.SEAccessDeniedException;
import ru.nsu.fit.mapper.feedback.FeedbackDtoMapper;
import ru.nsu.fit.service.auth.AuthManager;
import ru.nsu.fit.specification.FeedbackSpecificationFactory;
import ru.nsu.fit.util.CollectionUtils;
import ru.nsu.fit.util.feedback.createFeedback.CreateAnswer;
import ru.nsu.fit.util.feedback.specification.FeedbackCourseIdSpecification;
import ru.nsu.fit.util.feedback.specification.FeedbackStatusesSpecification;

@Service
@Transactional
@RequiredArgsConstructor
@ParametersAreNonnullByDefault
public class FeedbackServiceImpl implements FeedbackService {
    private final static String FEEDBACK_NOT_FOUND = "Не найден отзыв с идентификатором %d";
    private final static String TEACHER_ACCESS_DENIED = "Доступ запрещен. Вы можете модерировать отзывы " +
        "только тех опросов, которые создали сами или в которых являетесь преподавателем";

    private final AuthManager authManager;
    private final FeedbackRepository feedbackRepository;
    private final UserRepository userRepository;
    private final SurveyRepository surveyRepository;
    private final QuestionRepository questionRepository;
    private final AnswerChoiceRepository answerChoiceRepository;
    private final FeedbackSpecificationFactory feedbackSpecificationFactory;

    @Override
    public void updateStatus(int feedbackId, FeedbackStatus status) throws DataNotFoundException {
        Feedback feedback = feedbackRepository.findById(feedbackId)
            .orElseThrow(() -> new DataNotFoundException(String.format(FEEDBACK_NOT_FOUND, feedbackId)));
        if (authManager.hasRole(Role.TEACHER)) {
            Survey survey = feedback.getSurvey();
            User teacher = authManager.getUser();
            if (!survey.getAuthor().getId().equals(teacher.getId()) &&
                survey.getTeachers().stream().noneMatch(t -> t.getId().equals(teacher.getId()))) {
                throw new SEAccessDeniedException(TEACHER_ACCESS_DENIED);
            }
        }
        feedback.setStatus(status);
    }

    @Override
    public Page<Feedback> searchFeedbacks(FeedbackFilter filter, Pageable pageable) {
        if (
            Stream.<Function<FeedbackFilter, Collection<?>>>of(
                FeedbackFilter::getCourseIds,
                FeedbackFilter::getStatuses
            )
                .map(getter -> getter.apply(filter))
                .anyMatch(CollectionUtils::isNonnullAndEmpty)
        ) {
            return Page.empty();
        }
        return feedbackRepository.findAll(
            feedbackSpecificationFactory.fromFilter(filter),
            pageable
        );
    }

    @Override
    public Page<FeedbackDto> getFeedback(FeedbackFilter feedbackFilter, Pageable pageable) {
        FeedbackCourseIdSpecification spec1 = new FeedbackCourseIdSpecification(feedbackFilter.getCourseIds());
        FeedbackStatusesSpecification spec2 = new FeedbackStatusesSpecification(feedbackFilter.getStatuses());
        Page<Feedback> pages = feedbackRepository.findAll(Specification.where(spec1).and(spec2), pageable);
        return pages.map(FeedbackDtoMapper::map);
    }

    private List<UserAnswer> calculateNewAnswers(List<CreateAnswer> userAnswers, Feedback feedback) {
        List<UserAnswer> userAnswersResult = new LinkedList<>();

        for (CreateAnswer answer : userAnswers) {
            Optional<Question> optionalQuestion = questionRepository.findById(answer.getQuestionId());
            if (optionalQuestion.isEmpty()) {
                throw new IllegalArgumentException("Question not found");
            }
            Question question = optionalQuestion.get();
            UserAnswer userAnswer = new UserAnswer();
            userAnswer.setAnswerText(answer.getAnswerText());
            userAnswer.setFeedback(feedback);
            userAnswer.setQuestion(question);

            List<AnswerChoice> pickedAnswerChoices = new LinkedList<>();
            for (Integer answerChoiceId : answer.getAnswerChoiceId()) {
                Optional<AnswerChoice> optionalAnswerChoice = answerChoiceRepository.findById(answerChoiceId);
                if (optionalAnswerChoice.isEmpty()) {
                    throw new IllegalArgumentException("AnswerChoice not found");
                }
                AnswerChoice answerChoice = optionalAnswerChoice.get();
                pickedAnswerChoices.add(answerChoice);
            }
            userAnswer.setPickedAnswerChoices(pickedAnswerChoices);

            userAnswersResult.add(userAnswer);
        }

        return userAnswersResult;
    }

    @Override
    public long createFeedback(FeedbackCreateDto feedbackCreateDto) {
        Optional<User> optionalUser = userRepository.findById(feedbackCreateDto.getUserId());
        if (optionalUser.isEmpty()) {
            throw new IllegalArgumentException("User not found");
        }
        User user = optionalUser.get();

        Optional<Survey> optionalSurvey = surveyRepository.findById(feedbackCreateDto.getSurveyId());
        if (optionalSurvey.isEmpty()) {
            throw new IllegalArgumentException("Survey not found");
        }
        Survey survey = optionalSurvey.get();

        Feedback feedback = new Feedback();
        feedback.setCreationDate(feedbackCreateDto.getCreationDate());
        feedback.setPublishingDate(feedbackCreateDto.getPublishingDate());
        feedback.setStatus(feedbackCreateDto.getStatus());
        feedback.setAuthor(user);
        feedback.setSurvey(survey);

        feedback = feedbackRepository.save(feedback);

        List<CreateAnswer> userAnswers = feedbackCreateDto.getUserAnswers();

        List<UserAnswer> userAnswersResult = calculateNewAnswers(userAnswers, feedback);

        feedback.setUserAnswers(userAnswersResult);

        return feedbackRepository.save(feedback).getId();
    }

    @Override
    public FeedbackDto updateFeedback(FeedbackUpdateDto feedbackUpdateDto) {
        Optional<Feedback> optionalFeedback = feedbackRepository.findByAuthor_IdAndSurvey_Id(
                                                                                feedbackUpdateDto.getUserId()
                                                                                , feedbackUpdateDto.getSurveyId());
        if (optionalFeedback.isEmpty()) {
            throw new IllegalArgumentException("Feedback not found");
        }
        Feedback feedback = optionalFeedback.get();

        if (null != feedbackUpdateDto.getCreationDate()) {
            feedback.setCreationDate(feedbackUpdateDto.getCreationDate());
        }
        if (null != feedbackUpdateDto.getPublishingDate()) {
            feedback.setPublishingDate(feedbackUpdateDto.getPublishingDate());
        }

        List<CreateAnswer> userAnswers = feedbackUpdateDto.getUserAnswers();
        if (null != userAnswers) {
            List<UserAnswer> userAnswersResult = calculateNewAnswers(userAnswers, feedback);

            feedback.setUserAnswers(userAnswersResult);
        }

        Feedback feedbackResult = feedbackRepository.save(feedback);

        return FeedbackDtoMapper.map(feedbackResult);
    }

    @Override
    public FeedbackDto getUserFeedback(Integer userId, Integer surveyId) {
        Optional<Feedback> optionalFeedback= feedbackRepository.findByAuthor_IdAndSurvey_Id(userId, surveyId);
        if (optionalFeedback.isEmpty()) {
            throw new IllegalArgumentException("Feedback not found");
        }
        Feedback feedback = optionalFeedback.get();

        return FeedbackDtoMapper.map(feedback);
    }

}
