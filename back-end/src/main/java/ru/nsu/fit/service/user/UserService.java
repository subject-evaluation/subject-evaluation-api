package ru.nsu.fit.service.user;

import java.security.Principal;
import java.util.Optional;
import java.util.Set;

import javax.annotation.Nonnull;
import javax.annotation.ParametersAreNonnullByDefault;
import javax.servlet.http.HttpServletRequest;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import ru.nsu.fit.dto.auth.TokensDto;
import ru.nsu.fit.dto.filter.UserFilter;
import ru.nsu.fit.entity.User;
import ru.nsu.fit.entity.enums.UserStatus;

@ParametersAreNonnullByDefault
public interface UserService extends UserDetailsService {

    /**
     * Получение пользователя по его email.
     *
     * @param email - email пользователя
     * @throws UsernameNotFoundException если пользователь с таким логином не найден
     */
    @Nonnull
    @Override
    UserDetails loadUserByUsername(String email) throws UsernameNotFoundException;

    /**
     * Получение пользователя по его access токену.
     *
     * @param accessToken - токен пользователя
     * @throws UsernameNotFoundException если пользователь не найден
     */
    @Nonnull
    User loadUserByAccessToken(String accessToken) throws UsernameNotFoundException;

    /**
     * Метод авторизации пользователей. Если пользователя нет - заполняет первоначальную информацию
     * от Google и сохраняет нового пользователя + сохраняет токены.
     * Если пользователь есть - просто сохраняет токены.
     * Возвращает refresh токен пользователя
     *
     * @param authCode - код авторизации Google
     */
    @Nonnull
    TokensDto authorize(String authCode);

    /**
     * Обновляет access и refresh токены пользователя
     */
    @Nonnull
    TokensDto refreshTokens(String refreshToken);

    /**
     * В этом методе сначала удаляются токены, а потом происходит стандартный logout Spring Security
     */
    void logout(HttpServletRequest request);

    /**
     * Получение пользователя из бд по спринговской сущности пользователя
     *
     * @param principal - сущность Spring, содержащая в себе информацию о текущем авторизованном пользователе
     * @throws UsernameNotFoundException если в системе нет авторизованного пользователя
     */
    @Nonnull
    User getUserWithCheck(Principal principal);

    /**
     * Изменение статуса пользователя
     *
     * @param idUsers - идентификаторы пользователей
     * @param userStatus - статус пользователя, на который нужно обновить текущий статус
     */
    void updateStatusUser(Set<Integer> idUsers, UserStatus userStatus);

    /**
     * Получение пользователя по его id
     */
    @Nonnull
    User getById(int id);

    /**
     * Поиск пользователей по заданным параметрам
     *
     * @param filter   фильтр, содержащий параметры для поиска
     * @param pageable настройки для пагинации
     * @return страница пользователей
     */
    @Nonnull
    Page<User> searchUsers(UserFilter filter, Pageable pageable);

    /**
     * Сохранить пользователя.
     *
     * @param user пользователь
     * @return сохраненный пользователь
     */
    @Nonnull
    User save(User user);

    /**
     * Найти пользователя по email.
     *
     * @param email электронная почта
     * @return опциональный пользователь
     */
    Optional<User> findByEmail(String email);
}
