package ru.nsu.fit.service.file;

import javax.annotation.Nonnull;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import ru.nsu.fit.dao.FileRepository;
import ru.nsu.fit.entity.File;
import ru.nsu.fit.exceptions.DataNotFoundException;

@Service
@Transactional
@RequiredArgsConstructor
public class FileServiceImpl implements FileService {
    private final static String FILE_NOT_FOUND = "Не найден файл курса с идентификатором ";
    private final FileRepository fileRepository;

    @Override
    public File saveFile(File file) {
        File savedFile = fileRepository.save(file);
        savedFile.setUrl(createFileUrl(file.getId()));
        return savedFile;
    }

    @Override
    public File getFileById(int id) {
        return fileRepository.findById(id)
            .orElseThrow(() -> new DataNotFoundException(FILE_NOT_FOUND + id));
    }

    @Nonnull
    private String createFileUrl(Integer id) {
        return ServletUriComponentsBuilder.fromCurrentContextPath()
            .path("api/v1/files/" + id + "/download")
            .toUriString();
    }
}
