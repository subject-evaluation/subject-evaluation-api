package ru.nsu.fit.service.email;

import java.util.List;

public interface EmailService {
    /**
     * Отправка email по заданным адресам
     *
     * @param receiverEmails список email получателей
     * @param subject        тема письма
     * @param text           содержание письма
     */
    void sendMessage(List<String> receiverEmails, String subject, String text);
}
