package ru.nsu.fit.service.survey;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import ru.nsu.fit.dao.SurveyRepository;
import ru.nsu.fit.dto.filter.SurveyFilter;
import ru.nsu.fit.entity.Course;
import ru.nsu.fit.entity.Survey;
import ru.nsu.fit.dao.AnswerChoiceRepository;
import ru.nsu.fit.dao.QuestionRepository;
import ru.nsu.fit.entity.User;
import ru.nsu.fit.entity.UserRole;
import ru.nsu.fit.entity.enums.Role;
import ru.nsu.fit.entity.question.AnswerChoice;
import ru.nsu.fit.entity.question.Question;
import ru.nsu.fit.exceptions.DataNotFoundException;

import javax.annotation.Nonnull;
import javax.annotation.ParametersAreNonnullByDefault;
import javax.transaction.Transactional;
import javax.ws.rs.ForbiddenException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.time.LocalDate;
import java.util.Objects;

@Slf4j
@Service
@Transactional
@RequiredArgsConstructor
@ParametersAreNonnullByDefault
public class SurveyServiceImpl implements SurveyService {
    private static final String SURVEY_NOT_FOUND = "Не найден опрос с идентификатором ";

    private final SurveyRepository surveyRepository;
    private final QuestionRepository questionRepository;
    private final AnswerChoiceRepository answerChoiceRepository;

    @Nonnull
    @Override
    public Survey getSurvey(int id) {
        return surveyRepository.findById(id)
            .orElseThrow(() -> new DataNotFoundException(SURVEY_NOT_FOUND + id));
    }

    @Nonnull
    @Override
    public Survey createSurvey(Survey survey) {
        return surveyRepository.save(survey);
    }

    @Nonnull
    @Override
    public List<Question> updateQuestions(User user, int surveyId, List<Question> updatingQuestions) {
        Survey existingSurvey = getSurvey(surveyId);
        checkUpdatePermissions(user, existingSurvey);
        if (!existingSurvey.getFeedbacks().isEmpty()) {
            throw new IllegalArgumentException("На изменяемые вопросы опроса уже существуют отзывы");
        }
        Map<Question, Boolean> foundQuestions = new HashMap<>();
        Map<AnswerChoice, Boolean> foundAnswerChoices = new HashMap<>();
        existingSurvey.getQuestions().forEach(
            question -> {
                foundQuestions.put(question, false);
                question.getAnswerChoices().forEach(
                    answerChoice -> foundAnswerChoices.put(answerChoice, false)
                );
            }
        );
        updatingQuestions.forEach(
            question -> {
                if (question.getId() != null) {
                    Question existingQuestion = existingSurvey.getQuestions().stream()
                        .filter(q -> q.getId().equals(question.getId()))
                        .findFirst()
                        .orElseThrow(() ->
                            new DataNotFoundException("Не найден вопрос с идентификатором " + question.getId())
                        );
                    foundQuestions.put(existingQuestion, true);
                    question.getAnswerChoices().forEach(
                        answerChoice -> {
                            if (answerChoice.getId() != null) {
                                AnswerChoice existingAnswerChoice = existingQuestion.getAnswerChoices().stream()
                                    .filter(ans -> ans.getId().equals(answerChoice.getId()))
                                    .findFirst()
                                    .orElseThrow(() ->
                                        new DataNotFoundException("Не найден вариант ответа с идентификатором " + answerChoice.getId())
                                    );
                                foundAnswerChoices.put(existingAnswerChoice, true);
                            }
                        }
                    );
                }
            }
        );
        foundQuestions.entrySet().stream()
            .filter(e -> !e.getValue())
            .forEach(e -> questionRepository.delete(e.getKey()));
        foundAnswerChoices.entrySet().stream()
            .filter(e -> !e.getValue())
            .forEach(e -> answerChoiceRepository.delete(e.getKey()));
        existingSurvey.setQuestions(updatingQuestions);
        Survey updatedSurvey =  surveyRepository.save(existingSurvey);
        return updatedSurvey.getQuestions();
    }

    @Nonnull
    @Override
    public Survey updateSurvey(User user, int surveyId, Survey survey) {
        Survey existingSurvey = getSurvey(surveyId);
        checkUpdatePermissions(user, existingSurvey);
        return existingSurvey
            .setAuthor(survey.getAuthor())
            .setCourse(survey.getCourse())
            .setTeachers(survey.getTeachers())
            .setStudents(survey.getStudents())
            .setStartDate(survey.getStartDate())
            .setEndDate(survey.getEndDate())
            .setYear(survey.getYear())
            .setSemester(survey.getSemester());

    }

    private void checkUpdatePermissions(User user, Survey survey) {
        List<Role> roles = user.getRoles().stream()
            .map(UserRole::getName)
            .collect(Collectors.toList());
        if (!roles.contains(Role.EMPLOYEE) && !user.getId().equals(survey.getAuthor().getId())) {
            throw new ForbiddenException("Пользователь должен быть автором опроса или сотрудником университета");
        }
    }

    @Override
    public List<Survey> getSurveysForStartNotificationSending() {
        return surveyRepository.findByStartDateLessThanEqualAndIsStartNotificatedFalse(LocalDate.now());
    }

    @Override
    public List<Survey> getSurveysForEndNotificationSending() {
        return surveyRepository.findByEndDateBeforeAndIsEndNotificatedFalse(LocalDate.now());
    }

    @Override
    public void setSurveyStartNotificated(int surveyId) {
        surveyRepository.findById(surveyId)
            .ifPresent(this::setSurveyStartNotificated);
    }

    @Override
    public void setSurveyEndNotificated(int surveyId) {
        surveyRepository.findById(surveyId)
            .ifPresent(this::setSurveyEndNotificated);
    }

    private void setSurveyStartNotificated(Survey survey) {
        survey.setStartNotificated(true);
        surveyRepository.save(survey);
    }

    private void setSurveyEndNotificated(Survey survey) {
        survey.setEndNotificated(true);
        surveyRepository.save(survey);
    }

    @Nonnull
    @Override
    public Page<Survey> searchSurvey(SurveyFilter filter, Pageable pageable) {
        Course course = null;
        User user = null;

        if (Objects.nonNull(filter.getCourseId())) {
            course = new Course();
            course.setId(filter.getCourseId());
        }

        if (Objects.nonNull(filter.getStudentId())) {
            user = new User(filter.getStudentId());
        }

        return surveyRepository.findByCourseOrStudentsEquals(course, user, pageable);
    }
}
