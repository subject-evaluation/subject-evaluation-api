package ru.nsu.fit.service.user;

import java.util.Map;

import javax.annotation.ParametersAreNonnullByDefault;

import ru.nsu.fit.entity.UserRole;
import ru.nsu.fit.entity.enums.Role;

@ParametersAreNonnullByDefault
public interface RoleService {
    /**
     * Получение роли по имени
     *
     * @param name наименование роли
     * @return информация о роли
     */
    UserRole getRoleByName(Role name);

    Map<Role, UserRole> findAll();
}
