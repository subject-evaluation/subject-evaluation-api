package ru.nsu.fit.service.survey;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import ru.nsu.fit.dto.filter.SurveyFilter;
import ru.nsu.fit.entity.Survey;
import ru.nsu.fit.entity.User;
import ru.nsu.fit.entity.question.Question;
import ru.nsu.fit.exceptions.DataNotFoundException;

import javax.annotation.Nonnull;
import javax.annotation.ParametersAreNonnullByDefault;
import java.util.List;

/**
 * Сервис для работы с курсами
 */
@ParametersAreNonnullByDefault
public interface SurveyService {

    /**
     *
     * @param survey создаваемый опрос
     * @return созданный опрос
     */
    @Nonnull
    Survey createSurvey(Survey survey);

    /**
     *
     * @param user пользователь, инициировавший запрос
     * @param id идентификатор обновляемого опроса
     * @param survey данные для обновления опроса
     * @return обновленый опрос
     */
    @Nonnull
    Survey updateSurvey(User user, int id, Survey survey);

    /**
     * Если id вопроса или варианта ответа null, создается новая сущность.
     * Если в updatingQuestions нет сущностей, что имелись в опросе из бд, недостающие сущности удаляются из бд
     * Все остальные указанные сущности должны иметь реальные id, привязанные к опросу
     *
     * @param user пользователь, инициировавший запрос
     * @param id идентификатор обновляемого опроса
     * @param updatingQuestions обновляемые вопросы
     * @return обновленные вопросы
     */
    @Nonnull
    List<Question> updateQuestions(User user, int id, List<Question> updatingQuestions);

    /**
     * Получение опроса по его идентификатору
     *
     * @param id идентификатор опроса
     * @return опрос
     * @throws DataNotFoundException если опроса с таким идентификатором не найдено
     */
    @Nonnull
    Survey getSurvey(int id);

    /**
     * Получение списка опросов, о начале которых необходимо отправить
     * email уведомление
     */
    List<Survey> getSurveysForStartNotificationSending();

    /**
     * Получение списка опросов, об окончании которых необходимо отправить
     * email уведомление
     */
    List<Survey> getSurveysForEndNotificationSending();

    /**
     * Отметить, что уведомления о начале опроса
     * были отправлены на email указанных в опросе студентов
     *
     * @param surveyId идентификатор опроса, уведомления о начале которого успешно отправлены
     */
    void setSurveyStartNotificated(int surveyId);

    /**
     * Отметить, что уведомления об окончании опроса
     * были отправлены на email указанных в опросе студентов
     *
     * @param surveyId идентификатор опроса, уведомления об окончании которого успешно отправлены
     */
    void setSurveyEndNotificated(int surveyId);

    /**
     * Поиск опросов по заданным параметрам
     *
     * @param filter   фильтр, содержащий параметры поиска
     * @param pageable настройки постраничной навигации
     * @return страница опросов
     */
    @Nonnull
    Page<Survey> searchSurvey(SurveyFilter filter, Pageable pageable);
}

