package ru.nsu.fit.service.course;

import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import ru.nsu.fit.dao.CourseRepository;
import ru.nsu.fit.dto.filter.CourseFilter;
import ru.nsu.fit.entity.Course;
import ru.nsu.fit.entity.enums.CourseStatus;
import ru.nsu.fit.exceptions.DataNotFoundException;

import javax.annotation.Nonnull;
import javax.annotation.ParametersAreNonnullByDefault;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
@ParametersAreNonnullByDefault
public class CourseServiceImpl implements CourseService {
    private static final String COURSE_NOT_FOUND = "Не найден курс с идентификатором ";
    private final CourseRepository courseRepository;

    @Nonnull
    @Override
    public Course getCourse(int id) {
        return courseRepository.findById(id)
            .orElseThrow(() -> new DataNotFoundException(COURSE_NOT_FOUND + id));
    }

    @Nonnull
    @Override
    public Page<Course> searchCourses(CourseFilter filter, Pageable pageable) {
        return courseRepository.findByStatusInAndLevelInAndNameContainingIgnoreCase(
            filter.getStatuses(),
            filter.getLevels(),
            filter.getName(),
            pageable
        );
    }

    @Override
    public Course updateCourse(int id, Course updatingCourse) {
        return courseRepository.findById(id)
            .orElseThrow(() -> new DataNotFoundException(COURSE_NOT_FOUND + id))
            .setName(updatingCourse.getName())
            .setDescription(updatingCourse.getDescription())
            .setLevel(updatingCourse.getLevel())
            .setStatus(updatingCourse.getStatus())
            .setFiles(updatingCourse.getFiles());
    }

    @Override
    public Course createCourse(Course course) {
        return courseRepository.save(course);
    }

    @Override
    public void updateCourseStatus(Set<Integer> idCourses, CourseStatus courseStatus) {
        List<Course> listCourses = courseRepository.getByIdIn(idCourses);
        for (Course course : listCourses) {
            course.setStatus(courseStatus);
        }
        courseRepository.saveAll(listCourses);
    }

    @Override
    public List<Integer> getAllCourseIds() {
        return courseRepository.findAll().stream().map(Course::getId).collect(Collectors.toList());
    }

    @Override
    public CourseStatus defineStatusByIds(List<Integer> courseIds) {
        List<CourseStatus> statuses = courseRepository.findAllById(courseIds)
            .stream().map(Course::getStatus).distinct().collect(Collectors.toList());
        if (statuses.size() != 1) {
            return null;
        }
        return statuses.get(0);
    }
}
