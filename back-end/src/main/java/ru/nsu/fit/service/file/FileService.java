package ru.nsu.fit.service.file;

import ru.nsu.fit.entity.File;

/**
 * Сервис для работы с файлами
 */
public interface FileService {
    /**
     * Сохранение файла курса
     *
     * @param file файл курса
     */
    File saveFile(File file);

    /**
     * Получение файла курса по его идентификатору
     *
     * @param id идентификатор файла курса
     * @return файл курса
     */
    File getFileById(int id);
}
