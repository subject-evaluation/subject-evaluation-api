package ru.nsu.fit.service.auth;

import java.io.IOException;

import javax.annotation.ParametersAreNonnullByDefault;

import com.google.api.client.googleapis.auth.oauth2.GoogleAuthorizationCodeTokenRequest;
import com.google.api.client.googleapis.auth.oauth2.GoogleRefreshTokenRequest;
import com.google.api.client.googleapis.auth.oauth2.GoogleTokenResponse;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.jackson2.JacksonFactory;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import ru.nsu.fit.dto.user.GoogleUser;
import ru.nsu.fit.entity.User;
import ru.nsu.fit.exceptions.GoogleAuthorizationException;
import ru.nsu.fit.mapper.user.UserMapper;

/**
 * Класс, в котором производится вся работа по авторизации через Google
 */
@Slf4j
@Service
@RequiredArgsConstructor
@ParametersAreNonnullByDefault
public class GoogleAuthServiceImpl implements GoogleAuthService {

    private final UserMapper userMapper;

    @Value("${google.token-url}")
    private String tokenServerEncodedUrl;
    @Value("${redirect-url}")
    private String redirectUrl;
    @Value("${google.client_id}")
    private String clientId;
    @Value("${google.client_secret}")
    private String clientSecret;


    /**
     * Получает информацию о пользователе по коду авторизации google
     *
     * @param authCode - код авторизации google, по нему можно узать access token, refresh token и id_token
     */
    public GoogleUser authorize(String authCode) {
        try {
            GoogleTokenResponse tokenResponse = new GoogleAuthorizationCodeTokenRequest(
                new NetHttpTransport(),
                JacksonFactory.getDefaultInstance(),
                tokenServerEncodedUrl,
                clientId,
                clientSecret,
                authCode,
                redirectUrl
            ).execute();
            return userMapper.map(tokenResponse);
        } catch (IOException e) {
            log.warn("Failed to authorize google user by authCode");
            throw new GoogleAuthorizationException(e);
        }
    }

    /**
     * Обновляет access токен пользователя с помощью google api
     */
    public GoogleUser refreshAccessToken(User user) {
        try {
            GoogleTokenResponse tokenResponse = new GoogleRefreshTokenRequest(
                new NetHttpTransport(),
                JacksonFactory.getDefaultInstance(),
                user.getRefreshToken(),
                clientId,
                clientSecret
            ).execute();
            return userMapper.map(tokenResponse);
        } catch (IOException e) {
            log.warn("Failed to refresh tokens for user {}", user.getEmail());
            throw new GoogleAuthorizationException(e);
        }
    }
}
