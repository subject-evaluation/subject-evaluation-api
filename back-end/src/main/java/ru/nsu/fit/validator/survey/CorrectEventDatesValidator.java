package ru.nsu.fit.validator.survey;

import org.springframework.beans.BeanWrapperImpl;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.time.LocalDate;

public class CorrectEventDatesValidator implements ConstraintValidator<CorrectEventDates, Object> {

    private String startDateFieldName;
    private String endDateFieldName;

    public void initialize(CorrectEventDates constraintAnnotation) {
        this.startDateFieldName = constraintAnnotation.startDate();
        this.endDateFieldName = constraintAnnotation.endDate();
    }

    public boolean isValid(Object value, ConstraintValidatorContext context) {
        Object startDateObj = new BeanWrapperImpl(value).getPropertyValue(startDateFieldName);
        Object endDateObj = new BeanWrapperImpl(value).getPropertyValue(endDateFieldName);

        if (startDateObj instanceof LocalDate && endDateObj instanceof LocalDate) {
            LocalDate startDate =  (LocalDate) startDateObj;
            LocalDate endDate =  (LocalDate) endDateObj;
            if (startDate.compareTo(endDate) > 0) {
                return false;
            }
            LocalDate tomorrow = LocalDate.now().plusDays(1);
            if (startDate.compareTo(tomorrow) < 0) {
                return false;
            }
        } else {
            return false;
        }
        return true;
    }
}