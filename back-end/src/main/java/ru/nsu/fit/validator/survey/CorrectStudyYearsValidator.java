package ru.nsu.fit.validator.survey;

import org.springframework.beans.BeanWrapperImpl;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.time.LocalDate;

public class CorrectStudyYearsValidator implements ConstraintValidator<CorrectStudyYears, Object> {
    private String yearFieldName;

    public void initialize(CorrectStudyYears constraintAnnotation) {
        this.yearFieldName = constraintAnnotation.year();
    }

    public boolean isValid(Object value, ConstraintValidatorContext context) {
        Object yearObj = new BeanWrapperImpl(value).getPropertyValue(yearFieldName);

        if (yearObj instanceof String) {
            String year = (String) yearObj;
            year = year.replaceAll(" ", "");
            String[] years = year.split("[-]");
            if (years.length < 2) {
                return false;
            }
            try {
                int startYear = Integer.parseInt(years[0]);
                int endYear = Integer.parseInt(years[1]);
                LocalDate now = LocalDate.now();
                if (startYear >= endYear) {
                    return false;
                }
                if (now.getYear() > endYear) {
                    return false;
                }
            } catch (NumberFormatException e) {
                return false;
            }
        } else {
            return false;
        }
        return true;
    }
}
