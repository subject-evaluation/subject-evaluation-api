package ru.nsu.fit.entity.enums;

public enum Role {
    STUDENT,
    TEACHER,
    EMPLOYEE,
    ;

    public String getAuthority() {
        return "ROLE_" + name();
    }
}
