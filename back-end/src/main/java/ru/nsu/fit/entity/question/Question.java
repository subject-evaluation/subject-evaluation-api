package ru.nsu.fit.entity.question;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.Accessors;
import ru.nsu.fit.entity.Identifiable;
import ru.nsu.fit.entity.Survey;
import ru.nsu.fit.entity.enums.QuestionType;

import javax.persistence.DiscriminatorColumn;
import javax.persistence.DiscriminatorType;
import javax.persistence.Entity;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.util.List;

@Entity
@Getter
@Setter
@Accessors(chain = true)
@NoArgsConstructor
@Table(name = "questions")
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name="type",
    discriminatorType = DiscriminatorType.STRING)
public abstract class Question extends Identifiable {

    private String title;

    @ManyToOne
    @JoinColumn(name = "survey_id")
    private Survey survey;

    public abstract QuestionType getQuestionType();

    public abstract List<AnswerChoice> getAnswerChoices();

    public abstract Question setAnswerChoices(List<AnswerChoice> answerChoices);

}
