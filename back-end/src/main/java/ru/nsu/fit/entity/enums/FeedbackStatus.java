package ru.nsu.fit.entity.enums;

public enum FeedbackStatus {
    DRAFT,
    SUBMITTED,
    PUBLISHED,
    DELETED,
}
