package ru.nsu.fit.entity.question;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import ru.nsu.fit.entity.enums.QuestionType;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import java.util.ArrayList;
import java.util.List;

@Entity
@Getter
@Setter
@NoArgsConstructor
@DiscriminatorValue("OPEN")
public class OpenQuestion extends Question {

    @Override
    public QuestionType getQuestionType() {
        return QuestionType.OPEN;
    }

    @Override
    public List<AnswerChoice> getAnswerChoices() {
        return new ArrayList<>();
    }

    @Override
    public Question setAnswerChoices(List<AnswerChoice> answerChoices) {
        return this;
    }
}
