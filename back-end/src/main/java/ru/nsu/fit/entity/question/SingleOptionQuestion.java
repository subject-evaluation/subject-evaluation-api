package ru.nsu.fit.entity.question;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.OneToMany;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.Accessors;
import ru.nsu.fit.entity.enums.QuestionType;

@Entity
@Getter
@Setter
@Accessors(chain = true)
@NoArgsConstructor
@DiscriminatorValue("SINGLE_OPTION")
public class SingleOptionQuestion extends Question {

    @OneToMany(mappedBy = "question", cascade = CascadeType.ALL)
    private List<AnswerChoice> answerChoices;

    @Override
    public QuestionType getQuestionType() {
        return QuestionType.SINGLE_OPTION;
    }

    @Override
    public Question setAnswerChoices(List<AnswerChoice> answerChoices) {
        answerChoices.forEach(
            answerChoice -> answerChoice.setQuestion(this)
        );
        this.answerChoices = answerChoices;
        return this;
    }

}
