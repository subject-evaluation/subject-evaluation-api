package ru.nsu.fit.util;

import lombok.experimental.UtilityClass;

@UtilityClass
public class RegexpPatterns {

    public static final String DATE = "^\\d{4}-\\d{2}-\\d{2}$";

    public static final String STUDY_YEAR = "^\\d{4} ?- ?\\d{4}$";
}
