package ru.nsu.fit.util.paths;

import lombok.experimental.UtilityClass;

import static ru.nsu.fit.util.paths.RootPaths.API_VERSION_SECURE_PREFIX;

@UtilityClass
public class SurveyPaths {
    public static final String SURVEY_ROOT_PATH = API_VERSION_SECURE_PREFIX + "/survey";
    public static final String SEARCH_PATH = "/search";
}
