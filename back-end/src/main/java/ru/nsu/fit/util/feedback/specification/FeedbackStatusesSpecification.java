package ru.nsu.fit.util.feedback.specification;

import lombok.AllArgsConstructor;
import org.springframework.data.jpa.domain.Specification;
import ru.nsu.fit.entity.Feedback;
import ru.nsu.fit.entity.enums.FeedbackStatus;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.Set;

@AllArgsConstructor
public class FeedbackStatusesSpecification implements Specification<Feedback> {
    private Set<FeedbackStatus> feedbackStatusSet;

    @Override
    public Predicate toPredicate(Root<Feedback> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder) {
        if (feedbackStatusSet.size() == 0)
            return null;
        Predicate result = null;
        for (FeedbackStatus status : feedbackStatusSet) {
            if (null == result) {
                result = criteriaBuilder.equal(root.get("status"), status);
            } else {
                result = criteriaBuilder.or(result, criteriaBuilder.equal(root.get("status"), status));
            }
        }
        return result;
    }
}
