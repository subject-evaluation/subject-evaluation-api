package ru.nsu.fit.util.paths;

import lombok.experimental.UtilityClass;

import static ru.nsu.fit.util.paths.RootPaths.API_VERSION_SECURE_PREFIX;

@UtilityClass
public class UserPaths {
    public static final String USERS_ROOT_PATH = API_VERSION_SECURE_PREFIX + "/users";
}
