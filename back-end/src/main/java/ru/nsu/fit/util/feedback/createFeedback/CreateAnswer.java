package ru.nsu.fit.util.feedback.createFeedback;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotNull;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Accessors(chain = true)
public class CreateAnswer {

    @NotNull
    Integer questionId;

    String answerText;

    @NotNull
    List<Integer> answerChoiceId;

}
