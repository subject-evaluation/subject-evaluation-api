package ru.nsu.fit.util.feedback;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.Accessors;

import java.util.List;

@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
@Accessors(chain = true)
public class Answer {

    public String answerText;

    public Integer questionId;

    public String questionTitle;

    public List<String> answerChoice;

}
