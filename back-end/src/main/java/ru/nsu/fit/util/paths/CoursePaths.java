package ru.nsu.fit.util.paths;

import lombok.experimental.UtilityClass;

import static ru.nsu.fit.util.paths.RootPaths.API_VERSION_SECURE_PREFIX;

@UtilityClass
public class CoursePaths {
    public static final String COURSES_ROOT_PATH = API_VERSION_SECURE_PREFIX + "/courses";
}
