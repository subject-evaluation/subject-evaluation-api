package ru.nsu.fit.util.paths;

import lombok.experimental.UtilityClass;

import static ru.nsu.fit.util.paths.RootPaths.API_VERSION_SECURE_PREFIX;

@UtilityClass
public class FeedbackPaths {
    public final static String FEEDBACK_ROOT_PATH = API_VERSION_SECURE_PREFIX + "/feedback";
    public final static String FEEDBACK_UPDATE_STATUS = "/update-status/{feedbackId}";
}
