package ru.nsu.fit.util.feedback.specification;

import lombok.AllArgsConstructor;
import org.springframework.data.jpa.domain.Specification;
import ru.nsu.fit.entity.Feedback;

import javax.persistence.criteria.*;
import java.util.Set;

@AllArgsConstructor
public class FeedbackCourseIdSpecification implements Specification<Feedback> {

    private Set<Long> courseIds;

    @Override
    public Predicate toPredicate(Root<Feedback> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder) {
        if (courseIds.size() == 0)
            return null;
        Predicate result = null;
        for (Long value : courseIds) {
            if (null == result) {
                result = criteriaBuilder.equal(root.get("survey").get("course").get("id"), value);
            } else {
                result = criteriaBuilder.or(result, criteriaBuilder.equal(root.get("survey").get("course").get("id"), value));
            }
        }
        return result;
    }
}
