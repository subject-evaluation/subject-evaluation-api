package ru.nsu.fit.util.paths;

import lombok.experimental.UtilityClass;

import static ru.nsu.fit.util.paths.RootPaths.API_VERSION_PREFIX;

@UtilityClass
public class FilePaths {
    public static final String FILES_ROOT_PATH = API_VERSION_PREFIX + "/files";
}
