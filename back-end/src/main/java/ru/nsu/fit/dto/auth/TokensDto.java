package ru.nsu.fit.dto.auth;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;
import ru.nsu.fit.entity.enums.Role;

import java.time.Instant;
import java.util.List;

@Data
@Accessors(chain = true)
@ApiModel(description = "Токен авторизации")
public class TokensDto {
    @ApiModelProperty(value = "Роли")
    private final List<Role> roles;

    @ApiModelProperty(value = "Access токен", required = true)
    private final String accessToken;

    @ApiModelProperty(value = "Refresh токен", required = true)
    private final String refreshToken;

    @ApiModelProperty(value = "Срок действия Access токена", required = true)
    private final Instant accessTokenExpireDate;
}
