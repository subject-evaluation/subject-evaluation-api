package ru.nsu.fit.dto.feedback;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.Accessors;
import ru.nsu.fit.entity.enums.FeedbackStatus;

import javax.validation.constraints.NotNull;
import java.util.Set;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Accessors(chain = true)
public class FeedbackFilter {

    @NotNull
    Set<Long> courseIds;

    @NotNull
    Set<FeedbackStatus> statuses;

}
