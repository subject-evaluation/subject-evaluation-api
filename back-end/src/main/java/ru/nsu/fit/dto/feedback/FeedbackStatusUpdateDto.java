package ru.nsu.fit.dto.feedback;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;
import ru.nsu.fit.entity.enums.FeedbackStatus;

@Data
@Accessors(chain = true)
@ApiModel("Новый статус отзыва")
public class FeedbackStatusUpdateDto {
    @ApiModelProperty(value = "Новый статус", example = "PUBLISHED")
    private FeedbackStatus status;
}
