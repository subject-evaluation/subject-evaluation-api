package ru.nsu.fit.dto.user;

import java.util.Set;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.Accessors;
import ru.nsu.fit.dto.validation.ValidStudentGroupNumber;
import ru.nsu.fit.entity.enums.Role;
import ru.nsu.fit.entity.enums.UserStatus;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Accessors(chain = true)
@ValidStudentGroupNumber
public class UserRequestDto {

    @NotBlank
    @ApiModelProperty(value = "Email")
    private String email;

    @NotBlank
    @ApiModelProperty(value = "Имя")
    private String firstName;

    @NotBlank
    @ApiModelProperty(value = "Фамилия")
    private String lastName;

    @ApiModelProperty(value = "Отчество")
    private String patronymic;

    @ApiModelProperty(value = "Номер группы")
    private String groupNumber;

    @NotNull
    @ApiModelProperty(value = "Статус пользователя")
    private UserStatus status;

    @NotNull
    @ApiModelProperty(value = "Роли пользователя")
    private Set<Role> roles;
}
