package ru.nsu.fit.dto.file;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.*;
import ru.nsu.fit.entity.enums.FileType;

@Getter
@Setter
@ToString
@AllArgsConstructor
@NoArgsConstructor
@ApiModel("Файл курса")
public class FileCreateDto {

    @ApiModelProperty(value = "Имя файла", example = "programm")
    private String name;

    @ApiModelProperty(value = "Тип содержимого файла", example = "pdf")
    private String mimeType;

    @ApiModelProperty(value = "Тип файла", example = "Методическое пособие")
    private FileType type;

    @ApiModelProperty(value = "Url файла", example = "text")
    private String url;

    @ApiModelProperty(value = "Содержимое файла", example = "001010")
    private byte[] content;

}
