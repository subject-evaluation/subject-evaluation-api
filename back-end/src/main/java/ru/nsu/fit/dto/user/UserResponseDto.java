package ru.nsu.fit.dto.user;

import java.util.Set;

import javax.validation.constraints.NotNull;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;
import ru.nsu.fit.entity.enums.Role;
import ru.nsu.fit.entity.enums.UserStatus;

@Data
@Accessors(chain = true)
@ApiModel("Пользователь")
public class UserResponseDto {
    @ApiModelProperty(value = "Идентификатор пользователя", example = "1")
    private int id;

    @ApiModelProperty(value = "Фамилия", example = "Иванов")
    private String lastName;

    @ApiModelProperty(value = "Имя", example = "Иван")
    private String firstName;

    @ApiModelProperty(value = "Отчество", example = "Иванович")
    private String patronymic;

    @NotNull
    @ApiModelProperty(value = "E-mail", example = "i.ivanov@g.nsu.ru")
    private String email;

    @NotNull
    @ApiModelProperty(value = "Множество ролей пользователя", example = "STUDENT")
    private Set<Role> roles;

    @ApiModelProperty(value = "Номер группы", example = "12345")
    private String groupNumber;

    @ApiModelProperty(value = "Статус пользователя", example = "ACTIVE")
    private UserStatus status;
}
