package ru.nsu.fit.dto.feedback;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.Accessors;;
import ru.nsu.fit.entity.enums.FeedbackStatus;
import ru.nsu.fit.util.feedback.createFeedback.CreateAnswer;

import javax.validation.constraints.NotNull;
import java.time.Instant;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Accessors(chain = true)
public class FeedbackCreateDto {

    @NotNull
    private Instant creationDate;

    @NotNull
    private Instant publishingDate;

    @NotNull
    private FeedbackStatus status;

    @NotNull
    private Integer userId;

    @NotNull
    private Integer surveyId;

    @NotNull
    private List<CreateAnswer> userAnswers;

}
