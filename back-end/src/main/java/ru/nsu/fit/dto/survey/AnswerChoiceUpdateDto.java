package ru.nsu.fit.dto.survey;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotBlank;

@Data
@Accessors(chain = true)
@ApiModel("Изменяемый вариант ответа на вопрос")
public class AnswerChoiceUpdateDto {

    @ApiModelProperty(value = "ID варианта ответа", example = "1")
    private Integer id;

    @NotBlank
    @ApiModelProperty(value = "Текст варианта ответа на вопрос", example = "Организация курса была отличная")
    private String title;
}
