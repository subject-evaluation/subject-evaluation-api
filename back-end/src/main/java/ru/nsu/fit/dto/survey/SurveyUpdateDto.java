package ru.nsu.fit.dto.survey;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;
import ru.nsu.fit.validator.survey.CorrectStudyYears;

import javax.validation.constraints.DecimalMax;
import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import java.time.LocalDate;
import java.util.List;

import static ru.nsu.fit.util.FormPatterns.DATE_FORMAT;
import static ru.nsu.fit.util.RegexpPatterns.STUDY_YEAR;

@Data
@Accessors(chain = true)
@ApiModel("Изменяемый опрос")
@CorrectStudyYears(year = "year", message = "Некорректное значение учебного года курса")
public class SurveyUpdateDto {

    @NotNull
    @ApiModelProperty(value = "ID пользователя, который создал опрос", example = "1")
    private Integer authorId;

    @NotNull
    @ApiModelProperty(value = "ID курса, по которому будет проводиться опрос", example = "1")
    private Integer courseId;

    @NotNull
    @ApiModelProperty(value = "Дата начала опроса в формате " + DATE_FORMAT, example = "2020-01-13")
    private LocalDate startDate;

    @NotNull
    @ApiModelProperty(value = "Дата начала опроса в формате " + DATE_FORMAT, example = "2020-01-13")
    private LocalDate endDate;

    @NotNull
    @Pattern(regexp = STUDY_YEAR, message = "Поле 'year' некорректно")
    @ApiModelProperty(value = "Учебный год в формате yyyy - yyyy", example = "2020 - 2021")
    private String year;

    @DecimalMin(value = "1", message = "Поле 'semester' некорректно")
    @DecimalMax(value = "2", message = "Поле 'semester' некорректно")
    @ApiModelProperty(value = "Семестр (1 или 2)", example = "1")
    private int semester;

    @NotEmpty
    @ApiModelProperty(value = "ID студентов, для которых создается опрос", example = "[1, 2 ,3]")
    private List<Integer> studentIds;

    @NotNull
    @ApiModelProperty(value = "ID учителей, для которых создается опрос", example = "[1, 2 ,3]")
    private List<Integer> teacherIds;
}
