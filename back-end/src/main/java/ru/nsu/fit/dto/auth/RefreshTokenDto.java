package ru.nsu.fit.dto.auth;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
@ApiModel(description = "Обертка для refresh токена")
public class RefreshTokenDto {
    @ApiModelProperty(value = "Refresh токен", required = true)
    private String refreshToken;
}
