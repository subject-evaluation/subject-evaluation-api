package ru.nsu.fit.dto.filter;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;
import ru.nsu.fit.entity.enums.Role;
import ru.nsu.fit.entity.enums.UserStatus;

import java.util.Set;

@Data
@Accessors(chain = true)
@ApiModel("Фильтр для поиска курсов")
public class UserFilter {

    @ApiModelProperty(value = "Статус пользователя", example = "ACTIVE")
    private Set<UserStatus> statuses;

    @ApiModelProperty(value = "Роль пользователя", example = "STUDENT")
    private Set<Role> roles;

    @ApiModelProperty(value = "Строка для поиска", example = "Иван")
    private String line;
}
