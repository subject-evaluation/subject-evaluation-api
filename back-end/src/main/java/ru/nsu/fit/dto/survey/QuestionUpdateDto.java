package ru.nsu.fit.dto.survey;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;
import ru.nsu.fit.entity.enums.QuestionType;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.List;

@Data
@Accessors(chain = true)
@ApiModel("Изменяемый вопрос опроса")
public class QuestionUpdateDto {

    @ApiModelProperty(value = "ID вопроса", example = "1")
    private Integer id;

    @NotNull
    @ApiModelProperty(value = "Тип вопроса", example = "SINGLE_OPTION")
    private QuestionType type;

    @NotBlank
    @ApiModelProperty(value = "Текст вопроса", example = "Насколько хорошо курс был организован?")
    private String title;

    @ApiModelProperty(value = "Список вариантов ответов на вопрос")
    private List<AnswerChoiceUpdateDto> answerChoices;

}
