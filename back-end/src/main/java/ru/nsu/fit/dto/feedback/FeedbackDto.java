package ru.nsu.fit.dto.feedback;

import java.time.Instant;
import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.Accessors;
import ru.nsu.fit.entity.enums.FeedbackStatus;
import ru.nsu.fit.util.feedback.Answer;
import ru.nsu.fit.util.feedback.UserDto;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Accessors(chain = true)
public class FeedbackDto {

    private int id;

    private Instant creationDate;

    private Instant publishingDate;

    private String courseName;

    private String year;

    private Integer semester;

    private FeedbackStatus feedbackStatus;

    private UserDto author;

    private List<UserDto> teachers;

    private List<Answer> answers;

    private Instant startSurveyDate;

    private Instant endSurveyDate;
}
