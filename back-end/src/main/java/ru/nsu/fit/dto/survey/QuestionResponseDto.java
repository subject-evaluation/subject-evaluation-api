package ru.nsu.fit.dto.survey;

import com.fasterxml.jackson.annotation.JsonInclude;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;
import ru.nsu.fit.entity.enums.QuestionType;

import java.util.List;

@Data
@Accessors(chain = true)
@ApiModel("Опрос по курсу")
@JsonInclude(JsonInclude.Include.NON_NULL)
public class QuestionResponseDto {

    @ApiModelProperty(value = "ID вопроса", example = "1")
    private int id;

    @ApiModelProperty(value = "Тип вопроса", example = "SINGLE_OPTION")
    private QuestionType type;

    @ApiModelProperty(value = "Текст вопроса", example = "Насколько хорошо курс был организован?")
    private String title;

    @ApiModelProperty(value = "Список вариантов ответов на вопрос")
    private List<AnswerChoiceResponseDto> answerChoices;
}
