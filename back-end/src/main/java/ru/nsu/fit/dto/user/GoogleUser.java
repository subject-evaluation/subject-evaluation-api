package ru.nsu.fit.dto.user;

import io.swagger.annotations.ApiModel;
import lombok.Data;
import lombok.experimental.Accessors;

import java.time.Instant;

@Data
@Accessors(chain = true)
@ApiModel("Пользователь Google")
public class GoogleUser {
    private String email;
    private String firstName;
    private String lastName;
    private String googleId;
    private String accessToken;
    private String refreshToken;
    private Instant accessTokenExpireDate;
}
