package ru.nsu.fit.dto.course;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;
import ru.nsu.fit.entity.enums.CourseLevel;
import ru.nsu.fit.entity.enums.CourseStatus;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Data
@Accessors(chain = true)
@ApiModel("Учебный курс")
public class CourseRequestDto {
    @NotBlank
    @ApiModelProperty(value = "Название курса", example = "Математический анализ")
    private String name;

    @ApiModelProperty(value = "Описание курса", example = "Курс об основах математического анализа")
    private String description;

    @NotNull
    @ApiModelProperty(value = "Ступень курса", example = "UNDERGRADUATE")
    private CourseLevel level;

    @NotNull
    @ApiModelProperty(value = "Статус курса", example = "ACTIVE")
    private CourseStatus status;
}
