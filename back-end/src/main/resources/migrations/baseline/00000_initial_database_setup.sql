-- noinspection SqlNoDataSourceInspectionForFile

--liquibase formatted sql
--changeset smirnov_alexander:create tables

CREATE TABLE users
(
    id           SERIAL PRIMARY KEY,
    email        TEXT,
    last_name    VARCHAR(50) NOT NULL,
    first_name   VARCHAR(50) NOT NULL,
    patronymic   VARCHAR(50),
    group_number VARCHAR(20),
    status       VARCHAR(50) DEFAULT 'ACTIVE',
    CHECK (status IN ('ACTIVE', 'BLOCKED'))
);

CREATE TABLE roles
(
    id   SERIAL PRIMARY KEY,
    name VARCHAR(50) NOT NULL,
    CHECK (name IN ('STUDENT', 'TEACHER', 'EMPLOYEE'))
);

INSERT INTO roles (name)
values ('STUDENT'),
       ('TEACHER'),
       ('EMPLOYEE');

CREATE UNIQUE INDEX users_index
    ON users (id);

CREATE TABLE user_roles
(
    user_id INTEGER NOT NULL REFERENCES users (id),
    role_id INTEGER NOT NULL REFERENCES roles (id),
    PRIMARY KEY (user_id, role_id)
);

CREATE INDEX user_roles_index
    ON user_roles (user_id);

CREATE TABLE form_templates
(
    id        SERIAL PRIMARY KEY,
    name      VARCHAR(50) NOT NULL,
    author_id INTEGER     NOT NULL REFERENCES users (id),
    template  TEXT        NOT NULL
);

CREATE INDEX form_templates_index
    ON form_templates (author_id);

CREATE TABLE courses
(
    id          SERIAL PRIMARY KEY,
    name        VARCHAR(100) NOT NULL,
    description TEXT,
    status      VARCHAR(50)  NOT NULL,
    level       VARCHAR(50)  NOT NULL,
    CHECK (status IN ('ACTIVE', 'ARCHIVED'))
);

CREATE TABLE surveys
(
    id                   SERIAL PRIMARY KEY,
    author_id            INTEGER                     NOT NULL REFERENCES users (id),
    course_id            INTEGER                     NOT NULL REFERENCES courses (id),
    start_date           TIMESTAMP(0) WITH TIME ZONE NOT NULL,
    end_date             TIMESTAMP(0) WITH TIME ZONE NOT NULL,
    year                 SMALLINT                    NOT NULL,
    semester             SMALLINT,
    is_start_notificated BOOLEAN default false,
    is_end_notificated   BOOLEAN default false,
    CHECK (year > 0),
    CHECK (semester > 0)
);

CREATE INDEX surveys_index
    ON surveys (author_id);

CREATE TABLE feedbacks
(
    id              SERIAL PRIMARY KEY,
    author_id       INTEGER                     NOT NULL REFERENCES users (id),
    survey_id       INTEGER                     NOT NULL REFERENCES surveys (id),
    creation_date   TIMESTAMP(0) WITH TIME ZONE NOT NULL,
    publishing_date TIMESTAMP(0) WITH TIME ZONE NOT NULL,
    status          VARCHAR(50)                 NOT NULL CHECK (status IN ('DRAFT', 'SUBMITTED', 'PUBLISHED', 'DELETED'))
);

CREATE INDEX feedbacks_author_index
    ON feedbacks (author_id);

CREATE TABLE reactions
(
    id          SERIAL PRIMARY KEY,
    feedback_id INTEGER     NOT NULL REFERENCES feedbacks (id),
    user_id     INTEGER     NOT NULL REFERENCES users (id),
    vote        VARCHAR(50) NOT NULL,
    CHECK (vote IN ('UPVOTE', 'DOWNVOTE')),
    UNIQUE (feedback_id, user_id, vote)
);

CREATE INDEX reactions_index
    ON reactions (feedback_id);

CREATE TABLE files
(
    id        SERIAL PRIMARY KEY,
    course_id INTEGER      NOT NULL REFERENCES courses (id),
    name      VARCHAR(100) NOT NULL,
    mime_type VARCHAR(50),
    type      VARCHAR(50),
    url       TEXT
);

CREATE TABLE survey_teachers
(
    survey_id  INTEGER NOT NULL REFERENCES surveys (id),
    teacher_id INTEGER NOT NULL REFERENCES users (id),
    PRIMARY KEY (survey_id, teacher_id)
);

CREATE TABLE survey_students
(
    survey_id  INTEGER NOT NULL REFERENCES surveys (id),
    student_id INTEGER NOT NULL REFERENCES users (id),
    PRIMARY KEY (survey_id, student_id)
);

CREATE TABLE shedlock
(
    name        VARCHAR(64),
    lock_until TIMESTAMP(3) NULL,
    locked_at  TIMESTAMP(3) NULL,
    locked_by  VARCHAR(255),
    PRIMARY KEY (name)
);
